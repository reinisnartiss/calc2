<?php

namespace App\Exports;

use App\Models\User;
use Carbon\CarbonPeriod;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class EmployeeReportExport implements FromView, WithStyles, WithColumnWidths
{
    public function styles(Worksheet $sheet)
    {
        return [
            4 => ['font' => ['bold' => true, 'size' => 16]],
            ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 30,
        ];
    }

    public function view(): View
    {
        return view('exports.employee-report-export', [
            'users' => User::find(request()->selectedUsers),
            'startDate' => request()->startDate,
            'endDate' => request()->endDate,
            'period' => CarbonPeriod::create(request()->startDate, request()->endDate),
        ]);
    }
}
