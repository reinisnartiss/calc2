<?php

namespace App\Http\Controllers;

use App\Exports\EmployeeReportExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function export()
    {
        return Excel::download(new EmployeeReportExport, 'Darbinieku_atskaite.xlsx');
    }
}
