<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInvite;
use App\Models\Invite;
use App\Mail\InviteMail;
use App\Providers\MailSent;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;

class InviteController extends Controller
{
    public function index()
    {
        $invites = Invite::all();
        return view('invite', compact('invites'));
    }

    public function store(StoreInvite $request)
    {
        $token = Str::random(32);
        $invite = Invite::create([
            'email' => request('email'),
            'user_id' => auth()->user()->id,
            'token' => $token
        ]);

        $response = Mail::to($request->email)->send(new InviteMail($invite));
        MailSent::dispatch($request->email, env('MAIL_FROM_ADDRESS'), 'Portal invite sent', $response);


        return redirect()->back()->with('success', 'Ielūgums nosūtīts veiksmīgi');
    }

    public function destroy(Invite $invite)
    {
        $invite->delete();

        return redirect()->back()->with('success', 'Ielūgums veiksmīgi izdzēsts');
    }

    public function resendInvite(Invite $invite)
    {
        $response = Mail::to($invite->email)->send(new InviteMail($invite));
        MailSent::dispatch($invite->email, env('MAIL_FROM_ADDRESS'), 'Resent portal invite', $response);

        return redirect()->back()->with('success', 'Atkārtots ielūgums nosūtīts veiksmīgi');
    }

}
