<?php

namespace App\Http\Controllers;

use App\Models\Resignation;
use Illuminate\Http\Request;

class ResignationController extends Controller
{
    public function store(Request $request)
    {
        Resignation::create($request->except('_token'));

        return redirect()->back();
    }

    public function destroy($id)
    {
        $resignation = Resignation::find($id);

        $resignation->delete();

        return redirect()->back();
    }
}
