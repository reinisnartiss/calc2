<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserInfoRequest;
use App\Mail\NewUserRegisteredMail;
use App\Models\User;
use App\Models\Invite;
use App\Models\WorkContract;
use App\Http\Requests\StoreUser;
use App\Providers\MailSent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{

    public function index()
    {
        $users = User::with('companies', 'manager', 'roles')->get()->sortBy('name');
        $openContracts = WorkContract::active()->with('user', 'company')->get();
        return view('users.index', compact('users', 'openContracts'));
    }

    public function create($token)
    {
        $invite = Invite::where('token', $token)->firstOrFail();

        return view('users.create-login', compact('invite'));
    }

    public function store(StoreUser $request)
    {
        $user = User::create($request->validated());
        $user->assignRole('darbinieks');

        Auth::login($user);
        $response = Mail::to(User::role('grāmatvedis')->first()->email)->send(new NewUserRegisteredMail($user));
        MailSent::dispatch(User::role('grāmatvedis')->first()->email, env('MAIL_FROM_ADDRESS'), 'New user registration', $response);

        Invite::where('email', '=', auth()->user()->email)->delete();

        return redirect('/')->with('status', 'Profils izveidots veiksmīgi');
    }

    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    public function editRole(User $user)
    {
        $roles = \Spatie\Permission\Models\Role::all();
        $managers = User::role(['vadītājs','super-admin'])->get();
        return view('users.edit_role', compact('user', 'roles', 'managers'));
    }

    public function update(UpdateUserInfoRequest $request, User $user)
    {
        $user->update($request->validated());

        return redirect()->route('users.show', $user->id)->with('status', 'Lietotāja dati laboti veiksmīgi');
    }

    public function toggleBlock(User $user)
    {
        $user->is_blocked == true ? $user->is_blocked = false : $user->is_blocked = true;
        $user->save();

        return redirect()->back();
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index');
    }

}
