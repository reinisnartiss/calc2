<?php

namespace App\Http\Controllers;

use Acaronlex\LaravelCalendar\Calendar;
use App\Http\Requests\Vacations\StoreVacationRequest;
use App\Http\Requests\Vacations\UpdateVacationRequest;
use App\Mail\ManagerHasApprovedVacation;
use App\Mail\ManagerVacationConfirmation;
use App\Mail\VacationApprovedBookkeeperMail;
use App\Mail\VacationApprovedMail;
use App\Mail\VacationNotApprovedMail;
use App\Models\User;
use App\Models\Vacation;
use App\Providers\MailSent;
use App\Services\Exports\PdfExport;
use App\Services\Vacations\CreateVacationWorkReports;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class VacationController extends Controller
{
    public function index()
    {
        $user = auth()->user();

        $employeeIds = $user->employees->pluck('id');

        $vacations = Vacation::query()->when($user->hasRole('vadītājs'), function ($query) use ($employeeIds) {
            return $query->whereIn('user_id', $employeeIds);
        })->with('user')->get();

        $waitingApprovalVacations = $vacations
            ->where('final_approval', false)
            ->where('not_approved', false)
            ->where('start_date', '>=', Carbon::now())
            ->paginate(10, 'waiting');

        $approvedVacations = $vacations
            ->where('final_approval', true)
            ->where('start_date', '>', now());
        $activeVacations = $vacations
            ->where('final_approval', true)
            ->where('start_date', '<=', now())
            ->where('end_date', '>=', now());


        $approvedAndActiveVacations = $approvedVacations->merge($activeVacations)->sortBy('start_date')->paginate(10, 'approved');

        $userVacations = $vacations->where('user_id', $user->id)->paginate(10, 'user_vacations');

        $endedOrUnapprovedVacations = Vacation::query()
            ->when($user->hasRole('vadītājs'), function ($query) use ($employeeIds) {
                $query->whereIn('user_id', $employeeIds);
            })
            ->inactiveVacations()
            ->paginate(10, 'ended');

        $events = [];

        foreach ($vacations as $vacation) {
            $events[] = [
                'title' => $vacation->user->name . ' - ' . $vacation->status(),
                'start' => $vacation->start_date->toDateString(),
                'end' => $vacation->end_date->addDay()->toDateString(),
                'url' => route('vacations.show', $vacation),
                'backgroundColor' => $vacation->background_color,
                'borderColor' => 'black',
                'textColor' => 'black'
            ];
        }

        return view('vacations.index',
            compact('user', 'events', 'waitingApprovalVacations', 'approvedAndActiveVacations', 'userVacations', 'endedOrUnapprovedVacations'));
    }

    public function show(Vacation $vacation)
    {
        return view('vacations.show', compact('vacation'));
    }

    public function store(StoreVacationRequest $request)
    {
        $vacationPeriod = CarbonPeriod::create($request->start_date, $request->end_date);

        $vacationPeriod->filter(function ($date) {
            return $date->isBusinessDay();
        });

        $vacationDaysCount = $vacationPeriod->count();

        $newVacation = Vacation::create($request->validated());
        $newVacation->vacation_days = $vacationDaysCount;
        $newVacation->companies()->attach($request->selected_companies);
        $newVacation->save();

        if (auth()->user()->hasRole('super-admin')) {
            $newVacation->manager_approval = true;
            $newVacation->final_approval = true;
            $newVacation->save();
            CreateVacationWorkReports::make()->vacationWorkReports($newVacation);
            Mail::to(User::role('grāmatvedis')->first()->email)->send(new VacationApprovedBookkeeperMail($newVacation));
            MailSent::dispatch(User::role('grāmatvedis')->first()->email, env('MAIL_FROM_ADDRESS'), 'Admin has created a vacation');
        }
        if (auth()->user()->hasAnyRole(['vadītājs', 'grāmatvedis'])) {
            Mail::to(User::where('id', $request->user_id)
                ->first()->companies()->oldest()->first()->vacationManager->email)
                ->send(new ManagerVacationConfirmation($newVacation));
            MailSent::dispatch(User::where('id', $request->user_id)
                ->first()->companies()->first()->vacationManager->email, env('MAIL_FROM_ADDRESS'), 'Manager has created a vacation');
        }
        if (User::where('id', $newVacation->user_id)->first()->hasRole('darbinieks')) {
            Mail::to(User::where('id', $request->user_id)
                ->first()->manager->email)
                ->send(new ManagerVacationConfirmation($newVacation));
            MailSent::dispatch(User::where('id', $request->user_id)
                ->first()->manager->email, env('MAIL_FROM_ADDRESS'), 'Employee has created a vacation');
        }

        return redirect()->route('vacations.index');
    }

    public function edit(Vacation $vacation)
    {
        return view('vacations.edit', compact('vacation'));
    }

    public function updateDates(UpdateVacationRequest $request, Vacation $vacation)
    {
        $vacation->update($request->validated());

        $vacationDays = CarbonPeriod::create($vacation->start_date, $vacation->end_date);
        $vacationDays->filter(function ($date) {
            return $date->isBusinessDay();
        });
        $vacation->vacation_days = $vacationDays->count();
        $vacation->save();

        return redirect('/vacations/' . $vacation->id);
    }

    public function updateComment(Request $request, Vacation $vacation)
    {
        if ($request->comment) {
            $vacation->comments()->create(['body' => $request->comment, 'user_id' => auth()->user()->id]);
        }
        $vacation->save();

        return redirect()->back();
    }

    public function destroy(Vacation $vacation)
    {
        $vacation->delete();

        return redirect()->route('vacations.index')->with('status', 'Atvaļinājums izdzēsts veiksmīgi');
    }

    public function updateStatus(Request $request, Vacation $vacation)
    {
        if ($request->vacation_status == 'accept') {
            if (auth()->user()->hasPermissionTo('approve vacations')) {
                $vacation->not_approved = false;
                $vacation->manager_approval = true;
                $vacation->final_approval = true;
                $vacation->save();

                Mail::to($vacation->user->email)->send(new VacationApprovedMail($vacation));
                MailSent::dispatch($vacation->user->email, env('MAIL_FROM_ADDRESS'), 'Vacation has been fully approved - Employee mail');

                Mail::to(User::role('grāmatvedis')->first()->email)->send(new VacationApprovedBookkeeperMail($vacation));
                MailSent::dispatch(User::role('grāmatvedis')->first()->email, env('MAIL_FROM_ADDRESS'), 'Vacation has been fully approved - Bookkeeper mail');

                CreateVacationWorkReports::make()->vacationWorkReports($vacation);
            } else {
                $vacation->manager_approval = true;
                $vacation->save();
                Mail::to($vacation->user->companies()->first()->vacationManager->email)->send(new ManagerHasApprovedVacation($vacation));
                MailSent::dispatch($vacation->user->companies()->first()->vacationManager->email, env('MAIL_FROM_ADDRESS'), 'Manager has approved employees vacation');
            }
        }
        if ($request->vacation_status == 'reject') {
            Mail::to($vacation->user->email)->send(new VacationNotApprovedMail($vacation));
            MailSent::dispatch($vacation->user->email, env('MAIL_FROM_ADDRESS'), 'Vacation has not been approved');

            $vacation->not_approved = true;
            $vacation->save();
        }

        return redirect()->back();
    }

    public function export(Request $request, Vacation $vacation)
    {
        $user = User::find($vacation->user_id);

        PdfExport::make()->vacationPdf($request, $vacation, $user);
    }

}
