<?php

namespace App\Http\Controllers;

use App\Http\Requests\WorkContracts\WorkContractStoreRequest;
use App\Mail\NewWorkContractEmail;
use App\Models\Company;
use App\Models\User;
use App\Models\WorkContract;
use App\Providers\MailSent;
use App\Services\Exports\PdfExport;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;


class WorkContractController extends Controller
{
    public function create(User $user)
    {
        return view('work_contract.create', compact('user'));
    }

    public function store(WorkContractStoreRequest $request)
    {
        $user = User::find($request->user_id);
        $company = Company::find($request->company_id);

        if ($request->name_genitivs) {
            $user->name_genitivs = $request->name_genitivs;
        }
        if ($request->name_dativs) {
            $user->name_dativs = $request->name_dativs;
        }
        if ($request->name_akuzativs) {
            $user->name_akuzativs = $request->name_akuzativs;
        }

        $user->companies()->attach($company, ['contract_number' => $request->contract_number, 'starting_date' => $request->contract_start]);
        $user->save();

        WorkContract::create($request->except(['user_genitivs', 'user_dativs', 'user_akuzativs']));

        Mail::to($user->email)->send(new NewWorkContractEmail());
        MailSent::dispatch($user->email, env('MAIL_FROM_ADDRESS'), 'New work contract created');

        return redirect()->route('users.index');
    }

    public function show($id)
    {
        $contract = WorkContract::find($id);
        return view('work_contract.show', compact('contract'));
    }

    public function edit(WorkContract $contract)
    {
        $user = User::where('id', $contract->user_id)->first();
        return view('work_contract.edit', compact('contract','user'));
    }

    public function update(Request $request, WorkContract $contract)
    {
        $contract->update($request->all());
        $contract->is_printed_contract = false;
        $contract->is_printed_ordinance = false;
        $contract->order_date = null;
        $contract->order_number = null;
        $contract->save();

        return redirect('/work-contract/' . $contract->id);
    }

    public function destroy(WorkContract $contract)
    {
        $user = User::where('id', $contract->user_id)->first();
        $company = Company::where('id', $contract->company_id)->first();
        $company->vacations()->where('user_id', $user->id)->delete();
        $user->companies()->detach($company);
        $contract->delete();

        return redirect(route('users.index'))->with('status', 'Darba līgums izdzēsts veiksmīgi');
    }

    public function exportContract(WorkContract $contract)
    {
        PdfExport::make()->workContractPdf($contract);

        $contract->is_printed_contract = true;
        $contract->save();
    }

    public function exportContractWord(WorkContract $contract)
    {
        $headers = [
            "Content-type"=>"text/html",
            "Content-Disposition"=>"attachment;Filename=Darba_ligums.doc"
        ];

        $workContract = $contract;

        $content = View::make('work_contract.contractpdf', compact('workContract'));

        $contract->is_printed_contract = true;
        $contract->save();

        return \Illuminate\Support\Facades\Response::make($content,200, $headers);
    }

    public function exportOrdinance(Request $request, WorkContract $contract)
    {
        if ($request->order_number) {
            $contract->order_number = $request->order_number;
        }
        if ($request->order_date !== null) {
            $contract->order_date = $request->order_date;
        } else {
            $contract->order_date = $contract->date;
        }

        $contract->is_printed_ordinance = true;
        $contract->save();

        $format = $request->action;
        if ($format == 'pdf') {
            PdfExport::make()->ordinancePdf($contract);
        }
        if ($format == 'word') {
            $headers = [
                "Content-type"=>"text/html",
                "Content-Disposition"=>"attachment;Filename=Darba_rikojums.doc"
            ];

            $workContract = $contract;

            $content = View::make('work_contract.ordinancepdf', compact('workContract', 'format'));

            return \Illuminate\Support\Facades\Response::make($content,200, $headers);
        }
        return 'error';
    }

}
