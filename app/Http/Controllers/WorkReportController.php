<?php

namespace App\Http\Controllers;

use App\Services\Reports\WorkReportPeriod;
use Carbon\Carbon;
use App\Models\Reason;
use App\Models\WorkReport;
use App\Http\Requests\StoreWorkReport;
use App\Http\Requests\CreateWorkReport;
use App\Http\Requests\WorkReports\WorkReportUpdateRequest;

class WorkReportController extends Controller
{

    public function index()
    {
        return view('work_report.index');
    }

    public function show(WorkReport $workReport)
    {
        $reasons = Reason::query()->whereIn('id', WorkReport::ALLOWED_REASONS)->get();

        return view('work_report.show', compact('workReport', 'reasons'));
    }

    public function create(CreateWorkReport $request)
    {
        $reasons = Reason::query()->whereIn('id', WorkReport::ALLOWED_REASONS)->get();

        $dates = WorkReportPeriod::make()
            ->period(Carbon::parse($request->startDate), Carbon::parse($request->endDate), $request->weekends)
            ->values();

        return view('work_report.create', compact('dates', 'reasons'));
    }

    public function store(StoreWorkReport $request)
    {
        $filteredWorkReports = collect($request->workReports)->filter(function ($workReport) {
            return !(((Carbon::parse($workReport['work_date']))->isWeekend() && $workReport['did_work'] == 0)
                || ((Carbon::parse($workReport['work_date']))->isWeekend() && $workReport['work_hours'] == 0)
            || ((Carbon::parse($workReport['work_date']))->isHoliday() && $workReport['did_work'] == 0)
            || ((Carbon::parse($workReport['work_date']))->isHoliday() && $workReport['work_hours'] == 0));
        });

        auth()->user()->workReports()->createMany($filteredWorkReports);

        return redirect(route('work-report.index'))->with('status', 'Darba atskaite iesniegta veiksmīgi');
    }

    public function update(WorkReportUpdateRequest $request, WorkReport $workReport)
    {
        $workReport->work_hours = null;
        $workReport->reason_id = null;
        $workReport->comment = null;
        $workReport->save();

        $workReport->update($request->except('did-work'));
        return redirect(route('work-report.index'))->with('status', 'Darba atskaite saglabāta veiksmīgi');
    }

    public function destroy(WorkReport $workReport)
    {
        $workReport->delete();

        return redirect(route('work-report.index'))->with('status', 'Darba atskaite izdzēsta veiksmīgi');
    }
}
