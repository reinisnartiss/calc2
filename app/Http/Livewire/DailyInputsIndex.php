<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\WorkReport;
use Illuminate\Support\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Str;
use Livewire\Component;

class DailyInputsIndex extends Component
{
    public $users;
    public $user;
    public $allDates;
    public $date;
    public $selectedMonth;
    public $monthsData;
    public $workReports;

    public function mount()
    {
        $this->selectedMonth = Carbon::now()->format('Y-m');
        $this->users = User::with(['workReports' => function ($query) {
            return $query->whereMonth('work_date', now()->month);
        }])->get();
        $this->getAllData();
    }

    public function getAllData()
    {
        $this->allDates = collect(CarbonPeriod::create(Carbon::parse($this->selectedMonth)->startOfMonth(), Carbon::parse($this->selectedMonth)->endOfMonth()));
        $this->workReports = WorkReport::with(['user', 'reason'])->whereMonth('work_date', Str::after($this->selectedMonth, '-'))->get();
        $this->getMonthsData();
    }

    private function getMonthsData()
    {
        $this->monthsData = $this->allDates->mapWithKeys(function ($date) {
            return [
                $date->format('Y-m-d') => [
                    'work_report_count' => $this->workReports->where('work_date', $date)->count(),
                    'work_report_away' => $this->workReports->where('reason_id', !null)->where('work_date', $date)->count(),
                    'work_report_overtime' => $date->isBusinessDay()
                        ? $this->workReports->where('work_date', $date)->where('work_hours', '>', 8)->count()
                        : $this->workReports->where('work_date', $date)->where('work_hours', '>', 0)->count(),
                    'data' => $this->workReports->where('work_date', $date)->map(function ($workReport) use ($date) {
                        return [
                            'user' => $workReport->user->name,
                            'work_hours' => $workReport->work_hours,
                            'comment' => $workReport->comment,
                            'reason' => $workReport->reason ? $workReport->reason->name : null
                        ];
                    })
                ]
            ];
        });
        return $this->monthsData;
    }

    public function render()
    {
        return view('livewire.daily-inputs-index');
    }

}
