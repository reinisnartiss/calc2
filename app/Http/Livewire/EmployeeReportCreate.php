<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Carbon;
use Livewire\Component;
use App\Models\WorkReport;

class EmployeeReportCreate extends Component
{
    public $users;
    public $startDate;
    public $endDate;
    public $selectAll = false;
    public $selected = [];
    public $tableHeaders= [
        'Stundas',
        'Dienas',
        'Prombūtnes',
        'Virsstundas'
    ];

    public function mount()
    {
        $this->startDate = Carbon::now()->startOfMonth()->format('Y-m-d');
        $this->endDate = Carbon::now()->format('Y-m-d');

        if (auth()->user()->hasRole('vadītājs')) {
            $this->users = User::whereHas('manager', function ($query) {
                $query->where('id', auth()->user()->id);
            })->where('is_blocked', false)->orderBy('name')->get();
        } else {
            $this->users = User::query()->where('is_blocked', false)->orderBy('name')->get();
        }
    }

    public function updated()
    {
        $this->validate([
            'startDate' => 'beforeOrEqual:endDate',
            'selected' => 'required'
            ]);
    }

    private function calculateOvertime($workReports)
    {
        $overtime = 0;
        foreach ($workReports as $workReport) {
            $overtime += $workReport->getOvertime($workReport->work_hours, $workReport->work_date);
        }

        return $overtime;
    }

    public function selectAll()
    {
        if ($this->selectAll == true) {
            $this->selected = $this->users->pluck('id')->map(function ($id) {
               return (string) $id;
            });
        } else {
            $this->selected = [];
        }
    }

    public function render()
    {
        return view('livewire.employee-report-create')->with([
            'workReports' => WorkReport::query()->whereBetween('work_date', [$this->startDate, $this->endDate])->get()
        ]);
    }
}
