<?php

namespace App\Http\Livewire;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Http;
use Livewire\Component;

class ProjectHoursReport extends Component
{
    public $search = '';
    public $selectedProject = null;
    public $workTypes = [];
    public $workType;
    public $minutes;
    public $comment;
    public $lastReports;

    protected $rules = [
        'selectedProject' => 'required',
        'workType' => 'required',
        'minutes' => 'required',
        'comment' => 'sometimes'
    ];

    protected $messages = [
        'workType.required' => 'Izvēlies darba veidu',
        'minutes.required' => 'Ievadi nostrādātās minūtes',
    ];

    public function mount()
    {
        $this->workTypes = Http::withHeaders([
            'Content-Type' => 'application/json'
        ])->post(env('PORTAL_URL') . '/orderendpoint', [
            'search' => 'test',
            'secret' => env('PORTAL_SECRET')
        ])->json()['work'];
    }

    public function addProject($projectId, $projectName)
    {
        $this->selectedProject = [
            'id' => $projectId,
            'name' => $projectName
        ];
        $this->search = '';

        $this->lastReports = Http::post(env('PORTAL_URL') . '/orderendpoint/return_work', [
            'order_id' => $projectId,
            'secret' => env('PORTAL_SECRET')
        ])->json()['work'];
    }

    public function submit()
    {
        $this->validate();

        $response = Http::post(env('PORTAL_URL') . '/orderendpoint/add_work', [
            'order_id' => $this->selectedProject['id'],
            'work_id' => $this->workType,
            'worker_email' => auth()->user()->email,
            'work_minutes' => $this->minutes,
            'work_comment' => $this->comment,
            'secret' => env('PORTAL_SECRET')
        ]);

        $response->status() == 404
            ? session()->flash('error', $response->json('error'))
            : session()->flash('success', 'Projekta atskaite iesniegta');


        $this->reset(['selectedProject', 'workType', 'minutes', 'comment', 'lastReports']);
    }

    public function render()
    {
        $projects = [];

        if (strlen($this->search) >= 2) {
            $projects = Http::withHeaders([
                'Content-Type' => 'application/json'
            ])->post(env('PORTAL_URL') . '/orderendpoint', [
                'search' => $this->search,
                'secret' => env('PORTAL_SECRET')
            ])->json()['orders'];
        }

        return view('livewire.project-hours-report', [
            'projects' => collect($projects)->take(10),
            'workTypes' => $this->workTypes
        ]);
    }
}
