<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ResignationDateForm extends Component
{
    public $contract;
    public $state_button;
    public $state_form;
    public $resignation_date;

    public function mount()
    {
        if ($this->contract->user->resignations->where('company_id', $this->contract->company_id)->first()) {
            $this->state_button = 'hidden';
            $this->state_form = 'block';
        } else {
            $this->state_button = 'block';
            $this->state_form = 'hidden';
        }
    }

    protected $rules = [
        'resignation_date' => 'required|after:today'
    ];

    protected $messages = [
        'resignation_date.required' => 'Jānorāda datums',
        'resignation_date.after' => 'Nedrīkst būt šodienas vai senāks datums'
    ];

    public function changeState()
    {
        $this->state_form == 'block' ? $this->state_form = 'hidden' : $this->state_form = 'block';
        $this->state_button == 'block' ? $this->state_button = 'hidden' : $this->state_button = 'block';
    }

    public function updated($date)
    {
        $this->validateOnly($date);
    }

    public function render()
    {
        return view('livewire.resignation-date-form');
    }
}
