<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Crypt;
use Livewire\Component;

class UserData extends Component
{
    public $phone;
    public $address_street;
    public $address_city;
    public $address_postal;
    public $personal_id;
    public $bank_account;
    public $successMessage;

    protected $rules = [
        'phone' => 'required',
        'address_street' => 'required',
        'address_city' => 'required',
        'address_postal' => 'required',
        'personal_id' => 'required',
        'bank_account' => 'required'
    ];

    public function mount()
    {
        $this->phone = auth()->user()->phone;
        $this->address_city = auth()->user()->address_city;
        $this->address_street = auth()->user()->address_street;
        $this->address_postal = auth()->user()->address_postal;
        $this->personal_id = !empty(auth()->user()->personal_id) ? Crypt::decryptString(auth()->user()->personal_id) : '';
        $this->bank_account = auth()->user()->bank_account;
    }

    public function updateUserData()
    {
        $userData = $this->validate();

        auth()->user()->update($userData);
        $this->successMessage = 'Dati atjaunoti';
    }

    public function render()
    {
        return view('livewire.user-data');
    }
}
