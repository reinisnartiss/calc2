<?php

namespace App\Http\Livewire;

use App\Models\Note;
use Livewire\Component;

class UserNotes extends Component
{
    public $name;
    public $value;

    protected $rules = [
        'name' => 'required|string|max:255',
        'value' => 'required|string|max:255'
    ];

    public function storeNote()
    {
        $note = $this->validate();

        auth()->user()->notes()->create($note);

        $this->reset(['name', 'value']);
    }

    public function deleteNote(Note $note)
    {
        $note->delete();
    }

    public function render()
    {
        return view('livewire.user-notes');
    }
}
