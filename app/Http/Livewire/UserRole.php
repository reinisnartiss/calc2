<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class UserRole extends Component
{
    public $user;
    public $managers;
    public $roles;
    public $role_id;
    public $manager;
    public $successMessage;

    public function mount()
    {
        $this->role_id = $this->user->roles()->pluck('id');
        if ($this->user->manager) {
            $this->manager = $this->user->manager->id;
        } else {
            $this->manager = 0;
        }
    }

    public function updateUserRole(User $user)
    {
        $user->manager_id = $this->manager;
        $user->save();
        $user->syncRoles($this->role_id);
        $this->successMessage = 'Dati atjaunoti';
    }

    public function render()
    {
        return view('livewire.user-role');
    }
}
