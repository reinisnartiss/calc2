<?php

namespace App\Http\Livewire;

use App\Models\Company;
use App\Models\User;
use Livewire\Component;

class UserVacationDaysIndex extends Component
{
    public $users;
    public $companies;

    public function mount()
    {
        $this->users = User::query()->where('is_blocked', false)->with('companies', 'vacations.companies')->get()->sortBy('name');
        $this->companies = Company::with('vacations', 'users')->get();
    }

    public function render()
    {
        return view('livewire.user-vacation-days-index');
    }
}
