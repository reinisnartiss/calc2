<?php

namespace App\Http\Livewire;

use Carbon\CarbonPeriod;
use Illuminate\Support\Arr;
use Livewire\Component;

class VacationRequestCreate extends Component
{
    public $user;
    public $start_date;
    public $end_date;
    public $unpaidLeave;
    public $companies;
    public $selectAll = true;
    public $selected;
    public $holidays = [];
    public $lastVacationDay;
    public $vacationDaysCount;

    public function mount()
    {
        $this->selectAll();
    }

    protected $rules = [
        'start_date' => 'required|after_or_equal:today',
        'end_date' => 'required|after_or_equal:start_date|after_or_equal:today',
    ];

    protected $messages = [
        'start_date.required' => 'Nepieciešams sākuma datums',
        'start_date.after_or_equal' => 'Sākuma datumam jābūt vēlākam vai vienādam ar šodienas datumu',
        'end_date.required' => 'Nepieciešams beigu datums',
        'end_date.after_or_equal' => 'Beigu datumam jābūt vēlākam vai vienādam ar sākuma datumu',
    ];

    public function createVacationPeriod()
    {
        $this->validate();
        $this->holidays = [];
        $this->lastVacationDay = null;
        $vacationPeriod = CarbonPeriod::create($this->start_date, $this->end_date);

        $vacationPeriod->filter(function ($date) {
            return $date->isWeekday();
        });

        foreach ($vacationPeriod as $vacationDay) {
            if ($vacationDay->isHoliday()) {
                $this->holidays = Arr::prepend($this->holidays, $vacationDay);
            }
        }
        $vacationPeriod->filter(function ($date) {
            return $date->isBusinessDay();
        });

        $this->vacationDaysCount = $vacationPeriod->count();

    }

    public function selectAll()
    {
        if ($this->selectAll == true) {
            $this->selected = $this->user->companies->pluck('id')->map(function ($id) {
                return (string)$id;
            });
        } else {
            $this->selected = [];
        }
    }

    public function render()
    {
        return view('livewire.vacation-request-create');
    }
}
