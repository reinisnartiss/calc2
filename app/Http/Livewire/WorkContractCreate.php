<?php

namespace App\Http\Livewire;

use App\Models\Company;
use Livewire\Component;

class WorkContractCreate extends Component
{
    public $user;
    public $companies;
    public $selected_company;
    public $contractPeriod;
    public $salaryKind;
    public $start_date;
    public $end_date;

    public function mount()
    {
        $this->companies = Company::all();
        $this->selected_company = 1;
        $this->contractPeriod = 1;
        $this->salaryKind = 1;
    }

    public function render()
    {
        return view('livewire.work-contract-create');
    }
}
