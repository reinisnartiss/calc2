<?php

namespace App\Http\Livewire;

use App\Models\Company;
use Livewire\Component;

class WorkContractEdit extends Component
{
    public $user;
    public $companies;
    public $contract;
    public $selected_company;
    public $contractPeriod;
    public $salaryKind;

    public function mount()
    {
        $this->companies = Company::all();
        $this->selected_company = $this->contract->company_id;
        $this->contractPeriod = $this->contract->contract_period;
        $this->salaryKind = $this->contract->salary_kind;
    }

    public function render()
    {
        return view('livewire.work-contract-edit');
    }
}
