<?php

namespace App\Http\Livewire;

use Carbon\CarbonPeriod;
use Livewire\Component;
use App\Services\Reports\EmployeeMonthlyReport;
use Illuminate\Support\Carbon;


class WorkReportIndex extends Component
{
    public $workReports;
    public $selectedMonth;
    public $monthName;
    public $allDates;

    public function mount()
    {
        $this->getWorkReports();
    }

    public function getWorkReports()
    {
        $report = EmployeeMonthlyReport::make()
            ->month(Carbon::parse($this->selectedMonth))
            ->formatMonth()
            ->monthName()
            ->workReports();

        $this->selectedMonth = $report->formattedMonth;
        $this->workReports = $report->workReports;
        $this->monthName = $report->monthName;
        $this->allDates = collect(CarbonPeriod::create(Carbon::parse($this->selectedMonth)->startOfMonth(),
            Carbon::parse($this->selectedMonth)->endOfMonth()));
    }

    public function render()
    {
        return view('livewire.work-report-index');
    }
}
