<?php

namespace App\Http\Livewire;

use App\Models\WorkReport;
use Livewire\Component;

class WorkReportUpdate extends Component
{
    public $workReport;
    public $reasons;
    public $reason;
    public $hours;
    public $comment;
    public $didWork;
    public $overtime;

    public function mount()
    {
        $this->comment = $this->workReport->comment;
        $this->reason = $this->workReport->reason_id;
        $this->hours = $this->workReport->work_hours;
        $this->didWork = $this->workReport->hasWorkHours;
        $this->overtime = $this->workReport->overtime;
    }


    public function checkOvertime()
    {
        $this->overtime = WorkReport::getOvertime($this->hours, $this->workReport->work_date);
    }


    public function render()
    {
        return view('livewire.work-report-update');
    }
}
