<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateWorkReport extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'startDate' => 'required',
            'endDate' => 'required|after_or_equal:startDate|before_or_equal:now',
        ];
    }

    public function messages()
    {
        return [
          'startDate.required' => 'Jānorāda sākuma datums',
          'endDate.required' => 'Jānorāda beigu datums',
          'endDate.after_or_equal' => 'Beigu datums nedrīkst būt agrāks par sākuma datumu',
          'endDate.before_or_equal' => 'Beigu datums nedrīkst būt vēlāks par šodienas datumu'
        ];
    }
}
