<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExportWorkOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_date' => 'required_if:same_as_contract,null',
            'same_as_contract' => 'sometimes',
            'order_number' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'order_date.required_if' => 'Jānorāda rīkojuma datums',
            'order_number.required' => 'Jānorāda rīkojuma numurs',
        ];
    }
}
