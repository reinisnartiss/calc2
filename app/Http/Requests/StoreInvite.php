<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInvite extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:invites,email|unique:users,email'
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'Lietotājs ar šādu e-pastu ir jau reģistrēts',
            'email.required' => 'Jāievada e-pasts',
        ];
    }
}
