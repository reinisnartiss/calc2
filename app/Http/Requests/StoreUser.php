<?php

namespace App\Http\Requests;

use App\Models\Invite;
use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Invite::where('email', request()->email)->exists();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email',
            'phone' => 'required',
            'address_street' => 'required',
            'address_city' => 'required',
            'address_postal' => 'required',
            'personal_id' => 'required',
            'bank_account' => 'required',
            'manager_id' => 'required|nullable',
            'password' => 'required_with:password_confirmation|string|confirmed',
            'password_confirmation' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Jānorāda vārds un uzvārds',
            'email.required' => 'Jānorāda e-pasts',
            'email.email' => 'Neatbilstošs e-pasts',
            'email.unique' => 'Lietotājs ar šādu e-pastu jau ir reģistrēts',
            'phone.required' => 'Jānorāda telefona numurs',
            'address_street.required' => 'Jānorāda adrese',
            'address_city.required' => 'Jānorāda pilsēta vai novads/pagasts',
            'address_postal.required' => 'Jānorāda pasta indekss',
            'personal_id.required' => 'Jānorāda personas kods',
            'bank_account.required' => 'Jānorāda bankas konts',
            'password.confirmed' => 'Paroles nesakrīt'
        ];
    }
}
