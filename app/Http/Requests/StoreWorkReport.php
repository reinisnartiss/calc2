<?php

namespace App\Http\Requests;

use App\Rules\CommentIsRequired;
use App\Rules\WorkReports\HoursIsRequired;
use App\Rules\WorkReports\ReasonIsRequired;
use App\Rules\WorkReports\WorkDateAfterInitialDate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreWorkReport extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'workReports' => 'required',
            'workReports.*.did_work' => 'required',
            'workReports.*.work_date' => ['required', new WorkDateAfterInitialDate()],
            'workReports.*.work_hours' => new HoursIsRequired(),
            'workReports.*.reason_id' => new ReasonIsRequired(),
            'workReports.*.comment' => new CommentIsRequired()
        ];
    }

}
