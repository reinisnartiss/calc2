<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'sometimes',
            'email' => 'sometimes',
            'phone' => 'sometimes',
            'address_street' => 'sometimes',
            'address_city' => 'sometimes',
            'address_postal' => 'sometimes',
            'personal_id' => 'sometimes',
            'bank_account' => 'sometimes'
        ];
    }
}
