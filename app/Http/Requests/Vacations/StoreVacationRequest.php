<?php

namespace App\Http\Requests\Vacations;

use App\Rules\Vacations\VacationAlreadyExists;
use Illuminate\Foundation\Http\FormRequest;

class StoreVacationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => ['required','after_or_equal:today', new VacationAlreadyExists()],
            'end_date' => ['required','after_or_equal:start_date', new VacationAlreadyExists()],
            'unpaid_leave' => 'sometimes',
            'user_id' => 'required',
            'selected_companies' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'start_date.after_or_equal' => 'Sākuma datumam ir jābūt šodienas datumam vai vēlākam',
            'end_date.after_or_equal' => 'Beigu datumam ir jābūt vienādam ar beigu datumu vai vēlākam',
            'selected_companies.required' => 'Jānorāda vismaz viens uzņēmums'
        ];
    }
}
