<?php

namespace App\Http\Requests\Vacations;

use Illuminate\Foundation\Http\FormRequest;

class UpdateVacationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date' => 'required|after_or_equal:today|before_or_equal:end_date',
            'end_date' => 'required|after_or_equal:start_date',
        ];
    }

    public function messages()
    {
        return [
            'start_date.after_or_equal' => 'Sākuma datumam ir jābūt šodienas datumam vai vēlākam',
            'start_date.before_or_equal' => 'Sākuma datumam ir jābūt vienādam ar beigu datumu vai agrākam',
            'end_date.after_or_equal' => 'Beigu datumam ir jābūt vienādam ar beigu datumu vai vēlākam',
        ];
    }
}
