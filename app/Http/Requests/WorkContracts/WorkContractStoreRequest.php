<?php

namespace App\Http\Requests\WorkContracts;

use App\Rules\UserCompanyContractExists;
use Illuminate\Foundation\Http\FormRequest;

class WorkContractStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_genitivs' => 'sometimes|nullable',
            'name_dativs' => 'sometimes|nullable',
            'name_akuzativs' => 'sometimes|nullable',
            'user_id' => 'sometimes',
            'contract_number' => 'required|unique:work_contracts,contract_number',
            'date' => 'required',
            'company_id' => ['required', new UserCompanyContractExists()],
            'position_nominativs' => 'required',
            'position_genitivs' => 'required',
            'position_akuzativs' => 'required',
            'position_id' => 'required',
            'contract_start' => 'required',
            'contract_start_month' => 'required',
            'contract_period' => 'required',
            'contract_end_date' => 'sometimes',
            'contract_end_date_month' => 'sometimes',
            'work_hours_type' => 'required',
            'weekly_hours' => 'required',
            'work_hours' => 'required',
            'salary_kind' => 'required',
            'salary' => 'required',
            'salary_written' => 'required',
            'salary_type' => 'required',
            'company' => 'sometimes'
        ];
    }

    public function messages()
    {
        return [
            'contract_number.unique' => 'Darba līgums ar šādu numuru jau eksistē'
        ];
    }
}
