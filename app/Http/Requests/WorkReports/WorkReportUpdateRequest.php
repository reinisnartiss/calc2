<?php

namespace App\Http\Requests\WorkReports;

use App\Models\WorkReport;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class WorkReportUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'work_hours' => Rule::requiredIf(!request()->filled('reason_id')),
            'reason_id' => [
                Rule::requiredIf(!request()->filled('work_hours')),
                'exists:reasons,id'
            ],
            'comment' => Rule::requiredIf($this->checkOvertime()),
        ];
    }

    private function checkOvertime()
    {
        if (request()->filled('reason_id')) {
            return false;
        }

        $overtime = WorkReport::getOvertime(request()->work_hours, request()->workReport->work_date);

        return $overtime > 0;
    }
}
