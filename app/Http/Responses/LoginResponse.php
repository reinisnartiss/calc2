<?php

namespace App\Http\Responses;

use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{

    public function toResponse($request)
    {
        return !Auth::user()->filledDataForm()
            ? redirect('user/profile')->with('status', 'Lūdzu aizpildi datus par sevi')
            : redirect()->intended(config('fortify.home'));
    }

}
