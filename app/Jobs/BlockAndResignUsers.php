<?php

namespace App\Jobs;

use App\Models\Resignation;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

class BlockAndResignUsers implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = User::whereHas('resignations', function ($query) {
            $query->where('resignation_date', Carbon::now()->format('Y-m-d'));
        })->get();

        foreach ($users as $user) {
            foreach ($user->resignations as $resignation)
                if ($user->companies()->count() == 1) {
                    $user->companies()->detach($resignation->company);
                    $user->contracts()->where('company_id', $resignation->company_id)->delete();
                    $user->is_blocked = true;
                    $user->save();
                    $resignation->delete();
                }
                if ($user->companies()->count() > 2) {
                    $user->companies()->detach($resignation->company);
                    $user->contracts()->where('company_id', $resignation->company_id)->delete();
                    $resignation->delete();
                }
        }
    }
}
