<?php

namespace App\Jobs;

use App\Models\Vacation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

class DeleteInactiveVacations implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        /* Delete unapproved vacations */
        Vacation::where('not_approved', true)->where('created_at', '<', Carbon::now()->subDays(7))->delete();
        /* Delete vacations which were not approved on time */
        Vacation::where('final_approval', false)->where('start_date', '<', Carbon::now())
            ->where('created_at', '<', Carbon::now()->subDays(7))->delete();
    }
}
