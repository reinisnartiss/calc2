<?php

namespace App\Mail;

use App\Models\Vacation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ManagerHasApprovedVacation extends Mailable
{
    use Queueable, SerializesModels;

    public $vacation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Vacation $vacation)
    {
        $this->vacation = $vacation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->vacation->user->name_genitivs ? $this->vacation->user->name_genitivs : $this->vacation->user->name . ' atvaļinājuma galējais apstiprinājums')
                ->view('emails.manager_has_approved_vacation');
    }
}
