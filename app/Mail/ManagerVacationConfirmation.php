<?php

namespace App\Mail;

use App\Models\Vacation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ManagerVacationConfirmation extends Mailable
{
    use Queueable, SerializesModels;


    public $newVacation;

    /**
     * Create a new message instance.
     *
     * @param Vacation $newVacation
     */
    public function __construct(Vacation $newVacation)
    {
        $this->newVacation = $newVacation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->newVacation->user->name_genitivs . ' atvaļinājuma pieprasījums')
            ->view('emails.manager_vacation_approve');
    }
}
