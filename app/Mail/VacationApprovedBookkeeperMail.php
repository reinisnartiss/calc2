<?php

namespace App\Mail;

use App\Models\Vacation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VacationApprovedBookkeeperMail extends Mailable
{
    use Queueable, SerializesModels;

    public $newVacation;

    /**
     * Create a new message instance.
     *
     * @param Vacation $newVacation
     */
    public function __construct(Vacation $newVacation)
    {
        $this->newVacation = $newVacation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Atvaļinājums apstiprināts - ' . $this->newVacation->user->name)->view('emails.new_approved_vacation');
    }
}
