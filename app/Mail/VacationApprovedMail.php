<?php

namespace App\Mail;

use App\Models\Vacation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VacationApprovedMail extends Mailable
{
    use Queueable, SerializesModels;

    public $vacation;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Vacation $vacation)
    {
        $this->vacation = $vacation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Atvaļinājuma pieprasījums')->view('emails.vacation_approved');
    }
}
