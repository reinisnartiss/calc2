<?php

namespace App\Mail;

use App\Models\Vacation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class VacationNotApprovedMail extends Mailable
{
    use Queueable, SerializesModels;


    public $vacation;

    /**
     * Create a new message instance.
     *
     * @param Vacation $vacation
     */
    public function __construct(Vacation $vacation)
    {
        $this->vacation = $vacation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Atvaļinājuma pieprasījums')->view('emails.vacation_not_approved');
    }
}
