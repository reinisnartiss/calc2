<?php

namespace App\Models;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;


    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot(['contract_number', 'starting_date', 'initial_vacation_days_available']);
    }

    public function workContracts()
    {
        return $this->hasMany(WorkContract::class);
    }

    public function director()
    {
        return $this->belongsTo(User::class)->first();
    }

    public function vacations()
    {
        return $this->belongsToMany(Vacation::class);
    }

    public function resignations()
    {
        return $this->belongsToMany(Resignation::class);
    }

    public function vacationManager()
    {
        return $this->belongsTo(User::class, 'vacation_manager');
    }

    public function getAvailableVacationDays(User $user)
    {
        $initialDays = $user->companies->where('id', $this->id)->first()->pivot->initial_vacation_days_available;

        $usedVacationDays = $this->vacations->where([
            'user_id' => $user->id,
            'manager_approval' => true,
            'final_approval' => true,
            'unpaid_leave' => false
        ])->sum('vacation_days');

        if ($initialDays != null) {
            $period = CarbonPeriod::create(new Carbon('2020-11-01'), Carbon::now());

            $vacationDays = floor( ($initialDays + ($period->count() * 20) / 365) - $usedVacationDays );

        } else {
            $period = CarbonPeriod::create($user->companies->where('id', $this->id)->first()->pivot->starting_date, Carbon::now());

            $vacationDays = floor( ( ($period->count() * 20) / 365) - $usedVacationDays );
        }

        return $vacationDays;
    }
}
