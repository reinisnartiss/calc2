<?php

namespace App\Models;

use Carbon\CarbonPeriod;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Crypt;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Jetstream\HasProfilePhoto;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;
    use CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'address_street',
        'address_city',
        'address_postal',
        'phone',
        'personal_id',
        'bank_account',
        'manager_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    private $userDataFormFields = [
        'address_city',
        'address_street',
        'address_postal',
        'phone',
        'personal_id',
        'bank_account',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function setPersonalIdAttribute($personalId)
    {
        $this->attributes['personal_id'] = Crypt::encryptString($personalId);
    }

    public function filledDataForm()
    {
        foreach ($this->userDataFormFields as $field) {
            if (empty($this->$field)) {
                return false;
            }
        }
        return true;
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class)->withPivot(['contract_number', 'starting_date', 'initial_vacation_days_available'])->withTimestamps();
    }

    public function contracts()
    {
        return $this->hasMany(WorkContract::class);
    }

    public function vacations()
    {
        return $this->hasMany(Vacation::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function invites()
    {
        return $this->hasMany(Invite::class);
    }

    public function resignations()
    {
        return $this->hasMany(Resignation::class);
    }

    public function getContractNumberAttribute()
    {
        return self::companies()->withPivot('contract_number');
    }

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id')->withDefault(['name' => '---']);
    }

    public function isVacationManager()
    {
        return Company::where('vacation_manager', $this->id)->exists();
    }

    public function employees()
    {
        return $this->hasMany(User::class, 'manager_id');
    }

    public function workReports()
    {
        return $this->hasMany(WorkReport::class);
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    public function getMonthsWorkReports()
    {
        return $this->hasMany(WorkReport::class)->whereMonth('work_date', Carbon::now()->month)->orderBy('work_date', 'asc');
    }

    public function getDailyEmployeeReport(Carbon $date)
    {
        if (!$this->workReports()->where('work_date', $date)->exists()) {
            return '---';
        }

        if ($this->workReports()->where('work_date', $date)->first()->reason_id) {
            return $this->workReports()->where('work_date', $date)->first()->reason->short_name;
        }

        return $this->workReports()->where('work_date', $date)->first()->work_hours;
    }

    public function getPeriodOvertime($startDate, $endDate)
    {
        $workReports = $this->workReports()->customPeriodWorkReports($startDate, $endDate);
        $overtime = 0;
        foreach ($workReports as $workReport) {
            $overtime += $workReport->getOvertime($workReport->work_hours, $workReport->work_date);
        }

        return $overtime;
    }

    public function hasResigned($company)
    {
        return Resignation::where('user_id', $this->id)->where('company_id', $company->id)->exists();
    }

    public function getPeriodWorkHoursSum($startDate, $endDate)
    {
        return $this->workReports()->whereBetween('work_date', [$startDate, $endDate])->sum('work_hours');
    }

    public function getUnapprovedVacations()
    {
        return $this->vacations()->where([
            ['manager_approval', false],
                ['final_approval', false],
            ])->get();
    }

    public function scopeBlockedUsers($query)
    {
        return $query->where('is_blocked', true);
    }

    public function getAvailableVacationDays(Company $company)
    {
        $initialDays = $this->companies->where('id', $company->id)->first()->pivot->initial_vacation_days_available;

        $usedVacationDays = $this->vacations
            ->filter(function ($vacation) use ($company) {
                return $vacation->companies->contains($company);
            })
            ->where('final_approval', true)->where('unpaid_leave', false)
            ->sum('vacation_days');

        if ($initialDays != null) {
            $period = CarbonPeriod::create(new Carbon('2020-11-01'), Carbon::now());

            $vacationDays = floor( ($initialDays + ($period->count() * 20) / 365) - $usedVacationDays );

        } else {
            $period = CarbonPeriod::create($this->companies->where('id', $company->id)->first()->pivot->starting_date, Carbon::now());

            $vacationDays = floor( ( ($period->count() * 20) / 365) - $usedVacationDays );
        }

        return $vacationDays;
    }

}
