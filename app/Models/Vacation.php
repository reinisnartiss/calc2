<?php

namespace App\Models;

use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Vacation extends Model
{
    use HasFactory;

    protected $fillable = ['start_date', 'end_date', 'user_id', 'unpaid_leave', 'vacation_days'];

    protected $dates = ['start_date', 'end_date'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }

    public function scopePaidVacations($query)
    {
        return $query->where('unpaid_leave', null)->get();
    }

    public function scopeFinalApprovalVacations($query)
    {
        return $query->where('manager_approval', true)
            ->where('final_approval', false)
            ->where('not_approved', false)
            ->whereDate('start_date', '>=', Carbon::now())
            ->get();
    }

    public function scopeManagerApprovalVacations($query)
    {
        return $query->where('manager_approval', false)
            ->where('final_approval', false)
            ->where('not_approved', false)
            ->whereDate('start_date', '>=', Carbon::now())
            ->orderBy('start_date')
            ->get();
    }

    public function scopeActiveVacations($query)
    {
        return $query->where('manager_approval', true)
            ->where('final_approval', true)
            ->where(function ($query) {
                $query->where('start_date', '<=', Carbon::now());
                $query->where('end_date', '>=', Carbon::now());
            })
            ->get();
    }

    public function scopeInactiveVacations($query)
    {
        return $query->where(function ($query) {
            return $query->where('not_approved', true)
                ->orWhere(function ($query) {
                    $query->where('start_date', '<', now()->format('Y-m-d'));
                    $query->where('final_approval', false);
                    $query->where('end_date', '>=', now()->subDays(30));
                })
                ->orWhere(function ($query) {
                    $query->where('end_date', '<', now());
                    $query->where('end_date', '>=', now()->subDays(30));
                });
        })->where('end_date', '>=', Carbon::now()->subDays(30))
            ->get();
    }

    public function scopeWithoutRejectedVacations($query)
    {
        return $query->where('not_approved', false);
    }

    public function status()
    {
        if ($this->not_approved == true) {
            return "Pieprasījums noraidīts";
        }
        if ($this->final_approval == false && Carbon::now()->format('Y-m-d') > $this->start_date) {
            return "Beidzies pieprasījuma termiņš";
        }
        if ($this->manager_approval == true && $this->final_approval == true && (CarbonPeriod::create($this->start_date, $this->end_date))->contains(now()->toDateString())) {
            return "Darbinieks ir atvaļinājumā";
        }
        if (Carbon::now()->toDateString() > $this->end_date) {
            return "Atvaļinājums beidzies";
        }
        if ($this->manager_approval == true && $this->final_approval == true) {
            return "Apstiprināts";
        }
        if ($this->manager_approval == true && $this->final_approval == false) {
            return "Gaida galējo apstiprinājumu";
        }
        if ($this->manager_approval == false && $this->final_approval == false) {
            return "Gaida vadītāja apstiprinājumu";
        }

        return "Gaida atbildi";
    }

    public function getBackgroundColorAttribute()
    {
        if ($this->not_approved == true) {
            return "#ffcfcf";
        }
        if ($this->final_approval == false && Carbon::now()->format('Y-m-d') > $this->start_date) {
            return "#ffcfcf";
        }
        if ($this->manager_approval == true && $this->final_approval == true && (CarbonPeriod::create($this->start_date, $this->end_date))->contains(now()->toDateString())) {
            return "#ffeabf";
        }
        if (Carbon::now()->toDateString() > $this->end_date) {
            return "#fbebfc";
        }
        if ($this->manager_approval == true && $this->final_approval == true) {
            return "#ddf7d0";
        }
        if ($this->manager_approval == true && $this->final_approval == false) {
            return "#b6defc";
        }
        if ($this->manager_approval == false && $this->final_approval == false) {
            return "#f9fac0";
        }

        return "blue";
    }
}
