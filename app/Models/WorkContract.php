<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkContract extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'contract_pdf',
        'order_pdf',
        'contract_number',
        'date',
        'company_id',
        'position_nominativs',
        'position_genitivs',
        'position_dativs',
        'position_akuzativs',
        'position_id',
        'contract_start',
        'contract_start_month',
        'contract_period',
        'contract_end_date',
        'contract_end_date_month',
        'work_hours_type',
        'weekly_hours',
        'work_hours',
        'salary_kind',
        'salary',
        'salary_written',
        'salary_type',
    ];

    protected $dates = [
        'date',
        'contract_start',
        'contract_end_date',
        'order_date'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function scopeActive($query)
    {
        return $query->where([
            ['is_printed_contract', false],
            ['is_printed_ordinance', false]
        ]);
    }
}
