<?php

namespace App\Models;

class WorkDay
{
    public $workReport;

    public static function make()
    {
        return new static;
    }

    public function workReport(WorkReport $workReport)
    {
        $this->workReport = $workReport;

        return $this;
    }

    public function getOvertime()
    {
        if (!$this->workReport->work_hours) {
            return 0;
        }

        if ($this->workReport->work_date->isWeekday() && $this->workReport->work_hours > config('app.dailyHours')) {
            return $this->workReport->work_hours - config('app.dailyHours');
        }

        if ($this->workReport->work_date->isWeekend()) {
            return $this->workReport->work_hours;
        }

        return 0;
    }
}
