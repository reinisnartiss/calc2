<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class WorkReport extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'did_work', 'work_date', 'work_hours', 'reason_id', 'comment'];

    protected $dates = [
        'work_date'
    ];

    const ALLOWED_REASONS = [2,3,6,7];

    public function reason()
    {
        return $this->belongsTo(Reason::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeAwayDays($query)
    {
        return $query->where('work_hours', null)->get();
    }

    public function scopeMonthlyWorkReports($query, $requestedMonth = null)
    {
        $month = !$requestedMonth ? Carbon::now()->month : Carbon::parse($requestedMonth)->month;

        $year = !$requestedMonth ? Carbon::now()->year : Carbon::parse($requestedMonth)->year;

        return $query
            ->whereYear('work_date', $year)
            ->whereMonth('work_date', $month)
            ->orderBy('work_date', 'asc');
    }

    public function scopeCustomPeriodWorkReports($query, $startDate, $endDate)
    {
        return $query->wherebetween('work_date', [$startDate, $endDate])->get();
    }

    public function scopeWorkDays($query)
    {
        return $query->where('work_hours', '>', 0);
    }

    public function getOvertimeAttribute()
    {
        return self::getOvertime($this->work_hours, $this->work_date);
    }

    public static function getOvertime($hours, Carbon $date)
    {
        if (!$hours) {
            return 0;
        }

        if ($date->isWeekday() && $hours > config('app.dailyHours')) {
            return $hours - config('app.dailyHours');
        }

        if ($date->isWeekend()) {
            return $hours;
        }

        return 0;
    }

    public function getHasWorkHoursAttribute()
    {
        return $this->work_hours > 0;
    }
}
