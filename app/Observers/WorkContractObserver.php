<?php

namespace App\Observers;

use App\Models\WorkContract;

class WorkContractObserver
{
    /**
     * Handle the WorkContract "created" event.
     *
     * @param  \App\Models\WorkContract  $workContract
     * @return void
     */
    public function created(WorkContract $workContract)
    {
        //
    }

    /**
     * Handle the WorkContract "updated" event.
     *
     * @param  \App\Models\WorkContract  $workContract
     * @return void
     */
    public function updated(WorkContract $workContract)
    {
        //
    }

    public function updating(WorkContract $workContract)
    {
        if($workContract->isDirty('company_id') || $workContract->isDirty('contract_start')){
            $workContract->user->companies()->updateExistingPivot($workContract->getOriginal('company_id'),
            [
                'company_id' => $workContract->company_id,
                'contract_number' => $workContract->contract_number,
                'starting_date' => $workContract->contract_start
            ]);
        }
    }

    /**
     * Handle the WorkContract "deleted" event.
     *
     * @param  \App\Models\WorkContract  $workContract
     * @return void
     */
    public function deleted(WorkContract $workContract)
    {
        //
    }

    /**
     * Handle the WorkContract "restored" event.
     *
     * @param  \App\Models\WorkContract  $workContract
     * @return void
     */
    public function restored(WorkContract $workContract)
    {
        //
    }

    /**
     * Handle the WorkContract "force deleted" event.
     *
     * @param  \App\Models\WorkContract  $workContract
     * @return void
     */
    public function forceDeleted(WorkContract $workContract)
    {
        //
    }
}
