<?php

namespace App\Providers;

use App\Models\WorkContract;
use App\Observers\WorkContractObserver;
use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;
use Cmixin\BusinessDay;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $baseList = 'lv-national'; // or region such as 'us-il'
        // You can add/remove days (optional):
        $additionalHolidays = [
        ];

        BusinessDay::enable('Illuminate\Support\Carbon', $baseList, $additionalHolidays);

        Carbon::setHolidays('lv-special', [
            'jaunais-gads' => '01-01',
            'liela-piektdiena' => '= easter -2',
            'lieldienas' => '= easter',
            'otras-lieldienas' => '= easter 1',
            'darba-svetki' => '05-01',
            'Latvijas Republikas Neatkarības atjaunošanas diena' => '= 05-04 substitute',
            'ligo-diena' => '06-23',
            'jani' => '06-24',
            'Latvijas Republikas proklamēšanas diena' => '= 11-18 substitute',
            'ziemassvetku-vakars' => '12-24',
            'pirmie-ziemassvetki' => '12-25',
            'otrie-ziemassvetki' => '12-26',
            'vecgada-vakars' => '12-31'
        ]);

        Carbon::setHolidayName('jaunais-gads', 'lv', 'Jaunais Gads');
        Carbon::setHolidayName('liela-piektdiena', 'lv', 'Lielā Piektdiena');
        Carbon::setHolidayName('lieldienas', 'lv', 'Lieldienas');
        Carbon::setHolidayName('otras-lieldienas', 'lv', 'Otrās Lieldienas');
        Carbon::setHolidayName('darba-svetki', 'lv', 'Darba Svētki');
        Carbon::setHolidayName('Latvijas Republikas Neatkarības atjaunošanas diena', 'lv', 'Latvijas Republikas neatkarības atjaunošanas diena');
        Carbon::setHolidayName('ligo-diena', 'lv', 'Līgo diena');
        Carbon::setHolidayName('jani', 'lv', 'Jāņi');
        Carbon::setHolidayName('Latvijas Republikas proklamēšanas diena', 'lv', 'Latvijas Republikas proklamēšanas diena');
        Carbon::setHolidayName('ziemassvetku-vakars', 'lv', 'Ziemassvētku Vakars');
        Carbon::setHolidayName('pirmie-ziemassvetki', 'lv', 'Pirmie Ziemassvētki');
        Carbon::setHolidayName('otrie-ziemassvetki', 'lv', 'Otrie Ziemassvētki');
        Carbon::setHolidayName('vecgada-vakars', 'lv', 'Vecgada Vakars');

        Carbon::setHolidaysRegion('lv-special');


        WorkContract::observe(WorkContractObserver::class);
    }
}
