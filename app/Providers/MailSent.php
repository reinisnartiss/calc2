<?php

namespace App\Providers;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MailSent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $to;
    public $from;
    public $subject;
    public $respone;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($mailTo, $mailFrom, $mailSubject, $response = null)
    {
        $this->to = $mailTo;
        $this->from = $mailFrom;
        $this->subject = $mailSubject;
        $this->respone = json_encode($response);
    }

}
