<?php

namespace App\Providers;

use App\Models\SentMail;
use App\Providers\MailSent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreOutgoingMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MailSent  $event
     * @return void
     */
    public function handle(MailSent $event)
    {
        $outgoingMail = new SentMail();

        $outgoingMail->to = $event->to;
        $outgoingMail->from = $event->from;
        $outgoingMail->subject = $event->subject;
        $outgoingMail->response = $event->respone;
        $outgoingMail->save();
    }
}
