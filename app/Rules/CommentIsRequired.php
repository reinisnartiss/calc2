<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;

class CommentIsRequired implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $workReport = request()->workReports[explode('.', $attribute)[1]];
        if (Carbon::parse($workReport['work_date'])->isBusinessDay() && $workReport['work_hours'] > 8 && $workReport['comment'] == null) {
            return false;
        }
        if (Carbon::parse($workReport['work_date'])->isHoliday() && $workReport['work_hours'] > 0 && $workReport['comment'] == null) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Lūdzu norādi iemeslu virsstundām';
    }
}
