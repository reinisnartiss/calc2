<?php

namespace App\Rules;

use App\Models\Company;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class UserCompanyContractExists implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::where('id', request()->user_id)->first();
        $company = Company::where('id', $value)->first();
        if ($user->companies->contains($company)) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Lietotājam jau ir noslēgts līgums ar šo uzņēmumu';
    }
}
