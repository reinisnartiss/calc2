<?php

namespace App\Rules\WorkReports;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;

class HoursIsRequired implements Rule
{
    public $workReport;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $workReport = request()->workReports[explode('.', $attribute)[1]];

        if($workReport['did_work'] == 1 && Carbon::parse($workReport['work_date'])->isWeekday() && ! Carbon::parse($workReport['work_date'])->isHoliday()) {
            if ($value > 0) {
                return true;
            }
            if (!$value) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Nav norādītas stundas';
    }
}
