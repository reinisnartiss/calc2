<?php

namespace App\Rules\WorkReports;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;

class ReasonIsRequired implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $workReport = request()->workReports[explode('.', $attribute)[1]];
        if (Carbon::parse($workReport['work_date'])->isBusinessDay() && $workReport['did_work'] == false && $workReport['reason_id'] == null) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Jānorāda iemesls';
    }
}
