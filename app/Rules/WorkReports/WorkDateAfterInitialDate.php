<?php

namespace App\Rules\WorkReports;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Carbon;

class WorkDateAfterInitialDate implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (auth()->user()->companies()->first() && auth()->user()->companies()->first(['starting_date'])->starting_date !== null) {
            return Carbon::parse($value)->gte(auth()->user()->companies()->orderBy('starting_date')->first()->pivot->starting_date);
        } else {
            return Carbon::parse($value)->gte(new Carbon('2020-11-01'));
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if (auth()->user()->companies()->first(['starting_date'])->starting_date !== null) {
            return 'Datums nevar būt senāks par darba uzsākšanas datumu';
        } else {
            return 'Datums nedrīkst būt senāks par 1. novembri';
        }
    }
}
