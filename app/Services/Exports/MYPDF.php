<?php


namespace App\Services\Exports;


use Elibyy\TCPDF\TCPDF;

class MYPDF extends \TCPDF
{
    public function Footer()
    {
        if ($this->page !== 1) {
            $this->SetY(-15);
            $this->SetX(-15);
            // Set font
            $this->SetFont('freeserif', '', 10);
            $this->SetTextColor(127);
            // Page number
            $this->Cell(0, 10, $this->getAliasNumPage(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }

    }
}
