<?php


namespace App\Services\Exports;


use App\Models\Company;
use App\Models\User;
use App\Models\Vacation;
use App\Models\WorkContract;
use Illuminate\Http\Request;
use TCPDF;

class PdfExport
{

    public static function make()
    {
        return new static;
    }

    public function vacationPdf(Request $request, Vacation $vacation, User $user)
    {
        $company = Company::where('id', $request->company_id)->first();
        if ($request->action === 'iesniegums') {
            $view = \View::make('vacations.iesniegumspdf', $request->all(), compact('user', 'vacation', 'company'));
            $html = $view->render();

            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetFont("freeserif", "", 11, "", "false");
            $pdf->SetTitle('Iesnigums_atvalinajumam');
            $pdf->setCellHeightRatio(1.0);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->SetMargins(30, 20, 20, true);
            $pdf->AddPage();
            $pdf->writeHTML($html, true, false, true, false, '');
            if ($request->unpaid_leave) {
                $pdf->Output('Iesniegums_atvalinajumam_bezalgas.pdf', 'D');
            } else {
                $pdf->Output('Iesniegums_atvalinajumam.pdf', 'D');
            }

        } else if ($request->action === 'rikojums') {
            $view = \View::make('vacations.rikojumspdf', $request->all(), compact('user', 'vacation', 'company'));
            $html = $view->render();

            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetFont("freeserif", "", 11, "", "false");
            $pdf->SetTitle('Rikojums_atvalinajumam');
            $pdf->setCellHeightRatio(1.0);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->SetMargins(30, 20, 10, true);
            $pdf->AddPage();
            $pdf->writeHTML($html, true, false, true, false, '');
            if ($request->unpaid_leave = true) {
                $pdf->Output('Rikojums_atvalinajumam_bezalgas.pdf', 'D');
            } else {
                $pdf->Output('Rikojums_atvalinajumam.pdf', 'D');
            }

        }
    }

    public function workContractPdf($workContract)
    {
        $view = \View::make('work_contract.contractpdf', compact('workContract'));
        $html = $view->render();

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetFont("freeserif", "", 11, "", "false");
        $pdf->SetTitle('Darba līgums');
        $pdf->setCellHeightRatio(1.13);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(true);
        $pdf->SetMargins(30, 18,20, true);
        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->Output('Darba_ligums_Nr_' . request()->contract_number . '.pdf', 'D');
    }

    public function ordinancePdf(WorkContract $workContract)
    {
        $view = \View::make('work_contract.ordinancepdf', compact('workContract'));
        $html = $view->render();

        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetFont("freeserif", "", 11, "", "false");
        $pdf->SetTitle('Rīkojums');
        $pdf->setCellHeightRatio(1.3);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->SetMargins(30, 20,10, true);
        $pdf->AddPage();
        $pdf->writeHTML($html, true, false, true, false, '');

        if ($workContract->salary_kind == 1) {
            $pdf->Output('Rikojums_pienemsana_menesalga.pdf', 'D');
        } else {
            $pdf->Output('Rikojums_pienemsana_stundu_likme.pdf', 'D');
        }
    }
}
