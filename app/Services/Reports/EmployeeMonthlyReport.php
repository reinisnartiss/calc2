<?php

namespace App\Services\Reports;

use Illuminate\Support\Carbon;

class EmployeeMonthlyReport
{
    public $workReports;
    public $month;
    public $formattedMonth;
    public $overtime = 0;
    public $monthName;


    public static function make()
    {
        return new static;
    }

    public function month(Carbon $month)
    {
        $this->month = $month;
        return $this;
    }

    public function formatMonth()
    {
        $this->formattedMonth = $this->month->format('Y-m');
        return $this;
    }

    public function monthName()
    {
        $this->monthName = ucfirst(Carbon::parse($this->month)->locale('lv')->monthName);
        return $this;
    }

    public function workReports($user = null)
    {
        $selectedUser = !$user ? auth()->user() : $user;
        $this->workReports = $selectedUser->workReports()->monthlyWorkReports($this->month)->get();
        return $this;
    }

    public function overtime()
    {
        $this->overtime = $this->workReports->sum('overtime');
        return $this;
    }
}
