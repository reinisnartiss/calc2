<?php


namespace App\Services\Reports;

use Carbon\CarbonPeriod;


class EmployeePeriodReport
{
    public $startData;
    public $endDate;
    public $period;
    public $workDays;
    public $awayDays;
    public $workHours;
    public $overtime = 0;

    public function make()
    {
        return new static;
    }

    public function period($startDate, $endDate)
    {
        $this->period = CarbonPeriod::create($startDate, $endDate);
        return $this;
    }

}
