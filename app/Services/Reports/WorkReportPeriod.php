<?php


namespace App\Services\Reports;


use App\Models\WorkReport;
use Carbon\CarbonPeriod;

class WorkReportPeriod
{

    public static function make()
    {
        return new static;
    }

    public function period($startDate, $endDate, $weekends = null)
    {
        $newPeriod = CarbonPeriod::create($startDate, $endDate);

        if ($weekends == null) {
            $newPeriod->filter(function ($date) {
                return $date->isBusinessDay();
            });
        }

        $filteredPeriod = collect($newPeriod)->filter(function ($date) {
        return !WorkReport::where('user_id', auth()->user()->id)
            ->whereDate('work_date', $date)
            ->exists();
        });

        return $filteredPeriod->map(function ($date) {
            $workReport = new WorkReport();
            $workReport->work_date = $date;

            return $workReport;
        });
    }

}
