<?php


namespace App\Services\Vacations;


use App\Models\WorkReport;
use App\Services\Reports\WorkReportPeriod;

class CreateVacationWorkReports
{
    public static function make()
    {
        return new static;
    }

    public function vacationWorkReports($vacation)
    {
        $periodWorkReports = WorkReportPeriod::make()->period($vacation->start_date, $vacation->end_date);

        foreach ($periodWorkReports as $workReport) {
            WorkReport::create([
                'user_id' => $vacation->user->id,
                'did_work' => false,
                'work_date' => $workReport->work_date,
                'work_hours' => null,
                'reason_id' => $vacation->unpaid_leave == true ? 4 : 1,
                'comment' => null
            ]);
        }

    }
}
