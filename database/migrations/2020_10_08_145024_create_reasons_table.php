<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reasons', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('short_name');
            $table->timestamps();
        });

        DB::table('reasons')->insert(array('name' => 'Apmaksāts atvaļinājums', 'short_name' => 'AA'));
        DB::table('reasons')->insert(array('name' => 'Slimības lapa A', 'short_name' => 'SA'));
        DB::table('reasons')->insert(array('name' => 'Slimības lapa B', 'short_name' => 'SB'));
        DB::table('reasons')->insert(array('name' => 'Bezalgas atvaļinājums', 'short_name' => 'AB'));
        DB::table('reasons')->insert(array('name' => 'Mācību atvaļinājums', 'short_name' => 'AM'));
        DB::table('reasons')->insert(array('name' => 'Bērnu kopšanas atvaļinājums', 'short_name' => 'AK'));
        DB::table('reasons')->insert(array('name' => 'Paternitātes atvaļinājums', 'short_name' => 'AP'));
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reasons');
    }
}
