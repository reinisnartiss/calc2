<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name');
            $table->unsignedInteger('director_id')->nullable();
            $table->string('director_role_nominativs')->nullable();
            $table->string('director_role_dativs')->nullable();
            $table->string('address');
            $table->string('bank_account')->nullable();
            $table->string('registration_number')->unique();
            $table->string('VAT_number')->nullable();
            $table->timestamps();

            $table->foreign('director_id')->references('id')->on('users');
        });

        Schema::create('company_user', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedTinyInteger('company_id');
            $table->string('contract_number')->nullable();
            $table->date('starting_date')->nullable();
            $table->float('initial_vacation_days_available')->nullable();
            $table->timestamps();

            $table->unique(['user_id', 'company_id']);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
