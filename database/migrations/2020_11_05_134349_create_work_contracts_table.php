<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_contracts', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('user_id');
            $table->boolean('is_printed_contract')->default(0);
            $table->boolean('is_printed_ordinance')->default(0);
            $table->string('contract_number')->unique()->nullable();
            $table->string('order_number')->unique()->nullable();
            $table->date('order_date')->nullable();
            $table->date('date')->nullable();
            $table->unsignedTinyInteger('company_id');
            $table->string('position_nominativs')->nullable();
            $table->string('position_genitivs')->nullable();
            $table->string('position_dativs')->nullable();
            $table->string('position_akuzativs')->nullable();
            $table->string('position_id')->nullable();
            $table->date('contract_start')->nullable();
            $table->string('contract_start_month')->nullable();
            $table->integer('contract_period')->nullable();
            $table->date('contract_end_date')->nullable();
            $table->string('contract_end_date_month')->nullable();
            $table->integer('work_hours_type')->nullable();
            $table->integer('weekly_hours')->nullable();
            $table->integer('work_hours')->nullable();
            $table->integer('salary_kind')->nullable();
            $table->integer('salary')->nullable();
            $table->string('salary_written')->nullable();
            $table->integer('salary_type')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_contracts');
    }
}
