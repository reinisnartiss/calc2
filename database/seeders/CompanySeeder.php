<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            'name' => 'SIA Dizaina studija TEIKA',
            'address' => 'Aleksandra Čaka iela 128, Rīga, LV-1012',
//            'bank_account' => 'LV00HABA000000000001',
            'registration_number' => 40203013103,
            'VAT_number' => 'LV40203013103'
        ]);
        DB::table('companies')->insert([
            'name' => 'SIA MYPRINT',
            'address' => 'Aleksandra Čaka iela 128, Rīga, LV-1012',
//            'bank_account' => 'LV00HABA000000000002',
            'registration_number' => 40203192335,
            'VAT_number' => 'LV40203192335'
        ]);
        DB::table('companies')->insert([
            'name' => 'SIA Parrot Furniture',
            'address' => 'Aleksandra Čaka iela 128, Rīga, LV-1012',
//            'bank_account' => 'LV00HABA000000000003',
            'registration_number' => 40203193468,
            'VAT_number' => 'LV40203193468'
        ]);
        DB::table('companies')->insert([
            'name' => 'SIA Parrot Telpa',
            'address' => 'Aleksandra Čaka iela 128, Rīga, LV-1012',
//            'bank_account' => 'LV00HABA000000000004',
            'registration_number' => 40103902262,
            'VAT_number' => 'LV40103902262'
        ]);
        DB::table('companies')->insert([
            'name' => 'SIA PRINTPIGEONS',
            'address' => 'Aleksandra Čaka iela 128, Rīga, LV-1012',
//            'bank_account' => 'LV00HABA000000000005',
            'registration_number' => 40203201572,
            'VAT_number' => 'LV40203201572'
        ]);
        DB::table('companies')->insert([
            'name' => 'SIA Tu esi tas kas Tu esi',
            'address' => 'Biķernieku iela 11, Rīga, LV-1039',
//            'bank_account' => 'LV00HABA000000000005',
            'registration_number' => 44103142635,
            'VAT_number' => 'LV44103142635'
        ]);
        DB::table('companies')->insert([
            'name' => 'SIA SISTĒMU UZTURĒŠANA',
            'address' => 'Aleksandra Čaka iela 128, Rīga, LV-1012',
//            'bank_account' => 'LV00HABA000000000005',
            'registration_number' => 40103362552,
            'VAT_number' => 'LV40103362552'
        ]);
        DB::table('companies')->insert([
            'name' => 'SIA Parrot Reklāma',
            'address' => 'Bajāru iela 67, Rīga, LV-1006',
//            'bank_account' => 'LV00HABA000000000005',
            'registration_number' => 40103501827,
            'VAT_number' => 'LV40103501827'
        ]);
        DB::table('companies')->insert([
            'name' => 'SIA Parrot Services',
            'address' => 'Kalnciema iela 97-27, Rīga, LV-1046',
//            'bank_account' => 'LV00HABA000000000005',
            'registration_number' => 40203178740,
            'VAT_number' => 'LV40203178740'
        ]);
        DB::table('companies')->insert([
            'name' => 'SIA FFS',
            'address' => 'Ausekļa prospekts 1-51, Ogre, Ogres nov., LV-5001',
//            'bank_account' => 'LV00HABA000000000005',
            'registration_number' => 40203233008,
            'VAT_number' => 'LV40203233008'
        ]);
        DB::table('companies')->insert([
            'name' => 'SIA DS Teika',
            'address' => 'Biķernieku iela 11, Rīga, LV-1039',
//            'bank_account' => 'LV00HABA000000000005',
            'registration_number' => 40203237457,
            'VAT_number' => 'LV40203237457'
        ]);
    }
}
