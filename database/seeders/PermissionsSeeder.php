<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'add users']);
        Permission::create(['name' => 'view users']);
        Permission::create(['name' => 'delete user']);
        Permission::create(['name' => 'edit users']);
        Permission::create(['name' => 'get reports']);
        Permission::create(['name' => 'accept requests']);
        Permission::create(['name' => 'approve vacations']);

        $role1 = Role::create(['name' => 'darbinieks']);
        $role1->givePermissionTo('edit users');

        $role2 = Role::create(['name' => 'grāmatvedis']);
        $role2->givePermissionTo('view users');
        $role2->givePermissionTo('get reports');
        $role2->givePermissionTo('edit users');

        $role2 = Role::create(['name' => 'vadītājs']);
        $role2->givePermissionTo('view users');
        $role2->givePermissionTo('edit users');
        $role2->givePermissionTo('get reports');
        $role2->givePermissionTo('accept requests');

        Role::create(['name' => 'super-admin']);
    }
}
