<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'password' => Hash::make(123123123),
            'manager_id' => 6,
            'phone' => '12345678',
            'address' => 'Kr. Valdemāra iela 23-5',
            'personal_id' => '123456-12345',
            'bank_account' => 'LV89HABA01234570714327'
        ]);
        DB::table('users')->insert([
            'name' => 'Mike Oxlong',
            'email' => 'mike@example.com',
            'password' => Hash::make(123123123),
            'manager_id' => 6,
            'phone' => '12379328',
            'address' => 'Tērbatas iela 29',
            'personal_id' => '129392-11889',
            'bank_account' => 'LV89HABA02039484327'
        ]);
        DB::table('users')->insert([
            'name' => 'Boss',
            'email' => 'boss@example.com',
            'password' => Hash::make(123123123),
            'manager_id' => 6,
            'phone' => '12379328',
            'address' => 'Kalnciema iela 33-99',
            'personal_id' => '121232-14789',
            'bank_account' => 'LV89HABA020393834527'
        ]);
        DB::table('users')->insert([
            'name' => 'Random Name',
            'email' => 'random@example.com',
            'password' => Hash::make(123123123),
            'manager_id' => 3,
            'phone' => '19247932',
            'address' => 'Barona kvartāls 22',
            'personal_id' => '102983-91823',
            'bank_account' => 'LV89HABA12837459890'
        ]);
        DB::table('users')->insert([
            'name' => 'Cool Guy',
            'email' => 'cool@example.com',
            'password' => Hash::make(123123123),
            'manager_id' => 3,
            'phone' => '92178344',
            'address' => 'A.Čaka iela 128',
            'personal_id' => '102389-09458',
            'bank_account' => 'LV89HABA045986906456'
        ]);
    }
}
