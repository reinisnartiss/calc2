<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <title>Stundu uzskaites sistēma</title>
    <style>
        html * {
            font-family: Nunito, sans-serif;
        }
        .mainDiv {
            margin: 0.5rem;
            padding: 1rem;
            border: 1px solid rgba(209, 213, 219, 1);
            border-radius: 0.5rem;
            background-color: rgba(255, 255, 255, 1);
        }
        .logo {
            text-align: center;
            padding: 1.5rem;
        }
        .alert {
            font-size: 2rem;
            line-height: 1.75rem;
            margin-bottom: 4rem;
        }
        a {
            text-decoration: underline;
            color: rgba(55, 65, 81, 1);
        }
        .comment {
            margin-bottom: 1.5rem;
        }
        .body {
            margin-bottom: 3rem;
        }
        .thankYou {
            margin-bottom: 0.25rem;
        }
        .cheer {
            color: rgba(107, 114, 128, 1);
            font-size: 0.875rem;
            line-height: 1.25rem;
        }


    </style>

<body>
    <div class="mainDiv">
        <div class="logo">
            <img class="w-12 rounded-lg" src="https://parrot.lv/wp-content/uploads/2016/07/cropped-Parrot-logo-header-01-1.png" alt="Parrot logo">
        </div>

        <h1 class="alert">Hey!</h1>

        <h2 class="comment">Izskatās, ka esi kļuvis daļa no mūsu Komandas!</h2>

        <p class="body"><a href="{{ route('users.create', $invite->token) }}">Spied šeit</a>,
            lai reģistrētos Parrot Group stundu uzskaites un atvaļinājumu sistēmā.</p>

        <h3 class="thankYou">Paldies Tev</h3>
        <h4 class="cheer">#maucam</h4>
    </div>

</body>
</html>
