<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <title>{{ $newVacation->user->name_genitivs }} atvaļinājuma pieprasījums</title>
    <style>
        html * {
            font-family: Nunito, sans-serif;
        }
        .mainDiv {
            margin: 0.5rem;
            padding: 1rem;
            border: 1px solid rgba(209, 213, 219, 1);
            border-radius: 0.5rem;
            background-color: rgba(255, 255, 255, 1);
        }
        .logo {
            text-align: center;
            padding: 1.5rem;
        }
        .alert {
            font-size: 2rem;
            line-height: 1.75rem;
            margin-bottom: 4rem;
        }
        a {
            text-decoration: underline;
            color: rgba(55, 65, 81, 1);
        }
        .body {
            margin-bottom: 3rem;
        }
        .thankYou {
            margin-bottom: 0.25rem;
        }

    </style>

<body>
<div class="mainDiv">
    <div class="logo">
        <img class="w-12 rounded-lg" src="https://parrot.lv/wp-content/uploads/2016/07/cropped-Parrot-logo-header-01-1.png" alt="Parrot logo">
    </div>

    <h1 class="alert">Hey!</h1>

    <p class="body">{{ $newVacation->user->name }} ir izveidojis/-usi jaunu atvaļinājuma pieprasījumu,
        lūdzu nospied <a href="{{ route('vacations.show', $newVacation) }}">šeit</a>, lai pārietu
        uz Parrot Group uzskaites sistēmu un apstiprinātu to!</p>

    <h3 class="thankYou">Paldies</h3>

</div>

</body>
</html>

