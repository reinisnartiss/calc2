<x-app-layout>

    <x-slot name="slot">

        <div class="flex justify-center flex-wrap mt-6 mb-24 rounded-lg">
            <div class="container sm:p-0 p-0 md:p-6 w-full block">

                <div x-data="{ tab: '#newReport' }" class="">

                    <div class="flex flex-row justify-center">

                        <a class="px-4 text-center mr-6 border-b-2 border-gray-900 hover:border-blue-400"
                           href="#" x-on:click.prevent="tab='#newReport'">Jauna atskaite</a>

                        <a class="px-4 text-center mr-6 border-b-2 border-gray-900 hover:border-blue-400"
                           href="#" x-on:click.prevent="tab='#allUsers'">Visi mēneša ieraksti</a>

                        <a class="px-4 text-center border-b-2 border-gray-900 hover:border-blue-400"
                           href="#" x-on:click.prevent="tab='#userVacations'">Uzkrātās atvaļinājuma dienas</a>

                    </div>

                    <div x-show="tab == '#newReport'" x-cloak>
                        @livewire('employee-report-create')
                    </div>

                    <div x-show="tab == '#allUsers'" x-cloak>
                        @livewire('daily-inputs-index')
                    </div>

                    <div x-show="tab == '#userVacations'" x-cloak>
                        @livewire('user-vacation-days-index')
                    </div>

                </div>

            </div>
        </div>

    </x-slot>

</x-app-layout>
