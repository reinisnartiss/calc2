<table>
    <thead>
    <tr>
        <th style="text-align: left; font-size: 14px">Atskaite par periodu</th>
    </tr>
    <tr>
        <td style="text-align: left; font-size: 14px">{{$startDate}} / {{$endDate}}</td>
    </tr>
    <tr></tr>

    <tr>
        <th style="text-decoration: underline">Name</th>
        @foreach($period as $date)
            <th style="{{ $date->isWeekend() ? 'color: #d90000' : '' }}">{{ $date->format('d-m') }}</th>
        @endforeach
        <th style="background-color: #BBF1BA">KOPĀ</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td style="font-size: 13px">{{  $user->name }}</td>

            @foreach($period as $date)
                <td style="text-align: center; font-size: 13px">{{ $user->getDailyEmployeeReport(\Illuminate\Support\Carbon::parse($date)) }}</td>
            @endforeach
            <td style="background-color: #e5fae4; font-size: 13px">{{ $user->getPeriodWorkHoursSum($startDate, $endDate) }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
