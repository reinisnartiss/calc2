<x-app-layout>

<x-slot name="slot">

    @if (session('fail'))
            <div class="mx-auto bg-white bg-opacity-75 sm:w-3/4 lg:w-1/3 p-2 mt-6 text-center border border-red-200 shadow rounded-lg">
                {{ session('fail') }}
            </div>
    @elseif (session('success'))
            <div class="mx-auto bg-white bg-opacity-75 sm:w-3/4 lg:w-1/3 p-2 mt-6 text-center border border-green-200 shadow rounded-lg">
                {{ session('success') }}
            </div>
    @endif
    <div class="mt-6 rounded-lg">
        <div class="mx-auto rounded-lg w-full sm:w-3/4 md:w-3/4 lg:w-1/2"  style="min-width: 300px">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight text-center p-6">
                    {{ __('Nosūtīt jaunu uzaicinājumu') }}
                </h2>
            <div>
                <form action="{{ route('invite.store') }}" method="post">
                    @csrf
                    <div class="flex justify-center flex-wrap">
                            <input type="email" name="email" class="form-input rounded-md shadow-sm w-4/5 mr-4" required>

                            <x-jet-button class="mt-4 md:mt-0">
                                {{ __('Nosūtīt') }}
                            </x-jet-button>
                    </div>
                    @error('email')<p class="text-red-600 text-sm text-center"> {{ $message }} </p>@enderror
                </form>
            </div>
        </div>
        <div class="mx-auto rounded-lg w-full lg:w-3/4">
            <div>
                <h2 class="font-semibold text-xl text-gray-800 leading-tight text-center p-6">
                    {{ __('Aktīvie uzaicinājumi') }}
                </h2>
            </div>
            <div class="p-6 bg-white border border-gray-200 rounded-none md:rounded-lg w-full">
                @forelse($invites as $invite)
                    <div class="border border-gray-200 shadow p-4 rounded-lg mb-2 block md:grid grid-cols-4 relative">
                        <div class="text-center mb-2 col-span-1 self-center">
                            <span class="text-sm text-gray-800 block mb self-center">E-pasts:</span>
                            <span>{{ $invite->email }}</span>
                        </div>
                        <div class="text-center mb-2 col-span-1 self-center">
                            <span class="text-sm text-gray-800 block mb self-center">Uzaicināja:</span>
                            <span>{{ $invite->user()->name }}</span>
                        </div>
                        <div class="text-center mb-2 col-span-1 self-center">
                            <span class="text-sm text-gray-800 block mb self-center">Kad:</span>
                            <span>{{ $invite->created_at->locale('lv')->diffForHumans() }}</span>
                        </div>
                        <div class="text-center mb-2 col-span-1 self-center">
                            <a href="{{ route('invite.resend', $invite->id) }}">
                                <button type="button" class="inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                    Atkārtot uzaicinājumu
                                </button>
                            </a>
                        </div>
                        <form action="{{ route('invite.destroy', $invite->id) }}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" onclick="return confirm('Vai vēlies izdzēst ielūgumu?')" class="absolute top-0 left-0 m-2 text-gray-400 text-opacity-50 hover:text-opacity-100 hover:text-gray-600"><i class="fas fa-trash"></i></button>
                        </form>
                    </div>
                @empty
                    <div class="border border-gray-200 shadow p-4 rounded-lg mb-2">
                        Nav aktīvu uzaicinājumu
                    </div>
                @endforelse
            </div>
        </div>

    </div>

</x-slot>

</x-app-layout>
