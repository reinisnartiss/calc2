<div>
    <div class="text-center p-6">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight mt-6">
            {{ __('Mēneša atskaite') }}
        </h2>
    </div>
    <div class="text-center mb-6">
        <label for="selectedMonth" class="mr-4">Par mēnesi</label>
        <input type="month" id="selectedMonth" name="selectedMonth" wire:model="selectedMonth" wire:change="getAllData" class="form-input">
        <img wire:loading alt="Loading circle" src="{{ asset('svg/Rolling-1s-200px.gif') }}" width="30" class="ml-2">
    </div>
    <div class="p-6 bg-white border border-gray-200 rounded-none md:rounded-lg w-full">
        @foreach ($allDates as $date)
            <div x-data={show:false} class="border rounded-lg mb-2 shadow">
                <div
                    class="grid grid-cols-1 md:grid-cols-1 lg:grid-cols-12 text-center items-center overflow-hidden {{ $date->isWeekend() ? 'border-red-300 text-red-600' : 'border-gray-200' }}">
                    <div class="col-span-2 overflow-hidden my-4 md:my-4 text-lg">
                        <div>{{ $date->day }}. {{$date->locale('lv')->shortMonthName}}</div>
                        <div class="text-sm text-gray-800">{{ $date->locale('lv')->dayName }}</div>
                    </div>

                    <div class="col-span-8 overflow-hidden block md:grid md:grid-cols-3">
                        <div class="col-span-1">
                            <div class="text-sm text-gray-800">
                                Ieraksti
                            </div>
                            {{ $monthsData[$date->format('Y-m-d')]['work_report_count'] }}
                        </div>
                        <div class="col-span-1">
                            <div class="text-sm text-gray-800 {{ $date->isWeekend() ? 'hidden' : '' }}">
                                Prombūtnes
                            </div>
                            <div class="{{ $date->isWeekend() ? 'hidden' : '' }}">
                                {{ $monthsData[$date->format('Y-m-d')]['work_report_away'] }}
                            </div>
                        </div>
                        <div class="col-span-1">
                            <div class="text-sm text-gray-800">
                                Darbinieki ar virsstundām
                            </div>
                            <div class="">
                                {{ $monthsData[$date->format('Y-m-d')]['work_report_overtime'] }}
                            </div>
                        </div>
                    </div>
                    <div class="col-span-2 mb-4 lg:mb-0">
                        <button @click="show=!show" type="button"
                                class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                            Skatīt
                        </button>
                    </div>
                </div>
                <div x-show="show" class="border-t p-4 bg-gray-100 bg-opacity-25">
                    @foreach($monthsData[$date->format('Y-m-d')]['data'] as $workReport => $data)
                        <div class="py-2 block md:grid md:grid-cols-4 text-center {{ $loop->count = 1 ? '' : 'border-b' }}">
                            <div class="col-span-1">
                                {{ $data['user'] }}
                            </div>
                            <div class="col-span-1">
                                @if($data['work_hours'] != null)
                                    <p>Nostrādāts:</p>
                                @else
                                    <p>Prombūtne:</p>
                                @endif
                            </div>
                            <div class="col-span-1">
                                @if($data['work_hours'] != null)
                                    <p class="{{ $date->isWeekend() || $data['work_hours'] > 8 ? 'text-red-600' : '' }}">{{ $data['work_hours'] }} stundas</p>
                                @else
                                    <p>{{ $data['reason'] != null ? $data['reason'] : '' }}</p>
                                @endif
                            </div>
                            <div class="col-span-1">
                                @if($data['comment'] != null)
                                    <p class="text-red-600">{{ $data['comment'] }}</p>
                                @else
                                    ---
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

        @endforeach
    </div>
</div>
