<div>
    <div class="md:p-6 mt-8 md:m-12 text-center bg-white rounded-none md:rounded-lg border border-gray-200">
        <form action="{{ route('employee-report.export') }}" method="get">
            @csrf
            <div class="m-4">
                <p>Jauna atskaite par periodu</p>
            </div>

            <div class="block md:flex lg:flex justify-center items-start">
                <div class="inline-block">
                    <div class="mb-4">
                        <div class="text-center block md:inline-block lg:inline-block lg:mr-2">
                            <label for="startDate" class="mr-1 block md:inline-block lg:inline-block">No:</label>
                            <input class="p-1 border border-gray-200 rounded-lg" wire:model.lazy="startDate"
                                   required type="date" name="startDate" id="start-date"
                                   value="{{ \Carbon\Carbon::now()->firstOfMonth()->toDateString() }}">
                            @error('startDate')<p class="text-sm text-red-600">{{ $message }}</p>@enderror
                        </div>
                        <div class="text-center md:inline-block lg:inline-block inline-block md:ml-2 mt-2 md:mt-0">
                            <label for="endDate" class="mr-1 block md:inline-block lg:inline-block">Līdz:</label>
                            <input class="p-1 border border-gray-200 rounded-lg" wire:model.lazy="endDate"
                                   required type="date" name="endDate" id="end-date"
                                   value="{{ \Carbon\Carbon::now()->toDateString() }}">
                            @error('endDate')<p class="text-sm text-red-600">{{ $message }}</p>@enderror
                        </div>
                    </div>
                </div>
                <div class="block md:inline-block lg:inline:block ml-6 mb-6 md:mb-0">
                    <button type="submit"
                            class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                        Tālāk
                    </button>
                </div>
                <img wire:loading alt="Loading circle" src="{{ asset('svg/Rolling-1s-200px.gif') }}" width="30" class="ml-2">
            </div>
            <div class="p-0 md:p-2 lg:p-6">
                <div class="block md:grid md:grid-cols-6">
                    <div class="col-span-1 text-gray-800 text-sm ml-1 flex justify-center">
                        <input type="checkbox" id="select-all" name="select-all" class="form-checkbox p-3 mr-2"
                               wire:model="selectAll" wire:click="selectAll()">
                        <label for="select-all" class="block">Atzīmēt visus</label>
                    </div>
                    <div class="col-span-1"></div>

                    @foreach($tableHeaders as $header)
                        <div class="col-span-1 text-gray-800 text-sm mb-6">
                            <p class="hidden md:block">{{ $header }}</p>
                        </div>
                    @endforeach
                    <div
                        class="col-span-6 block border-t md:border border-gray-200 rounded-none md:rounded-lg m-0 md:m-2">
                        @foreach($users as $user)
                            <div class=" md:grid md:grid-cols-6 p-4 border-gray-200 shadow-sm {{ $loop->last ? '' : 'border-b' }}">
                                <div class="flex justify-left items-center md:grid grid-cols-2 md:col-span-2">
                                    <div class="grid-cols-1">
                                        <input type="checkbox" class="form-checkbox mr-4 p-3" wire:model="selected"
                                               value="{{ $user->id }}" name="selectedUsers[]">
                                    </div>
                                    <div class="grid-cols-1">
                                        <h1>{{ $user->name }}</h1>
                                    </div>
                                </div>
                                <div
                                    class="flex flex-wrap justify-center items-center md:grid grid-cols-4 md:col-span-4 mt-4">
                                    <div class="grid-cols-1 mx-4">
                                        <p class="block md:hidden text-gray-800 text-sm">Stundas</p>
                                        <h1>{{ $workReports->where('user_id', $user->id)->sum('work_hours') }}</h1>
                                    </div>
                                    <div class="grid-cols-1 mx-4">
                                        <p class="block md:hidden text-gray-800 text-sm">Dienas</p>
                                        <h1>{{ $workReports->where('user_id', $user->id)->where('work_hours', '>', 0)->count() }}</h1>
                                    </div>
                                    <div class="grid-cols-1 mx-4">
                                        <p class="block md:hidden text-gray-800 text-sm">Prombūtnes</p>
                                        <h1>{{ $workReports->where('user_id', $user->id)->where('work_hours', null)->count() }}</h1>
                                    </div>
                                    <div class="grid-cols-1 mx-4">
                                        <p class="block md:hidden text-gray-800 text-sm">Prombūtnes</p>
                                        <h1>{{ $workReports->where('user_id', $user->id)->sum(function ($workReport) { return $workReport->getOvertime($workReport->work_hours, $workReport->work_date); }) }}</h1>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
