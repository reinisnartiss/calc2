<div>
    <div class="p-6 text-center">
        <div>
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Pievienot darba laiku') }}
            </h2>
        </div>
    </div>
    <div class="p-2 md:p-6 text-center bg-white rounded-none md:rounded-lg border border-gray-200" style="min-height: 11rem">
        <div class="mb-4">
            <p>Meklēt projektu</p>
        </div>
        <div class="block md:flex lg:flex justify-center items-start relative">
            <input class="form-input w-full" type="text" placeholder="Search..." wire:model.debounce.200ms="search">
        </div>
        @if (session()->has('success'))
            <div class="border border-green-400 bg-green-100 mt-4 rounded-lg py-3">
                {{ session('success') }}
            </div>
        @endif
        @if (session()->has('error'))
            <div class="border border-red-400 bg-red-100 mt-4 rounded-lg py-3">
                {{ session('error') }}
            </div>
        @endif
        @if($search != '')
            <div class="absolute bg-gray-100 text-sm rounded mt-2 w-full md:w-80">
                @foreach($projects as $project)
                    <button class="w-full w-80 p-2 text-left" wire:click='addProject( {{ $project['order_id'] }}, "{{ $project['order_name'] }}" )'>{{$project['order_id'] . ', ' . $project['order_name'] }}</button>
                @endforeach
            </div>
        @endif
        @if($selectedProject)
            <form wire:submit.prevent="submit">
                <div class="block md:flex md:space-x-2 my-2 text-center md:text-left">
                    <div class="w-full md:w-3/6 mb-2 md:mb-0">
                        <h1 class="text-sm mb-2 text-gray-800">Projekts:</h1>
                        <h2>ID: {{ $selectedProject['id'] }}</h2>
                        <h2>{{  $selectedProject['name'] }}</h2>
                        <input type="hidden" value="{{ $selectedProject['id'] }}" name="selectedProject">
                    </div>
                    <div class="w-full md:w-2/6 mb-2 md:mb-0">
                        <h1 class="text-sm mb-2 text-gray-800">Darbs:  <span class="text-red-400">*</span></h1>
                        <select name="workType" id="workType" class="form-select w-full" wire:model="workType">
                            <option value=""></option>
                            @foreach($workTypes as $work)
                                <option value="{{ $work['id'] }}">{{ $work['work'] }}</option>
                            @endforeach
                        </select>
                        @error('workType') <span class="text-xs text-red-400">{{ $message }}</span> @enderror
                    </div>
                    <div class="w-1/4 md:w-1/6 mt-2 md:mt-0 mx-auto">
                        <h1 class="text-sm mb-2 text-gray-800">Minūtes: <span class="text-red-400">*</span></h1>
                        <input type="number" id="minutes" name="minutes" min="1" class="form-input w-full" wire:model="minutes">
                        @error('minutes') <span class="text-xs text-red-400">{{ $message }}</span> @enderror
                    </div>
                </div>
                <div class="text-center md:text-left mb-4">
                    <h1 class="text-sm mb-2 text-gray-800">Komentārs:</h1>
                    <input type="text" id="comment" name="comment" class="form-input w-full" wire:model="comment">
                </div>
                <button class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                    Sūtīt
                </button>
            </form>
            <div class="text-left text-sm mb-2 text-gray-800 mt-2">
                <span>Pēdējie ieraksti:</span>
            </div>
            @if(count($lastReports))
                <div class="grid grid-cols-5 underline">
                    <div class="text-sm mb-2 text-gray-800">Datums</div>
                    <div class="text-sm mb-2 text-gray-800">Darbinieks</div>
                    <div class="text-sm mb-2 text-gray-800">Stundas</div>
                    <div class="text-sm mb-2 text-gray-800">Darbs</div>
                    <div class="text-sm mb-2 text-gray-800">Piezīmes</div>
                </div>
                @foreach($lastReports as $report)
                    <div class="grid grid-cols-5 text-sm {{ $loop->odd ? 'bg-gray-200' : '' }}">
                        <div>{{ $report['created'] }}</div>
                        <div>{{ $report['creator_name'] }}</div>
                        <div>{{ $report['hours'] }}</div>
                        <div>{{ $report['product_name'] }}</div>
                        <div>{{ $report['note'] }}</div>
                    </div>
                @endforeach
            @else
                <div>Darbam nav ierakstu</div>
            @endif

        @endif
    </div>
</div>
