<div>
    @if($contract->user->hasResigned($contract->company) && $contract->user->companies->count() == 1)
        <div class="flex items-center">
            <p class="mr-4 self-center">Darba attiecības tiks izbeigtas un lietotājs tiks bloķēts pēc
                {{ $contract->user->resignations->where('company_id', $contract->company_id)->first()->resignation_date->addDays(1)->locale('lv')->diffInDays() }}
                dienām ({{ $contract->user->resignations->where('company_id', $contract->company_id)->first()->resignation_date->format('d.m.Y.') }})</p>
            <form action="{{ route('work-contract.unresign', $contract->user->resignations()->where('company_id', $contract->company_id)->first()->id) }}"
                  method="post">
                @csrf
                @method('delete')
                <button type="submit" class="mr-4 text-gray-400 hover:text-black"><i class="fas fa-times"></i></button>
            </form>
        </div>
    @elseif($contract->user->hasResigned($contract->company) && $contract->user->companies->count() > 1)
        <div class="flex items-center">
            <p class="mr-4 self-center">Darba attiecības tiks izbeigtas pēc
                {{ $contract->user->resignations->where('company_id', $contract->company_id)->first()->resignation_date->addDays(1)->locale('lv')->diffInDays() }} dienām
                ({{ $contract->user->resignations->where('company_id', $contract->company_id)->first()->resignation_date->format('d.m.Y.') }})</p>
            <form action="{{ route('work-contract.unresign', $contract->user->resignations()->where('company_id', $contract->company_id)->first()->id) }}"
                  method="post">
                @csrf
                @method('delete')
                <button type="submit" class="mr-4 text-gray-400 hover:text-black"><i class="fas fa-times"></i></button>
            </form>
        </div>
    @else
        <div class="{{ $state_button }}">
            <button type="button" wire:click="changeState" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                Ir atlūgums?
            </button>
        </div>
        <div class="{{ $state_form }}">
            <div class="flex">
                <form action="{{ route('work-contract.resign', $contract->id) }}" method="post" class="mr-4">
                    @csrf
                    <div>
                        <input type="hidden" name="user_id" value="{{ $contract->user_id }}">
                        <input type="hidden" name="company_id" value="{{ $contract->company_id }}">
                        <label for="resignation_date">Darba attiecību beigu datums</label>
                        <input type="date" name="resignation_date" id="resignation_date" class="form-input" wire:model="resignation_date" required>
                        <button type="submit" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                            Saglabāt
                        </button>
                        <button type="button" wire:click="changeState" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                            Atcelt
                        </button>
                    </div>
                    @error('resignation_date')<p class="text-sm text-red-600">{{ $message }}</p>@enderror

                </form>
            </div>
        </div>
    @endif
</div>
