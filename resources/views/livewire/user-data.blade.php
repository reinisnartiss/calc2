<x-jet-form-section submit="updateUserData">
    <x-slot name="title">
        {{ __('Lietotāja dati') }}
    </x-slot>

    <x-slot name="description">
        {{ __('') }}
    </x-slot>

    <x-slot name="form">
        <!-- Phone -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="phone" value="{{ __('Telefons') }}"/>
            <x-jet-input type="text" name="phone" class="mt-1 block w-full" wire:model.defer="phone"/>
            <x-jet-input-error for="phone" class="mt-2"/>
        </div>
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label class="mb-4" value="{{ __('Adrese (deklarētā)') }}"/>
            <div class="pl-4">
                <x-jet-label for="address_street" value="{{ __('Iela, Māja, Dzīvoklis') }}"/>
                <x-jet-input name="address" type="text" class="mt-1 block w-full" wire:model.defer="address_street"/>
                <x-jet-input-error for="address_street" class="mt-2"/>
                <x-jet-label for="address_city" value="{{ __('Pilsēta') }}"/>
                <x-jet-input name="address_city" type="text" class="mt-1 block w-full" wire:model.defer="address_city"/>
                <x-jet-input-error for="address_city" class="mt-2"/>
                <x-jet-label for="address_postal" value="{{ __('Pasta indekss') }}"/>
                <x-jet-input name="address_postal" type="text" class="mt-1 block w-full" placeholder="LV-1111" wire:model.defer="address_postal"/>
                <x-jet-input-error for="address_postal" class="mt-2"/>
            </div>
        </div>
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="personal_id" value="{{ __('Personas kods') }}"/>
            <x-jet-input name="personal_id" type="text" class="mt-1 block w-full" wire:model.defer="personal_id"/>
            <x-jet-input-error for="personal_id" class="mt-2"/>
        </div>
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="bank_account" value="{{ __('Bankas konts') }}"/>
            <x-jet-input name="bank_account" type="text" class="mt-1 block w-full" wire:model.defer="bank_account"/>
            <x-jet-input-error for="bank_account" class="mt-2"/>
        </div>
    </x-slot>

    <x-slot name="actions">
        @if ($successMessage)
            <span class="mr-3 text-sm text-gray-600"> {{ $successMessage }}</span>
        @endif
        <x-jet-button>
            {{ __('Saglabāt') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>
