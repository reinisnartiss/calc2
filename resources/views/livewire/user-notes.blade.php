<x-jet-form-section submit="storeNote" id="notes">
    <x-slot name="title">
        {{ __('Piezīmes') }}
    </x-slot>

    <x-slot name="description">
        {{ __('') }}
    </x-slot>

    <x-slot name="text">
        @if(count(auth()->user()->notes))
            <div class="mb-10">
                <div class="grid grid-cols-2 md:grid-cols-3 pb-5">
                    <div class="col-span-1 font-bold">Nosaukums</div>
                    <div class="col-span-1 md:col-span-2 font-bold">Vērtība</div>
                </div>
                @foreach(auth()->user()->notes as $note)
                    <div class="grid grid-cols-2 md:grid-cols-3 p-2 border rounded-lg mb-2 relative">
                        <div>{{ $note->name }}</div>
                        <div class="col-span-1 md:col-span-2">{{ $note->value }}</div>
                        <button wire:click="deleteNote({{ $note }})" onclick="return confirm('Vai vēlies izdzēst šo piezīmi?')" class="absolute top-0 right-0 m-2 text-gray-400 text-opacity-50 hover:text-opacity-100 hover:text-gray-600"><i class="fas fa-trash"></i></button>
                    </div>
                @endforeach
            </div>
        @endif
    </x-slot>

    <x-slot name="form">
        <!-- Email -->
        <div class="col-span-6 block sm:flex items-center gap-4">
            <x-jet-label for="email" value="{{ __('Nosaukums') }}" />
            <x-jet-input id="email" type="text" class="mt-1 block w-full" placeholder="durvju PIN" wire:model.defer="name" />
            <x-jet-input-error for="email" class="mt-2" />

            <x-jet-label for="email" value="{{ __('Vērtība') }}" />
            <x-jet-input id="email" type="text" class="mt-1 block w-full" placeholder="1234" wire:model.defer="value" />
            <x-jet-input-error for="email" class="mt-2" />
        </div>
    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Dati atjaunoti') }}
        </x-jet-action-message>

        <x-jet-button wire:loading.attr="disabled">
            {{ __('Saglabāt') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>

