<div>
    <div class="items-center p-6 bg-white border border-gray-200 shadow rounded-lg w-full">
        <div class="w-full">
            <form wire:submit.prevent="updateUserRole({{ $user->id }})" action="/" method="post">
                <div class="mb-4 w-full md:w-3/4 lg:w-1/2">
                    <x-jet-label for="roles" class="mb-1" value="{{ __('Pozīcija') }}"/>
                    <select wire:model="role_id" name="roles" class="form-input w-full">
                        @foreach ($roles as $role)
                            <option value="{{ $role->id }}">{{ ucfirst($role->name) }}</option>
                        @endforeach
                    </select>
                    <x-jet-input-error for="role" class="mt-2"/>
                </div>
                <div>
                    <x-jet-label for="manager_id" class="mb-1" value="{{ __('Vadītājs') }}"/>
                    <select wire:model="manager" name="manager_id"
                            class="form-input mb-4 w-full md:w-3/4 lg:w-1/2">
                        <option value="0">Nav vadītāja</option>
                        @forelse ($managers as $manager)
                            <option value="{{ $manager->id }}">{{ $manager->name }}</option>
                        @empty

                        @endforelse
                    </select>
                    <x-jet-input-error for="role" class="mt-2"/>
                </div>
                <div>
                    <a href="/users/{{ $user->id }}">
                        <button type="button"
                                class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase
                                tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                            Atpakaļ
                        </button>
                    </a>
                    <button type="submit"
                            class="my-4 md:my-2 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest
                            hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                        Saglabāt
                    </button>
                    @if ($successMessage)
                        <span class="mr-3 text-sm text-gray-600"> {{ $successMessage }}</span>
                    @endif
                </div>
            </form>
        </div>
    </div>
</div>
