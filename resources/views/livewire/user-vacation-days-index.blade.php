<div>
    <div class="md:p-6 mt-8 md:m-12 text-center bg-white rounded-none md:rounded-lg border border-gray-200">
        <div class="m-4">
            <p>Darbinieku uzkrātās atvaļinājuma dienas uz {{ now()->format('d.m.Y.') }} </p>
        </div>
        <div>
            @hasanyrole('super-admin|grāmatvedis')
                <div class="hidden lg:grid lg:grid-cols-12 sticky top-0 bg-white p-3">
                    <div class="col-span-1">

                    </div>
                    <div class="col-span-11 grid grid-cols-11">
                        @foreach($companies as $company)
                            <div class="col-span-1 text-xs text-gray-800">
                                {{ $company->name }}
                            </div>
                        @endforeach
                    </div>
                </div>


                @foreach($users as $user)
                    <div class="block lg:grid lg:grid-cols-12 border-b">
                        <div class="p-2 col-span-1">
                            {{ $user->name }}
                        </div>
                        <div class="col-span-11 flex justify-center items-center flex-wrap lg:grid lg:grid-cols-11">
                            @foreach($companies as $company)
                                <div class="col-span-1">
                                    <div>
                                        @if($user->companies->contains($company))
                                            <span class="block lg:hidden text-sm text-gray-800 mx-2">{{ $company->name }}</span>
                                            <span>{{ $user->getAvailableVacationDays($company) }}</span>
                                        @else
                                            <span></span>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endforeach
            @endhasanyrole
            @role('vadītājs')
                @if(auth()->user()->employees()->first())
                    <div class="block lg:grid lg:grid-cols-12 sticky top-0 bg-white p-3">
                        <div class="col-span-1">

                        </div>

                        @foreach($companies as $company)
                            <div class="hidden lg:block col-span-1 text-xs text-gray-800">
                                {{ $company->name }}
                            </div>
                        @endforeach

                        @foreach(auth()->user()->employees as $user)
                            <div class="p-2 col-span-1">
                                {{ $user->name }}
                            </div>
                            <div class="col-span-11 flex justify-center items-center lg:grid lg:grid-cols-11">
                                @foreach($companies as $company)
                                    <div class="col-span-1">
                                        <div class="">
                                            @if($company->users->contains($user))
                                                <span class="block lg:hidden text-sm text-gray-800 mx-2">{{ $company->name }}</span>
                                                <span>{{ $user->getAvailableVacationDays($company) }}</span>
                                            @else
                                                <span></span>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                @else
                    <div>
                        Lietotājam nav piesaistītu darbinieku
                    </div>
                @endif
            @endrole
        </div>
    </div>
</div>
