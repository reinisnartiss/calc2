<div>
    <div class="mb-4 text-center">
        <div class="text-lg mb-4">Jauns pieprasījums:</div>
        <div class="text-center block md:inline-block lg:inline-block lg:mr-2">
            <label for="start_date" class="mr-1 block md:inline-block lg:inline-block">No <span class="text-xs text-gray-800">(ieskaitot)</span>:</label>
            <input class="p-1 border border-gray-200 rounded-lg" required type="date" name="start_date" id="start_date" wire:model="start_date" wire:change.lazy="createVacationPeriod()">
            @error('start_date')<p class="text-sm text-red-600">{{ $message }}</p>@enderror
        </div>
        <div class="text-center md:inline-block lg:inline-block inline-block md:ml-2 mt-2 md:mt-0">
            <label for="end_date" class="mr-1 block md:inline-block lg:inline-block">Līdz <span class="text-xs text-gray-800">(ieskaitot)</span>:</label>
            <input class="p-1 border border-gray-200 rounded-lg" required type="date" name="end_date" id="end_date" wire:model="end_date" wire:change.lazy="createVacationPeriod()">
            @error('end_date')<p class="text-sm text-red-600">{{ $message }}</p>@enderror
        </div>
        <input type="hidden" value="{{ $user->id }}" name="user_id">

    </div>
    @if($start_date && $end_date !== null)
        <div class="mb-4 text-center">
            @if(count($holidays) !== 0)
                <p>Atvaļinājuma laikā ietilpist {{ count($holidays) }} svētku dienas:</p>
            @endif
            @foreach($holidays as $holiday)
                <p class="text-red-500 text-sm">{{ \Illuminate\Support\Carbon::parse($holiday)->format('d.m.Y.') }}</p>
            @endforeach
            @if($vacationDaysCount == 1)
                <p>Tiks izmantota {{ $vacationDaysCount }} atvaļinājuma diena.</p>
            @else
                <p>Tiks izmantotas {{ $vacationDaysCount }} atvaļinājuma dienas.</p>
            @endif
        </div>
        @isset($message)
            <div class="text-sm text-red-500">
                {{ $message }}
            </div>
        @endisset
    @endif
    <div class="text-center mb-2">
        <div class="mb-2">
            <input class="p-2 form-checkbox" type="checkbox" id="selectAll" wire:model="selectAll" wire:click="selectAll()">
            <label for="selectAll" class="mr-1 inline-block">Visos uzņēmumos</label>
        </div>
        <div class="block md:flex justify-center border-t border-b border-dotted p-4 w-3/4 mx-auto">
            @foreach($user->companies as $company)
                <div class="my-2 md:my-0 mx-2">
                    <input class="p-2 form-checkbox" type="checkbox" name="selected_companies[]" wire:model="selected" value="{{ $company->id }}">
                    <label for="selectedCompanies" class="mr-1 inline-block">{{ $company->name }}</label>
                </div>
            @endforeach
        </div>
        @error('selected_companies')<p class="text-sm text-red-600">{{ $message }}</p>@enderror
    </div>
    <div class="text-center mb-2">
        <input type="checkbox" name="unpaid_leave" id="unpaid_leave" class="form-checkbox" value="1" wire:model="unpaidLeave">
        <label for="unpaid_leave">Bezalgas atvaļinājums</label>
    </div>
    <div class="text-center">
        <button type="submit" class="my-4 md:my-2 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
            Nosūtīt pieprasījumu
        </button>
    </div>
</div>
