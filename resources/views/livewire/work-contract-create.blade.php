<div>
    <div class="items-center p-6 bg-white border border-gray-200 shadow rounded-lg w-full">
        <div class="w-full">
            <form action="{{ route('work-contract.store') }}" method="post">
                @csrf

                <div class="mb-4 w-full md:w-3/4 lg:w-1/2">
                    @if($user->filledDataForm())
                        <div class="bg-white w-full mt-6">
                            <div class="mb-4">
                                E-pasts: {{ $user->email }}
                            </div>
                            <div class="mb-4">
                                Telefona numurs: {{ $user->phone }}
                            </div>
                            <div class="mb-4">
                                Adrese: {{ $user->address_street }}, {{ $user->address_city }}, {{ $user->address_postal }}
                            </div>
                            <div>
                                Bankas konts: {{ $user->bank_account }}
                            </div>
                        </div>
                    @else
                        <div>
                            <p class="text-red-600 text-sm">Lietotājs vēl nav aizpldījis savus datus</p>
                        </div>
                    @endif
                </div>
                @if( ! $user->name_genitivs || ! $user->name_dativs || ! $user->name_akuzativs)
                    <div class="border-t py-4">
                        <div class="mb-4">
                            <p class="text-sm text-gray-800">
                                Darba līguma, rīkojuma un turpmāko dokumentu sagatavošanas ērtībai lūdzu aizpildiet šos laukus par lietotāju:
                            </p>
                        </div>
                        <div>
                            @if( ! $user->name_genitivs)
                                <div class="mb-4">
                                    <label for="name_genitivs">Darbinieka vārds ģenitīvā: </label>
                                    <input type="text" id="name_genitivs" name="name_genitivs" class="form-input ml-2" required>
                                    <span class="text-xs text-gray-800">Jāņa Krūmiņa</span>
                                </div>
                                @error('name_genitivs')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                            @endif
                            @if( ! $user->name_dativs)
                                <div class="mb-4">
                                    <label for="name_dativs">Darbinieka vārds datīvā: </label>
                                    <input type="text" id="name_dativs" name="name_dativs" class="form-input ml-2" required>
                                    <span class="text-xs text-gray-800">Jānim Krūmiņam</span>
                                </div>
                                @error('name_dativs')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                            @endif
                            @if( ! $user->name_akuzativs)
                                <div class="mb-4">
                                    <label for="name_akuzativs">Darbinieka vārds akuzatīvā: </label>
                                    <input type="text" id="name_akuzativs" name="name_akuzativs" class="form-input ml-2" required>
                                    <span class="text-xs text-gray-800">Jāni Krūmiņu</span>
                                </div>
                                @error('name_akuzativs')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                            @endif
                        </div>
                    </div>
                @endif
                <div class="border-t py-4">
                    <div class="text-center my-2">
                        <h1 class="text-lg font-bold">Darba līgums</h1>
                    </div>
                    <div>
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                    </div>
                    <div>
                        <label for="contract_number" class="mr-2">Darba līguma numurs:</label>
                        <input type="text" class="form-input" id="contract_number" name="contract_number" required>
                        @error('contract_number')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    </div>
                    <div class="mt-4">
                        <label for="date" class="mr-2">Datums:</label>
                        <input type="date" class="form-input" id="date" name="date" required>
                    </div>
                    @error('date')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    <div class="mt-4">
                        <label for="company_id" class="mr-2">Uzņēmums:</label>
                        <select name="company_id" id="company_id" class="form-input" wire:model="selected_company" required>
                            @foreach($companies as $company)
                                <option value="{{ $company->id }}">{{ $company->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('company_id')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    <div class="text-center mt-6 mb-2">
                        <h1 class="text-lg font-bold">Līguma priekšmets</h1>
                    </div>
                    <div class="mt-4 block">
                        <label for="position" class="mr-2">Amats:</label>
                        <div class="mb-4">
                            <input type="text" class="form-input" id="position_nominativs" name="position_nominativs" required>
                            <span class="text-xs text-gray-800">nominatīvā (palīgstrādnieks)</span>
                        </div>
                        @error('position_nominativs')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                        <div class="mb-4">
                            <input type="text" class="form-input" id="position_genitivs" name="position_genitivs" required>
                            <span class="text-xs text-gray-800">ģenitīvā (palīgstrādnieka)</span>
                        </div>
                        @error('position_genitivs')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                        <div class="mb-4">
                            <input type="text" class="form-input" id="position_dativs" name="position_dativs" required>
                            <span class="text-xs text-gray-800">datīvā (palīgstrādniekam)</span>
                        </div>
                        @error('position_dativs')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                        <div class="mb-4">
                            <input type="text" class="form-input" id="position_akuzativs" name="position_akuzativs" required>
                            <span class="text-xs text-gray-800">akuzatīvā (palīgstrādnieku)</span>
                        </div>
                        @error('position_akuzativs')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    </div>
                    <div class="mt-4">
                        <label for="position_id" class="mr-2">Amata kods:</label>
                        <input type="text" class="form-input" id="position_id" name="position_id" required>
                    </div>
                    @error('position_id')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    <div class="text-center mt-6 mb-2">
                        <h1 class="text-lg font-bold">Darba tiesisko attiecību termiņš un pārbaudes laiks</h1>
                    </div>
                    <div class="mt-4">
                        <label for="contract_start" class="mr-2">Darba tiesiskās attiecības tiek uzsāktas ar:</label>
                        <input type="date" class="form-input" id="contract_start" name="contract_start" required wire:model="start_date">
                    </div>
                    @error('contract_start')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    <div class="mt-4">
                        <label for="contract_start_month" class="mr-2">Ar kuru mēnesi: </label>
                        <select name="contract_start_month" id="contract_start_month" class="form-select">
                            @foreach([1 => 'janvāri', 2 => 'februāri', 3 => 'martu', 4 => 'aprīli', 5 => 'maiju', 6 => 'jūniju',
                                        7 => 'jūliju', 8 => 'augustu', 9 => 'septembri', 10 => 'oktobri', 11 => 'novembri',
                                        12 => 'decembri'] as $key => $month)
                                <option value="{{ $month }}" {{ $key == \Illuminate\Support\Carbon::parse($start_date)->month ? 'selected' : '' }}>{{ $month }}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('contract_start_month')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4 flex items-center">
                        <span class="mr-4">Uz:</span>
                        <div>
                            <label class="inline-flex items-center mr-2">
                                <input type="radio" class="form-radio" name="contract_period" value="1" wire:model="contractPeriod">
                                <span class="ml-2">Nenoteiktu laiku</span>
                            </label>
                        </div>
                        <div>
                            <label class="inline-flex items-center">
                                <input type="radio" class="form-radio" name="contract_period" value="2" wire:model="contractPeriod">
                                <span class="ml-2">Noteiktu laiku</span>
                            </label>
                        </div>
                    </div>
                    @error('contract_period')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    @if($contractPeriod == 2)
                        <div class="mt-4">
                            <label for="contract-end-date" class="mr-2">Līdz:</label>
                            <input type="date" class="form-input" id="contract-end-date" name="contract_end_date" wire:model="end_date">
                        </div>
                        <div class="mt-4">
                            <label for="contract_end_date_month" class="mr-2">Līdz kuram mēnesim:</label>
                            <select name="contract_end_date_month" id="contract_end_date_month" class="form-select">
                                @foreach([1 => 'janvārim', 2 => 'februārim', 3 => 'martam', 4 => 'aprīlim', 5 => 'maijam', 6 => 'jūnijam',
                                        7 => 'jūlijam', 8 => 'augustam', 9 => 'septembrim', 10 => 'oktobrim', 11 => 'novembrim',
                                        12 => 'decembrim'] as $key => $month)
                                    <option value="{{ $month }}" {{ $key == \Illuminate\Support\Carbon::parse($end_date)->month ? 'selected' : '' }}>{{ $month }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('contract-end-date_month')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    @endif
                    <div class="text-center mt-6 mb-2">
                        <h1 class="text-lg font-bold">Darba vieta un laiks</h1>
                    </div>
                    <div class="mt-4 flex">
                        <span class="mr-4">Tiek noteikts:</span>
                        <div>
                            <label class="inline-flex items-center mr-2">
                                <input type="radio" class="form-radio" name="work_hours_type" value="1">
                                <span class="ml-2">Nepilnais</span>
                            </label>
                        </div>
                        <div>
                            <label class="inline-flex items-center mr-2">
                                <input type="radio" class="form-radio" name="work_hours_type" value="2" checked>
                                <span class="ml-2">Normālais</span>
                            </label>
                        </div>
                        <div>
                            <label class="inline-flex items-center">
                                <input type="radio" class="form-radio" name="work_hours_type" value="3">
                                <span class="ml-2">Summētais</span>
                            </label>
                        </div>
                        <span class="ml-4">darba laiks</span>
                    </div>
                    @error('work_hours_type')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4">
                        <input type="number" class="form-input" id="weekly_hours" name="weekly_hours" required>
                        <label for="weekly_hours" class="ml-2">stundas nedēļā</label>
                    </div>
                    @error('weekly_hours')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4 flex">
                        <span class="mr-4">Darba laiks:</span>
                        <div>
                            <label class="inline-flex items-center mr-2">
                                <input type="radio" class="form-radio" name="work_hours" value="1">
                                <span class="ml-2">No 9:00 līdz 18:00</span>
                            </label>
                        </div>
                        <div>
                            <label class="inline-flex items-center">
                                <input type="radio" class="form-radio" name="work_hours" value="2" checked>
                                <span class="ml-2">No 9:00 līdz 18:00 ar pusdienu pārtraukumu</span>
                            </label>
                        </div>
                    </div>
                    @error('work_hours')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="text-center mt-6 mb-2">
                        <h1 class="text-lg font-bold">Darba samaksa</h1>
                    </div>
                    <div>
                        <label class="inline-flex items-center mr-2">
                            <input type="radio" class="form-radio" name="salary_kind" value="1" wire:model="salaryKind">
                            <span class="ml-2">Fiksēta alga</span>
                        </label>
                    </div>
                    <div>
                        <label class="inline-flex items-center">
                            <input type="radio" class="form-radio" name="salary_kind" value="2" wire:model="salaryKind">
                            <span class="ml-2">Stundas likme</span>
                        </label>
                    </div>
                    @error('salary_kind')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4">
                        <input type="number" step="0.01" class="form-input" id="salary" name="salary" required>
                        @if($salaryKind == 2)
                            <label for="salary" class="ml-2">EUR/stundā</label>
                        @else
                            <label for="salary" class="ml-2">EUR</label>
                        @endif
                    </div>
                    @error('salary')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4">
                        <label for="salary_written" class="mr-2">Atalgojums rakstiski:</label>
                        <input type="text" class="form-input" id="salary_written" name="salary_written" required>
                    </div>
                    @error('salary_written')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4">
                        <label class="inline-flex items-center mr-2">
                            <input type="radio" class="form-radio" name="salary_type" value="1" checked>
                            <span class="ml-2">BRUTO alga</span>
                        </label>
                    </div>
                    <div>
                        <label class="inline-flex items-center">
                            <input type="radio" class="form-radio" name="salary_type" value="2">
                            <span class="ml-2">NETO alga</span>
                        </label>
                    </div>
                    @error('salary_type')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="text-center mt-6 mb-2">
                        <h1 class="text-lg font-bold">Pušu paraksti</h1>
                    </div>
                    <div class="mt-4">
                        <p class="mr-2 inline-block">Darba devējs:</p>
                        <span>{{ \App\Models\Company::where('id', $selected_company)->first()->director()->name }}</span>
                    </div>
                    @error('company')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror


                </div>

                <div>
                    <button type="submit"
                            class="my-4 md:my-2 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                        Saglabāt
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
