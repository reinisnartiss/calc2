<div>
    <div class="items-center p-6 bg-white border border-gray-200 shadow rounded-lg w-full">
        <div class="w-full">
            <form action="{{ route('work-contract.update', $contract) }}" method="post">
                @csrf
                @method('put')

                <div class="py-4">
                    <a href="/work-contract/{{ $contract->id }}">
                        <button type="button" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                            Atpakaļ
                        </button>
                    </a>
                    <div class="text-center my-2">
                        <h1 class="text-lg font-bold">Darba līgums</h1>
                    </div>
                    <div>
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                    </div>
                    <div>
                        <label for="contract-number" class="mr-2">Darba līguma numurs:</label>
                        <input type="text" class="form-input" id="contract-number" name="contract_number" value="{{ $contract->contract_number }}" required>
                        @error('contract-number')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    </div>
                    <div class="mt-4">
                        <label for="date" class="mr-2">Datums:</label>
                        <input type="date" class="form-input" id="date" name="date" value="{{ $contract->date ? $contract->date->format('Y-m-d') : '' }}" required>
                    </div>
                    @error('date')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    <div class="mt-4">
                        <label for="company_id" class="mr-2">Uzņēmums:</label>
                        <select name="company_id" id="company_id" class="form-input" wire:model="selected_company" required>
                            @foreach($companies as $company)
                                <option value="{{ $company->id }}">{{ $company->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('company_id')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    <div class="text-center mt-6 mb-2">
                        <h1 class="text-lg font-bold">Līguma priekšmets</h1>
                    </div>
                    <div class="mt-4 block">
                        <label for="position" class="mr-2">Amats:</label>
                        <div class="mb-4">
                            <input type="text" class="form-input" id="position_nominativs" name="position_nominativs" value="{{ $contract->position_nominativs }}" required>
                            <span class="text-xs text-gray-800">nominatīvā (palīgstrādnieks)</span>
                        </div>
                        @error('position_nominativs')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                        <div class="mb-4">
                            <input type="text" class="form-input" id="position" name="position_genitivs" value="{{ $contract->position_genitivs }}" required>
                            <span class="text-xs text-gray-800">ģenitīvā (palīgstrādnieka)</span>
                        </div>
                        @error('position_genitivs')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                        <div class="mb-4">
                            <input type="text" class="form-input" id="position" name="position_dativs" value="{{ $contract->position_dativs }}" required>
                            <span class="text-xs text-gray-800">datīvā (palīgstrādniekam)</span>
                        </div>
                        @error('position_dativs')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                        <div class="mb-4">
                            <input type="text" class="form-input" id="position" name="position_akuzativs" value="{{ $contract->position_akuzativs }}" required>
                            <span class="text-xs text-gray-800">akuzatīvā (palīgstrādnieku)</span>
                        </div>
                        @error('position_akuzativs')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    </div>
                    <div class="mt-4">
                        <label for="position_id" class="mr-2">Amata kods:</label>
                        <input type="text" class="form-input" id="position_id" name="position_id" value="{{ $contract->position_id }}" required>
                    </div>
                    @error('position_id')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    <div class="text-center mt-6 mb-2">
                        <h1 class="text-lg font-bold">Darba tiesisko attiecību termiņš un pārbaudes laiks</h1>
                    </div>
                    <div class="mt-4">
                        <label for="contract_start" class="mr-2">Darba tiesiskās attiecības tiek uzsāktas ar:</label>
                        <input type="date" class="form-input" id="contract_start" name="contract_start" value="{{ $contract->contract_start ? $contract->contract_start->format('Y-m-d') : '' }}" required>
                    </div>
                    @error('contract_start')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                    <div class="mt-4">
                        <label for="contract_start_month" class="mr-2">Ar kuru mēnesi: </label>
                        <select name="contract_start_month" id="contract_start_month" class="form-select">
                            @foreach(['janvāri', 'februāri', 'martu', 'aprīli', 'maiju', 'jūniju', 'jūliju', 'augustu', 'septembri', 'oktobri', 'novembri', 'decembri'] as $month)
                                <option value="{{ $month }}" {{ $contract->contract_start_month == $month ? 'checked' : '' }}>{{ $month }}</option>
                            @endforeach
                        </select>
                    </div>
                    @error('contract-start_month')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4 flex items-center">
                        <span class="mr-4">Uz:</span>
                        <div>
                            <label class="inline-flex items-center mr-2">
                                <input type="radio" class="form-radio" name="contract_period" value="1" wire:model="contractPeriod">
                                <span class="ml-2">Nenoteiktu laiku</span>
                            </label>
                        </div>
                        <div>
                            <label class="inline-flex items-center">
                                <input type="radio" class="form-radio" name="contract_period" value="2" wire:model="contractPeriod">
                                <span class="ml-2">Noteiktu laiku</span>
                            </label>
                        </div>
                    </div>
                    @error('contract_period')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    @if($contractPeriod == 2)
                        <div class="mt-4">
                            <label for="contract-end-date" class="mr-2">Līdz:</label>
                            <input type="date" class="form-input" id="contract-end-date" name="contract_end_date" value="{{ $contract->contract_end_date }}">
                        </div>
                        <div class="mt-4">
                            <label for="contract_end_date_month" class="mr-2">Līdz kuram mēnesim:</label>
                            <select name="contract_end_date_month" id="contract_end_date_month" class="form-select" >
                                @foreach(['janvārim', 'februārim', 'martam', 'aprīlim', 'maijam', 'jūnijam', 'jūlijam', 'augustam', 'septembrim', 'oktobrim', 'novembrim', 'decembrim'] as $month)
                                    <option value="{{ $month }}" {{ $contract->contract_end_date_month == $month ? 'checked' : '' }}>{{ $month }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('contract-end-date_month')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    @endif
                    <div class="text-center mt-6 mb-2">
                        <h1 class="text-lg font-bold">Darba vieta un laiks</h1>
                    </div>
                    <div class="mt-4 flex">
                        <span class="mr-4">Tiek noteikts:</span>
                        <div>
                            <label class="inline-flex items-center mr-2">
                                <input type="radio" class="form-radio" name="work_hours_type" value="1" {{ $contract->work_hours_type == 1 ? 'checked' : '' }}>
                                <span class="ml-2">Nepilnais</span>
                            </label>
                        </div>
                        <div>
                            <label class="inline-flex items-center mr-2">
                                <input type="radio" class="form-radio" name="work_hours_type" value="2" {{ $contract->work_hours_type == 2 ? 'checked' : '' }}>
                                <span class="ml-2">Normālais</span>
                            </label>
                        </div>
                        <div>
                            <label class="inline-flex items-center">
                                <input type="radio" class="form-radio" name="work_hours_type" value="3" {{ $contract->work_hours_type == 3 ? 'checked' : '' }}>
                                <span class="ml-2">Summētais</span>
                            </label>
                        </div>
                        <span class="ml-4">darba laiks</span>
                    </div>
                    @error('work_hours_type')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4">
                        <input type="number" class="form-input w-24" id="weekly_hours" name="weekly_hours" required value="{{ $contract->weekly_hours }}">
                        <label for="weekly_hours" class="ml-2">stundas nedēļā</label>
                    </div>
                    @error('weekly_hours')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4 flex">
                        <span class="mr-4">Darba laiks:</span>
                        <div>
                            <label class="inline-flex items-center mr-2">
                                <input type="radio" class="form-radio" name="work_hours" value="1" {{ $contract->work_hours == 1 ? 'checked' : '' }}>
                                <span class="ml-2">No 9:00 līdz 18:00</span>
                            </label>
                        </div>
                        <div>
                            <label class="inline-flex items-center">
                                <input type="radio" class="form-radio" name="work_hours" value="2" {{ $contract->work_hours == 2 ? 'checked' : '' }}>
                                <span class="ml-2">No 9:00 līdz 18:00 ar pusdienu pārtraukumu</span>
                            </label>
                        </div>
                    </div>
                    @error('work_hours')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="text-center mt-6 mb-2">
                        <h1 class="text-lg font-bold">Darba samaksa</h1>
                    </div>
                    <div>
                        <label class="inline-flex items-center mr-2">
                            <input type="radio" class="form-radio" name="salary_kind" value="1" wire:model="salaryKind">
                            <span class="ml-2">Fiksēta alga</span>
                        </label>
                    </div>
                    <div>
                        <label class="inline-flex items-center">
                            <input type="radio" class="form-radio" name="salary_kind" value="2" wire:model="salaryKind">
                            <span class="ml-2">Stundas likme</span>
                        </label>
                    </div>
                    @error('salary_kind')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4">
                        <input type="number" step="0.01" class="form-input w-32" id="salary" name="salary" value="{{ $contract->salary }}">
                        @if($salaryKind == 2)
                            <label for="salary" class="ml-2">EUR/stundā</label>
                        @else
                            <label for="salary" class="ml-2">EUR</label>
                        @endif
                    </div>
                    @error('salary')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4">
                        <label for="salary_written" class="mr-2">Atalgojums rakstiski:</label>
                        <input type="text" class="form-input w-64" id="salary_written" name="salary_written" value="{{ $contract->salary_written }}">
                    </div>
                    @error('salary_written')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="mt-4">
                        <label class="inline-flex items-center mr-2">
                            <input type="radio" class="form-radio" name="salary_type" value="1" {{ $contract->salary_type == 1 ? 'checked' : '' }}>
                            <span class="ml-2">BRUTO alga</span>
                        </label>
                    </div>
                    <div>
                        <label class="inline-flex items-center">
                            <input type="radio" class="form-radio" name="salary_type" value="2" {{ $contract->salary_type == 2 ? 'checked' : '' }}>
                            <span class="ml-2">NETO alga</span>
                        </label>
                    </div>
                    @error('salary_type')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror

                    <div class="text-center mt-6 mb-2">
                        <h1 class="text-lg font-bold">Pušu paraksti</h1>
                    </div>
                    <div class="mt-4">
                        <p class="mr-2 inline-block">Darba devējs:</p>
                        <span>{{ \App\Models\Company::where('id', $selected_company)->first()->director()->name }}</span>
                    </div>
                    @error('company')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror


                </div>

                <div>
                    <button type="submit"
                            class="my-4 md:my-2 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                        Saglabāt
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

