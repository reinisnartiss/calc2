<div>
    <div class="py-4 px-2 md:p-6 text-center bg-white rounded-none md:rounded-lg border border-gray-200 mb-2">
        <h2>{{ $monthName }} nostrādāts</h2>
        <div class="flex justify-around p-2 rounded-lg mt-3">
            <div>
                <span class="text-3xl">{{ $workReports->where('work_hours', '!=', null)->count() }} </span>
                <span class="block text-gray-800">dienas</span>
            </div>
            <div>
                <span class="text-3xl">{{ $workReports->sum('work_hours') }}</span>
                <span class="block text-gray-800">stundas</span>
            </div>
        </div>
    </div>
    <div class="p-6 text-center bg-white rounded-none md:rounded-lg border border-gray-200">
        <div class="mb-4 border-b pb-6 flex justify-center">
            <input type="month" wire:model.lazy="selectedMonth" wire:change="getWorkReports()" class="form-input">
        </div>
        <div class="block lg:grid lg:grid-cols-7 gap-2 mx-auto">
            @foreach (['Pirmdiena', 'Otrdiena', 'Trešdiena', 'Ceturtdiena', 'Piektdiena', 'Sestdiena', 'Svētdiena'] as $dayName)
                <div class="hidden lg:block lg:col-span-1">
                    <span class="text-gray-800 text-sm">{{ $dayName }}</span>
                </div>
            @endforeach

            @foreach($allDates as $date)
                    @if($workReports->contains('work_date', $date))
                        <div class="border-2 grid md:col-span-2 lg:col-span-1 row-auto rounded-lg p-4 mb-2 shadow relative {{ $date->isWeekend() || $date->isHoliday() ? 'bg-red-100 bg-opacity-50 border-red-300 weekend' : 'border-green-500 border-opacity-25 work-day' }} work-report-div" style="grid-column: {{ $date->dayOfWeekIso }}">
                            <a href="/work-report/{{ $workReports->where('work_date', $date)->first()->id }}" class="absolute top-0 right-0 m-2 text-gray-400 text-opacity-50 hover:text-opacity-100 hover:text-gray-600"><i class="fas fa-pen"></i></a>
                            <form action="/work-report/{{ $workReports->where('work_date', $date)->first()->id }}" method="post">
                                @csrf
                                @method('delete')
                                <button type="submit" onclick="return confirm('Vai vēlies izdzēst šo ierakstu?')" class="absolute top-0 left-0 m-2 text-gray-400 text-opacity-50 hover:text-opacity-100 hover:text-gray-600"><i class="fas fa-trash"></i></button>
                            </form>
                            <div>
                                <p class="text-sm text-gray-600 pt-4">{{ $workReports->where('work_date', $date)->first()->work_date->day }}. {{ $workReports->where('work_date', $date)->first()->work_date->locale('lv')->shortMonthName }}</p>
                            </div>
                            <div class="{{ $workReports->where('work_date', $date)->first()->work_hours > 8 || $workReports->where('work_date', $date)->first()->work_date->isWeekend() ? 'text-red-600 font-bold' : '' }}">
                                {{ $workReports->where('work_date', $date)->first()->work_hours == null || 0 ? 'Nestrādāju' : $workReports->where('work_date', $date)->first()->work_hours . ' stundas' }}
                            </div>
                            @isset($workReports->where('work_date', $date)->first()->reason)
                                <div class="text-sm text-gray-800">
                                    {{ $workReports->where('work_date', $date)->first()->reason->name }}
                                </div>
                            @endisset
                            @isset($workReports->where('work_date', $date)->first()->comment)
                                <div>
                                    {{ $workReports->where('work_date', $date)->first()->comment }}
                                </div>
                            @endisset
                        </div>
                    @else
                        <div class="border grid md:col-span-2 lg:col-span-1 row-auto rounded-lg p-4 mb-2 h-24 lg:h-36 shadow relative lg:w-full {{ $date->isWeekend() || $date->isHoliday() ? 'bg-red-100 bg-opacity-25 border-red-300 weekend' : 'border-gray-400 border-opacity-25 work-day' }} work-report-div" style="grid-column: {{ $date->dayOfWeekIso }}">
                            <div>
                                <p class="text-sm text-gray-600 pt-4">{{ $date->day }}. {{ $date->locale('lv')->shortMonthName }}</p>
                                <div>
                                    <p class="self-center text-sm">{{ $date->isHoliday() ? $date->getHolidayName() : '' }}</p>
                                </div>

                            </div>
                        </div>
                    @endif
            @endforeach
        </div>
    </div>
</div>
