<div>
    <div class="p-6 text-center">
        <div>
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                Darba laika atskaite:     {{ $workReport->work_date->format('d-m-Y') }}
            </h2>
        </div>
    </div>
    <div class="p-6 text-center bg-white rounded-lg border border-gray-200">
        <form action="/work-report/{{ $workReport->id }}" method="post">
            @csrf
            @method('PUT')
            <div class="mb-4 block md:flex flex-wrap">
                <div class="mb-2 md:mb-0 flex justify-center">
                        <div class="flex items-center">
                        <span class="mr-4">Strādāji:</span>
                          <div>
                            <label class="inline-flex items-center mr-2">
                              <input type="radio" class="form-radio" name="did-work" value="1" wire:model="didWork">
                              <span class="ml-2">Jā</span>
                            </label>
                          </div>
                          <div>
                            <label class="inline-flex items-center">
                              <input type="radio" class="form-radio" name="did-work" wire:model="didWork" value="0">
                              <span class="ml-2">Nē</span>
                            </label>
                          </div>
                        </div>
                </div>
                @if ($didWork)
                    <div class="work_hours ml-6">
                        <label for="hours" class="mr-2">Stundas:</label>
                        <input type="number" id="hourDiv" wire:model="hours" name="work_hours" class="work_hours form-input" wire:change="checkOvertime()">
                        @error('work_hours') <p class="text-sm text-red-400">Jānorāda stundas</p> @enderror
                    </div>
                @else
                    <div class="reason mx-6">
                        <label for="reason" class="mr-2">Iemesls</label>
                        <select name="reason_id" class="reason_id form-select" wire:model="reason">
                                <option class="display:none"></option>
                            @foreach ($reasons as $reason)
                                <option value="{{ $reason->id }}" {{ $reason->id == $workReport->reason_id ? 'selected' : '' }}>{{ $reason->name }}</option>
                            @endforeach
                        </select>
                        @error('reason_id') <p class="text-sm text-red-400">Jānorāda iemesls</p> @enderror
                    </div>
                @endif

                @if ($didWork && $overtime > 0)
                    <div class="comment ml-6 ">
                        <label for="comment" class="mr-2">Iemesls virsstundām</label>
                        <input type="text" name="comment" wire:model="comment" class="comment_text form-input">
                    </div>
                @endif
                @error('comment') <p class="text-sm text-red-400 p-3">Jānorāda komentārs</p> @enderror
            </div>
            <div class="flex justify-between">
                <div>
                    <a href="/work-report">
                        <button type="button" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                            Atpakaļ
                        </button>
                    </a>
                </div>
                <button type="submit" id="btn" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                    Atjaunot
                </button>
            </div>
        </form>
    </div>
</div>
