<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">
                <!-- Logo -->
                <div class="flex-shrink-0 flex items-center">
                    <a href="{{ route('work-report.index') }}">
                        <x-jet-application-mark class="block h-9 w-auto"/>
                    </a>
                </div>

                <!-- Navigation Links -->
                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-jet-nav-link href="{{ route('work-report.index') }}"
                                    :active="request()->routeIs('work-report.index')">
                        {{ __('Darba laiks') }}
                    </x-jet-nav-link>
                    @can('view users')
                        <x-jet-nav-link href="{{ route('users.index') }}" :active="request()->routeIs('users.index')">
                            {{ __('Darbinieki') }}
                        </x-jet-nav-link>
                    @endcan
                    <x-jet-nav-link href="{{ route('vacations.index') }}"
                                    :active="request()->routeIs('vacations.index')">
                        {{ __('Atvaļinājums') }}
                    </x-jet-nav-link>
                    @can('get reports')
                        <x-jet-nav-link href="{{ route('employee-report.index') }}"
                                        :active="request()->routeIs('employee-report.index')">
                            {{ __('Atskaites') }}
                        </x-jet-nav-link>
                    @endcan
                    @hasanyrole('super-admin|vadītājs|grāmatvedis')
                        <x-jet-nav-link href="{{ route('invite.index') }}"
                                        :active="request()->routeIs('invite.index')">
                            {{ __('Nosūtīt uzaicinājumu') }}
                        </x-jet-nav-link>
                    @endhasanyrole
                    @if (!auth()->user()->filledDataForm())
                        <x-jet-nav-link class="text-red-500" href="/user/profile"
                                        :active="request()->routeIs('user-data.create')">
                            {{ __('Iesniegt datus') }}
                        </x-jet-nav-link>
                    @endif

                </div>
            </div>

            <!-- Settings Dropdown -->
            <div class="hidden sm:flex sm:items-center sm:ml-6">
                <x-jet-dropdown align="right" width="48">
                    <x-slot name="trigger">
                        <button
                            class="flex text-sm border-2 border-transparent rounded-full focus:outline-none focus:border-gray-300 transition duration-150 ease-in-out">
                            <img class="h-8 w-8 rounded-full object-cover" src="{{ Auth::user()->profile_photo_url }}"
                                 alt="{{ Auth::user()->name }}"/>
                        </button>
                    </x-slot>

                    <x-slot name="content">

                        <!-- Account Management -->
                        <x-jet-dropdown-link href="{{ route('profile.show') }}">
                            {{ __('Profils') }}
                        </x-jet-dropdown-link>
                        <x-jet-dropdown-link href="/user/profile#notes">
                            {{ __('Piezīmes') }}
                        </x-jet-dropdown-link>

                        <!-- Authentication -->
                        <form method="POST" action="{{ route('logout') }}">
                            @csrf

                            <x-jet-dropdown-link href="{{ route('logout') }}" onclick="event.preventDefault();
                                                            this.closest('form').submit();">
                                {{ __('Beigt darbu') }}
                            </x-jet-dropdown-link>
                        </form>
                    </x-slot>
                </x-jet-dropdown>
            </div>

            <!-- Hamburger -->
            <div class="-mr-2 flex items-center sm:hidden">
                <button @click="open = ! open"
                        class="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:bg-gray-100 focus:text-gray-500 transition duration-150 ease-in-out">
                    <svg class="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                        <path :class="{'hidden': open, 'inline-flex': ! open }" class="inline-flex"
                              stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M4 6h16M4 12h16M4 18h16"/>
                        <path :class="{'hidden': ! open, 'inline-flex': open }" class="hidden" stroke-linecap="round"
                              stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"/>
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
    <div :class="{'block': open, 'hidden': ! open}" class="hidden sm:hidden">
        <div class="pt-2 pb-3 space-y-1">
            <x-jet-responsive-nav-link href="{{ route('work-report.index') }}"
                                       :active="request()->routeIs('work-report.index')">
                {{ __('Darba laiks') }}
            </x-jet-responsive-nav-link>
            @can('view users')
                <x-jet-responsive-nav-link href="{{ route('users.index') }}"
                                           :active="request()->routeIs('users.index')">
                    {{ __('Darbinieki') }}
                </x-jet-responsive-nav-link>
            @endcan
            <x-jet-responsive-nav-link href="{{ route('vacations.index') }}"
                                       :active="request()->routeIs('vacations.index')">
                {{ __('Atvaļinājums') }}
            </x-jet-responsive-nav-link>
            @can('get reports')
                <x-jet-responsive-nav-link href="{{ route('employee-report.index') }}"
                                           :active="request()->routeIs('employee-report.index')">
                    {{ __('Atskaites') }}
                </x-jet-responsive-nav-link>
            @endcan
            @can('add users')
                <x-jet-responsive-nav-link href="{{ route('invite.index') }}"
                                           :active="request()->routeIs('invite.index')">
                    {{ __('Nosūtīt uzaicinājumu') }}
                </x-jet-responsive-nav-link>
            @endcan
            @if (!auth()->user()->filledDataForm())
                <x-jet-responsive-nav-link class="text-red-500" href="/user/profile"
                                           :active="request()->routeIs('user-data.create')">
                    {{ __('Iesniegt datus') }}
                </x-jet-responsive-nav-link>
            @endif

        </div>

        <!-- Responsive Settings Options -->
        <div class="pt-4 pb-1 border-t border-gray-200">
            <div class="flex items-center px-4">
                <div class="flex-shrink-0">
                    <img class="h-10 w-10 rounded-full" src="{{ Auth::user()->profile_photo_url }}"
                         alt="{{ Auth::user()->name }}"/>
                </div>

                <div class="ml-3">
                    <div class="font-medium text-base text-gray-800">{{ Auth::user()->name }}</div>
                    <div class="font-medium text-sm text-gray-500">{{ Auth::user()->email }}</div>
                </div>
            </div>

            <div class="mt-3 space-y-1">
                <!-- Account Management -->
                <x-jet-responsive-nav-link href="{{ route('profile.show') }}"
                                           :active="request()->routeIs('profile.show')">
                    {{ __('Profils') }}
                </x-jet-responsive-nav-link>
                <x-jet-responsive-nav-link href="/user/profile#notes">
                    {{ __('Piezīmes') }}
                </x-jet-responsive-nav-link>

                <!-- Authentication -->
                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <x-jet-responsive-nav-link href="{{ route('logout') }}"
                                               onclick="event.preventDefault(); this.closest('form').submit();">
                        {{ __('Beigt darbu') }}
                    </x-jet-responsive-nav-link>
                </form>
            </div>
        </div>
    </div>
</nav>
