<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Profile') }}
        </h2>
    </x-slot>

    <div>

        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            @livewire('profile.update-profile-information-form')

            <div class="hidden sm:block">
                <div class="py-8">
                    <div class="border-t border-gray-200"></div>
                </div>
            </div>
            <div class="mt-10 sm:mt-0">
                @if (session('status'))
                    <div class="flex justify-center rounded-lg my-2">
                        <div class="mx-auto bg-white bg-opacity-75 sm:w-3/4 lg:w-1/2 p-2 mb-6 text-center border border-red-200 shadow rounded-lg">
                            {{ session('status') }}
                        </div>
                    </div>
                @endif
                @livewire('user-notes')
                    <div class="hidden sm:block">
                        <div class="py-8">
                            <div class="border-t border-gray-200"></div>
                        </div>
                    </div>
                    <div class="mt-10 sm:mt-0">
                        @if (session('status'))
                            <div class="flex justify-center rounded-lg my-2">
                                <div class="mx-auto bg-white bg-opacity-75 sm:w-3/4 lg:w-1/2 p-2 mb-6 text-center border border-red-200 shadow rounded-lg">
                                    {{ session('status') }}
                                </div>
                            </div>
                        @endif
                    @livewire('user-data')
            </div>
            @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::updatePasswords()))
                <x-jet-section-border />

                <div class="mt-10 sm:mt-0">
                    @livewire('profile.update-password-form')
                </div>

            @endif
        </div>
    </div>
</x-app-layout>
