<x-jet-form-section submit="updateProfileInformation">
    <x-slot name="title">
        {{ __('Profila informācija') }}
    </x-slot>

    <x-slot name="description">
        {{ __('') }}
    </x-slot>

    <x-slot name="form">

        <!-- Name -->
        <div class="col-span-6 sm:col-span-4">
            <h1 class="text-sm text-gray-800 mb-2">Vārds, Uzvārds</h1>
            <p class="text-lg">{{ auth()->user()->name }}</p>
        </div>

        <!-- Email -->
        <div class="col-span-6 sm:col-span-4">
            <x-jet-label for="email" value="{{ __('E-pasts') }}" />
            <x-jet-input id="email" type="email" class="mt-1 block w-full" wire:model.defer="state.email" />
            <x-jet-input-error for="email" class="mt-2" />
        </div>
    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Dati atjaunoti') }}
        </x-jet-action-message>

        <x-jet-button wire:loading.attr="disabled" wire:target="photo">
            {{ __('Saglabāt') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>
