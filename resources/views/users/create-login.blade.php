<x-app-layout>

    <x-slot name="slot">

        <div class="flex justify-center mt-6 mb-24 rounded-lg">
            <div class="rounded-lg sm:w-3/4 lg:w-1/2 mt-6"  style="min-width: 300px">
                    <h2 class="font-semibold text-center text-xl text-gray-800 leading-tight p-4">
                        {{ __('Lūdzu ievadi savus personas datus') }}
                    </h2>
                <div class="bg-white border border-gray-200 rounded-lg shadow mt-6">
                    <form action="/users/store" method="POST">
                    @csrf
                        <div class="border-b p-6">
                            <div class="mb-3">
                                <label for="name">Vārds, Uzvārds</label>
                                <input type="text" name="name" required class="form-input w-full p-1 border mt-2" autofocus value="{{ old('name') }}">
                                <p class="text-gray-700 text-xs">Viktors Melnais</p>
                                    @error('name')
                                        <p class="text-red-500 text-xs">Jāievada vārds</p>
                                    @enderror
                            </div>
                            <div class="mb-3">
                                <label for="email">E-pasts</label>
                                <input type="email" name="email" required value="{{ $invite->email }}" class="form-input w-full p-1 border mt-2">
                                    @error('email')
                                        <p class="text-red-500 text-xs">{{ $message }}</p>
                                    @enderror
                            </div>
                            <input type="hidden" name="manager_id" value="{{ \App\Models\User::where('id', $invite->user_id)->first()->id }}">
                        </div>
                        <div class="border-b p-6">
                            <div class="mb-3">
                                <label for="phone">Telefons</label>
                                <input type="number" name="phone" id="phone" required class="form-input w-full p-1 border mt-2">
                                <span class="text-xs text-gray-800">12345678</span>
                            </div>
                            <div class="mb-3">
                                <p for="address" class="mb-2">Adrese (deklarētā)</p>
                                <div class="p-2">
                                    <div class="mb-2">
                                        <label for="address_street" class="text-sm text-gray-800">Iela</label>
                                        <input type="text" name="address_street" id="address_street" required class="form-input w-full p-1 border" placeholder="Iela, Māja, Dzīvoklis">
                                    </div>
                                    <div class="mb-2">
                                        <label for="address_city" class="text-sm text-gray-800">Pilsēta</label>
                                        <input type="text" name="address_city" id="address_city" required class="form-input w-full p-1 border" placeholder="Pilsēta">
                                    </div>
                                    <div class="mb-2">
                                        <label for="address_postal" class="text-sm text-gray-800">Pasta indekss</label>
                                        <input type="text" name="address_postal" id="address_postal" required class="form-input w-full p-1 border" placeholder="Pasta indekss">
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="personal_id">Personas kods</label>
                                <input type="text" name="personal_id" id="personal_id" required class="form-input w-full p-1 border mt-2">
                            </div>
                            <div class="mb-3">
                                <label for="bank_account">Bankas konts</label>
                                <input type="text" name="bank_account" id="bank_account" required class="form-input w-full p-1 border mt-2">
                            </div>
                        </div>
                        <div class="p-6">
                            <div class="my-3">
                                <label for="password">Jaunā parole</label>
                                <input type="password" name="password" required class="form-input w-full p-1 border mt-2">
                            </div>
                                    @error('password')
                                        <p class="text-red-500 text-xs">{{ $message }}</p>
                                    @enderror


                            <div class="mb-3">
                                <label for="password_confirmation">Apstiprini paroli</label>
                                <input type="password" name="password_confirmation" required class="form-input w-full p-1 border mt-2">
                                    @error('password_confirmation')
                                        <p class="text-red-500 text-xs">{{ $message }}</p>
                                    @enderror
                            </div>
                        </div>
                        <div class="flex justify-end mb-6 px-6">
                            <button type="submit" class="mt-4 mr-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                Saglabāt
                            </button>
                        <a href="/login"><button type="button" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 border-2 border-gray-800 rounded-md font-semibold text-xs uppercase tracking-widest hover:bg-gray-700 hover:text-white active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                            Atcelt
                        </button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </x-slot>

</x-app-layout>
