<x-app-layout>

    <x-slot name="slot">

        <div class="flex justify-center mt-6 mb-24 rounded-lg">
            <div class="rounded-lg sm:w-3/4 md:w-3/4 lg:w-1/2 w-full" style="min-width: 300px">
                <div class="flex justify-center mt-6">
                    <div class="w-full">
                        <div class=" text-center p-6">
                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                {{ $user->name }}
                            </h2>
                        </div>
                        <div class="items-center p-6 bg-white border border-gray-200 shadow rounded-lg w-full">
                            <div class="w-full">
                                <form action="/users/{{ $user->id }}" method="post">
                                    @csrf
                                    @method('patch')
                                    <div class="mb-4 w-full md:w-3/4 lg:w-1/2">
                                        <div class="py-4">
                                            <div>
                                                <div class="mb-3">
                                                    <x-jet-label for="name" value="{{ __('Vārds, Uzvārds') }}"/>
                                                    <input type="text" name="name" id="name"
                                                           class="form-input p-1 border mt-1 w-64"
                                                           value="{{ $user->name }}">
                                                </div>
                                                <div class="mb-3">
                                                    <x-jet-label for="email" value="{{ __('E-pasts') }}"/>
                                                    <input type="email" name="email" id="email"
                                                           class="form-input p-1 border mt-1 w-64"
                                                           value="{{ $user->email }}">
                                                </div>
                                                <div class="mb-3">
                                                    <x-jet-label for="phone" value="{{ __('Telefons') }}"/>
                                                    <input type="number" name="phone" id="phone"
                                                           class="form-input p-1 border mt-1 w-64"
                                                           value="{{ $user->phone }}">
                                                </div>
                                                <div class="mb-3">
                                                    <p class="text-sm text-gray-600 mb-2">Adrese</p>
                                                    <div class="pl-2">
                                                        <x-jet-label for="address_street" value="{{ __('Iela, Māja, Dzīvoklis') }}"/>
                                                        <input type="text" name="address_street"
                                                               id="address_street"
                                                               class="form-input p-1 border mt-1 w-64"
                                                               value="{{ $user->address_street }}"
                                                               placeholder="Iela, Māja, Dzīvoklis">
                                                        <x-jet-label for="address_city" value="{{ __('Pilsēta') }}"/>
                                                        <input type="text" name="address_city"
                                                               id="address_city"
                                                               class="form-input p-1 border mt-1 w-64"
                                                               value="{{ $user->address_city }}"
                                                               placeholder="Pilsēta">
                                                        <x-jet-label for="address_postal" value="{{ __('Pasta indekss') }}"/>
                                                        <input type="text" name="address_postal"
                                                               id="address_postal"
                                                               class="form-input p-1 border mt-1 w-64"
                                                               value="{{ $user->address_postal }}"
                                                               placeholder="LV-XXXX">
                                                    </div>

                                                </div>
                                                @role('grāmatvedis')
                                                    <div class="mb-3">
                                                        <x-jet-label for="bank_account" value="{{ __('Bankas konts') }}"/>
                                                        <input type="text" name="bank_account" id="bank_account"
                                                               class="form-input p-1 border mt-1 w-64"
                                                               value="{{ $user->bank_account }}">
                                                    </div>
                                                @endrole
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <a href="/users/{{ $user->id }}">
                                            <button type="button"
                                                    class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase
                                tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                Atpakaļ
                                            </button>
                                        </a>
                                        <button type="submit"
                                                class="my-4 md:my-2 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest
                            hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                            Saglabāt
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </x-slot>

</x-app-layout>

