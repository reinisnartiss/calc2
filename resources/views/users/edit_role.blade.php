<x-app-layout>

    <x-slot name="slot">



        <div class="flex justify-center mt-6 mb-24 rounded-lg">


            <div class="rounded-lg sm:w-3/4 md:w-3/4 lg:w-1/2 w-full"  style="min-width: 300px">

                <div class="flex justify-center mt-6">
                    <div class="w-full">
                        <div class=" text-center p-6">
                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                {{ $user->name }}
                            </h2>
                        </div>
                        @livewire('user-role', ['user' => $user, 'managers' => $managers, 'roles' => $roles])
                    </div>

                </div>

            </div>
        </div>

    </x-slot>

</x-app-layout>
