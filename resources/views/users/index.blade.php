<x-app-layout>

    <x-slot name="slot">

        <div class="flex justify-center mt-6 mb-24">
            <div class="rounded-lg sm:w-3/4 md:w-3/4 lg:w-full w-full" style="min-width: 300px">
                <div class="flex justify-center mt-6">
                    <div class="w-full">
                        <div x-data="{ tab: '#allUsers' }" class="">

                            <div class="flex flex-row justify-center mb-8">

                                @role('grāmatvedis')
                                <a class="px-4 text-center mr-6 border-b-2 border-gray-900 hover:border-blue-400"
                                   href="#" x-on:click.prevent="tab='#contracts'">Sagatavotie darba līgumi</a>
                                @endrole

                                @hasanyrole('grāmatvedis|super-admin')
                                <a class="px-4 text-center mr-6 border-b-2 border-gray-900 hover:border-blue-400"
                                   href="#" x-on:click.prevent="tab='#allUsers'">Visi darbinieki</a>
                                @endhasanyrole

                                @hasanyrole('vadītājs|super-admin')
                                <a class="px-4 text-center mr-6 border-b-2 border-gray-900 hover:border-blue-400"
                                   href="#" x-on:click.prevent="tab='#myUsers'">Mani darbinieki</a>
                                @endhasanyrole

                                @hasanyrole('vadītājs|super-admin|grāmatvedis')
                                <a class="px-4 text-center border-b-2 border-gray-900 hover:border-blue-400"
                                   href="#" x-on:click.prevent="tab='#blockedUsers'">Bloķētie darbinieki</a>
                                @endhasanyrole

                            </div>

                            <div x-show="tab == '#contracts'" x-cloak>
                                @include('work_contract.open-contracts')
                            </div>

                            <div x-show="tab == '#allUsers'" x-cloak>
                                <div class="p-3 md:p-6 bg-white border border-gray-200 rounded-none md:rounded-lg container mx-auto block md:grid md:grid-cols-3 lg:grid-cols-5 gap-3 space-y-3 md:space-y-0">
                                    @foreach ($users->where('is_blocked', false) as $user)
                                        <div class="overflow-hidden border border-gray-200 rounded-lg shadow group relative pb-10 mx-6 md:mx-0">
                                            <div class="col-span-2 overflow-hidden text-lg">
                                                <div class="h-12 bg-gray-100 group-hover:bg-black group-hover:text-white group-hover:font-bold flex items-center justify-center font-semibold">
                                                    {{ $user->name }}
                                                </div>
                                            </div>
                                            <div class="px-2 py-4 space-y-2 text-center md:text-left">
                                                <div>
                                                    <span><i class="fas fa-briefcase text-gray-500"></i></span>
                                                    {{ ucfirst($user->getRoleNames()->first()) }}
                                                </div>
                                                <div>
                                                    <span><i class="fas fa-user-tie text-gray-500"></i></span>
                                                    {{ $user->manager->name }}
                                                </div>
                                                <div>
                                                    <span><i class="far fa-envelope text-gray-500"></i></span>
                                                    {{$user->email}}
                                                </div>
                                                <div>
                                                    <span><i class="fas fa-phone-alt text-gray-500"></i></span>
                                                    {{$user->phone}}
                                                </div>
                                            </div>
                                            @canany(['edit users', 'get reports'])
                                                <a href="{{ route('users.show', $user) }}" class="absolute inset-x-0 bottom-0 h-10 w-full border-t flex items-center justify-center uppercase tracking-widest font-semibold bg-gray-100 text-xs group-hover:bg-black group-hover:text-white">
                                                        Skatīt
                                                </a>
                                            @endcanany
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div x-show="tab == '#myUsers'" x-cloak>
                                @if(auth()->user()->employees->first())
                                    <div class="p-3 md:p-6 bg-white border border-gray-200 rounded-lg container mx-auto block md:grid md:grid-cols-3 lg:grid-cols-5 gap-3 space-y-3 md:space-y-0">
                                    @foreach (auth()->user()->employees as $employee)
                                            <div class="overflow-hidden border {{ $employee->is_blocked ? 'border-red-300' : 'border-gray-200' }} rounded-lg shadow group relative pb-10 mx-6 md:mx-0">
                                                <div class="col-span-2 overflow-hidden text-lg">
                                                    <div class="h-12 {{ $employee->is_blocked ? 'bg-red-100 group-hover:bg-red-500' : 'bg-gray-100 group-hover:bg-black' }} group-hover:text-white group-hover:font-bold flex items-center justify-center font-semibold">
                                                        {{ $employee->name }}
                                                    </div>
                                                </div>
                                                <div class="px-2 py-4 space-y-2">
                                                    <div>
                                                        <span><i class="fas fa-briefcase text-gray-500"></i></span>
                                                        {{ ucfirst($employee->getRoleNames()->first()) }}
                                                    </div>
                                                    <div>
                                                        <span><i class="far fa-envelope text-gray-500"></i></span>
                                                        {{$employee->email}}
                                                    </div>
                                                    <div>
                                                        <span><i class="fas fa-phone-alt text-gray-500"></i></span>
                                                        {{$employee->phone}}
                                                    </div>
                                                </div>
                                                @canany(['edit users', 'get reports'])
                                                    <a href="{{ route('users.show', $employee) }}" class="absolute inset-x-0 bottom-0 h-10 w-full border-t flex items-center justify-center uppercase tracking-widest font-semibold {{ $employee->is_blocked ? 'bg-red-100 group-hover:bg-red-500' : 'bg-gray-100 group-hover:bg-black' }} text-xs group-hover:text-white">
                                                        Skatīt
                                                    </a>
                                                @endcanany
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <div class="p-6 bg-white border border-gray-200 rounded-lg w-full">
                                        <div class="block text-center overflow-hidden border border-gray-200 rounded-lg mb-2 shadow p-2">
                                            Lietotājam nav piesaistītu darbinieku
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div x-show="tab == '#blockedUsers'" x-cloak>
                                <div class="p-3 md:p-6 bg-white border border-gray-200 rounded-none md:rounded-lg container mx-auto block md:grid md:grid-cols-3 lg:grid-cols-5 gap-3 space-y-3 md:space-y-0">
                                    @forelse ($users->where('is_blocked', true) as $user)
                                        <div class="overflow-hidden border border-red-300 rounded-lg shadow group relative pb-10 mx-6 md:mx-0">
                                            <div class="col-span-2 overflow-hidden text-lg">
                                                <div class="h-12 bg-red-100 group-hover:bg-red-500 group-hover:text-white group-hover:font-bold flex items-center justify-center font-semibold">
                                                    {{ $user->name }}
                                                </div>
                                            </div>
                                            <div class="px-2 py-4 space-y-2">
                                                <div>
                                                    <span><i class="fas fa-briefcase text-gray-500"></i></span>
                                                    {{ ucfirst($user->getRoleNames()->first()) }}
                                                </div>
                                                <div>
                                                    <span><i class="fas fa-user-tie text-gray-500"></i></span>
                                                    {{ $user->manager->name }}
                                                </div>
                                                <div>
                                                    <span><i class="far fa-envelope text-gray-500"></i></span>
                                                    {{$user->email}}
                                                </div>
                                                <div>
                                                    <span><i class="fas fa-phone-alt text-gray-500"></i></span>
                                                    {{$user->phone}}
                                                </div>
                                            </div>
                                            @canany(['edit users', 'get reports'])
                                                <a href="{{ route('users.show', $user) }}" class="absolute inset-x-0 bottom-0 h-10 w-full border-t flex items-center justify-center uppercase tracking-widest font-semibold bg-red-100 text-xs group-hover:bg-red-500 group-hover:text-white">
                                                    Skatīt
                                                </a>
                                            @endcanany
                                        </div>
                                    @empty
                                        <div class="md:col-span-3 lg:col-span-5">
                                            <div class="block text-center overflow-hidden border border-gray-200 rounded-lg mb-2 shadow p-2">
                                                Nav neviena bloķēta darbinieka
                                            </div>
                                        </div>
                                    @endforelse
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </x-slot>

</x-app-layout>
