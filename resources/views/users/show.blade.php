<x-app-layout>

    <x-slot name="slot">


        <div class="flex justify-center mt-6 mb-24 rounded-lg">


            <div class="rounded-lg sm:w-3/4 md:w-3/4 lg:w-1/2 w-full" style="min-width: 300px">
                @if (session('status'))
                    <div class="flex justify-center rounded-lg my-8">
                        <div class="mx-auto bg-white bg-opacity-75 sm:w-3/4 lg:w-1/2 p-2 mt-6 text-center border border-green-200 shadow rounded-lg">
                            {{ session('status') }}
                        </div>
                    </div>
                @endif
                <div class="flex justify-center mt-6">

                    <div class="w-full">
                        <div class="text-center p-6">
                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                {{ $user->name }}
                            </h2>
                        </div>
                        <div>
                            <div
                                class="items-center p-6 bg-white border border-gray-200 shadow rounded-none md:rounded-lg w-full">
                                <div class="w-full">
                                    <div class="pb-4">
                                        <a href="/users">
                                            <button type="button"
                                                    class="inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                Atpakaļ
                                            </button>
                                        </a>
                                    </div>
                                    @if($user->is_blocked)
                                        <div class="rounded-lg p-2 my-2 border border-red-400 bg-red-100 text-red-500 text-center">
                                            Lietotājs ir bloķēts!
                                        </div>
                                    @endif
                                    <div class="border rounded-lg">
                                        <div class="block md:grid md:grid-cols-2 text-center p-4 relative border-b">
                                            <div class="col-span-1 md:border-r mb-4 md:mb-0">
                                                <h1 class="text-sm mb-2">Pozīcija</h1>
                                                <p>{{ ucfirst($user->getRoleNames()->first()) }}</p>
                                            </div>
                                            <div class="col-span-1">
                                                <h1 class="text-sm mb-2">Vadītājs</h1>
                                                <p>{{ $user->manager->name }}</p>
                                            </div>
                                            @hasanyrole('super-admin|vadītājs')
                                                <a href="/users/{{ $user->id }}/edit-role" class="absolute top-0 right-0 m-2 text-gray-400 text-opacity-50 hover:text-opacity-100 hover:text-gray-600"><i class="fas fa-pen"></i></a>
                                            @endhasanyrole
                                        </div>
                                        <div class="border-b p-4">
                                            <p class="text-center text-sm text-gray-800">Darba līgumi</p>
                                            @role('grāmatvedis')
                                            <div class="mb-4">
                                                <a href="/work-contract/{{ $user->id }}/create">
                                                    <button type="button"
                                                            class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-gray-800 border-2 border-gray-800 rounded-md font-semibold text-xs text-white hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                        Jauns darba līgums
                                                    </button>
                                                </a>
                                            </div>
                                            @endrole
                                            @forelse($user->companies as $company)
                                                <div class="py-3 {{ $loop->last ? '' : 'border-b' }}">
                                                    <div>
                                                        <p class="my-2">Uzņēmums: <span
                                                                class="mx-4">{{ $company->name }}</span></p>
                                                        @if($company->pivot->contract_number != null)
                                                            <p class="my-2">Darba līguma numurs: <span
                                                                    class="mx-4">{{ $company->pivot->contract_number }}</span>
                                                            </p>
                                                        @endif
                                                        @isset($company->pivot->starting_date)
                                                            <p class="my-2">Spēkā no: <span class="text-lg mx-4">{{ \Illuminate\Support\Carbon::parse($company->pivot->starting_date)->format('d.m.Y.') }}</span></p>
                                                        @endisset
                                                    </div>
                                                    @if($user->contracts()->where('company_id', $company->id)->first())
                                                        <div class="mt-4">
                                                            <a href="/work-contract/{{ $user->contracts()->where('company_id', $company->id)->first()->id }}">
                                                                <button type="button"
                                                                        class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                                    Skatīt darba līgumu
                                                                </button>
                                                            </a>
                                                        </div>
                                                    @endif
                                                </div>
                                            @empty
                                                <div class="py-3">
                                                    Lietotājam nav darba līguma.
                                                </div>
                                            @endforelse
                                        </div>
                                        <div class="p-4 mb-4 relative">
                                            <p class="text-center text-sm text-gray-800">Lietotāja dati</p>
                                            @if (!$user->filledDataForm())
                                                <div class="my-4 bg-white w-full text-red-500">Visi personas dati vēl
                                                    nav iesniegti
                                                </div>
                                            @endif
                                            <div class="mb-4">
                                                E-pasts: {{ $user->email ? $user->email : '' }}
                                            </div>
                                            <div class="mb-4">
                                                Telefona numurs: {{ $user->phone ? $user->phone : '' }}
                                            </div>
                                            <div class="mb-4">
                                                Adrese: {{ ($user->address_street && $user->address_city && $user->address_postal) ? $user->address_street . ', ' . $user->address_city . ', ' . $user->address_postal : '' }}
                                            </div>
                                            @role('grāmatvedis')
                                                <div class="mb-4">
                                                    Bankas konts: {{ $user->bank_account ? $user->bank_account : '' }}
                                                </div>
                                            @endrole
{{--                                            <div>--}}
{{--                                                Personas kods: {{ !empty($user->personal_id) ? \Illuminate\Support\Facades\Crypt::decryptString($user->personal_id) : '' }}--}}
{{--                                            </div>--}}
                                            <a href="/users/{{ $user->id }}/edit" class="absolute top-0 right-0 m-2 text-gray-400 text-opacity-50 hover:text-opacity-100 hover:text-gray-600"><i class="fas fa-pen"></i></a>
                                        </div>
                                    </div>
                                    @hasanyrole('super-admin|vadītājs|grāmatvedis')
                                    <div class="flex justify-between my-4">
                                        <div>
                                            Darbības ar lietotāju:
                                        </div>
                                        <div class="flex justify-end">
                                            <div class="mr-2">
                                                <form action="/users/{{ $user->id }}/block" method="post">
                                                    @csrf
                                                    @method('patch')
                                                    <button type="submit"
                                                            onclick="return {{ $user->is_blocked ? '' : 'confirm("Vai tiešām vēlaties bloķēt šo lietotāju?")' }}"
                                                            class="inline-flex items-center px-4 py-2 bg-white border-2 border-yellow-300 rounded-md font-semibold text-xs text-yellow-400 hover:text-white uppercase tracking-widest hover:bg-yellow-300 active:bg-yellow-400 focus:outline-none focus:border-yellow-400 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                        {{ $user->is_blocked ? 'Atbloķēt' : 'Bloķēt' }}
                                                    </button>
                                                </form>
                                            </div>
                                            <div class="flex justify-end">
                                                <form action="/users/{{ $user->id }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit"
                                                            onclick="return confirm('Vai tiešām vēlaties dzēst šo lietotāju?')"
                                                            class="inline-flex items-center px-4 py-2 bg-white border-2 border-red-600 rounded-md font-semibold text-xs text-red-800 hover:text-white uppercase tracking-widest hover:bg-red-600 active:bg-red-700 focus:outline-none focus:border-red-700 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                        Izdzēst
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    @endhasanyrole
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </x-slot>

</x-app-layout>
