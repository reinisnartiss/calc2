<x-app-layout>

    <x-slot name="slot">


        <div class="flex justify-center mt-6 mb-24 rounded-lg">


            <div class="rounded-lg w-full    lg:w-3/4 w-full" style="min-width: 300px">

                <div class="flex justify-center mt-6">
                    <div class="w-full">
                        <div class=" text-center p-6">
                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                {{ $vacation->user->name }}
                            </h2>
                        </div>
                        <div class="p-6 bg-white border border-gray-200 rounded-lg w-full">
                            <div class="mb-4">
                                <a href="/vacations/{{ $vacation->id }}">
                                    <button type="button" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                        Atpakaļ
                                    </button>
                                </a>
                            </div>
                            <div class="border border-gray-200 shadow p-4 rounded-lg mb-2">
                                <form action="/vacations/{{ $vacation->id }}/update-dates" method="post">
                                    @csrf
                                    @method('patch')

                                    <div class=" md:grid grid-cols-4 items-center">
                                        <div class="text-center mb-2 col-span-1">
                                            <span class="text-sm text-gray-800 block mb self-center">Darbinieks:</span>
                                            <span>{{ $vacation->user->name }}</span>
                                        </div>
                                        <div class="text-center mb-2 col-span-1">
                                            <span class="text-sm text-gray-800 block mb self-center">No:</span>
                                            <input type="date" name="start_date" value="{{ $vacation->start_date->format('Y-m-d') }}" class="form-input">
                                            @error('start_date')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                                        </div>
                                        <div class="text-center mb-2 col-span-1">
                                            <span class="text-sm text-gray-800 block self-center">Līdz:</span>
                                            <input type="date" name="end_date" value="{{ $vacation->end_date->format('Y-m-d') }}" class="form-input">
                                            @error('end_date')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                                        </div>
                                        <div class="text-center mb-2 col-span-1 self-center">
                                            <button type="submit" id="btn" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                Saglabāt
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </x-slot>

</x-app-layout>
