<p align="right">{{ $company->name }}</p>
<p align="right">Reģ. Nr. {{ $company->registration_number }}</p>
<p align="right">{{ $company->director_role_dativs }}</p>
<p align="right">{{ $company->director()->name_dativs }}</p>
<p></p>
<p></p>

<p align="right">{{ $user->name_genitivs }}</p>
<p align="right">
    p.k. {{ !empty($user->personal_id) ? \Illuminate\Support\Facades\Crypt::decryptString($user->personal_id) : '' }}</p>
<p></p>
<p></p>

<p align="center"><strong>IESNIEGUMS</strong></p>
<table border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td width="173" valign="top">
            <p>{{ \Illuminate\Support\Carbon::parse($iesnieguma_date)->format('d.m.Y.') }}</p>
        </td>
        <td width="472" valign="top">
        </td>
    </tr>
    </tbody>
</table>
<p></p>
<p></p>

@if($vacation->unpaid_leave == true)
    <p>Lūdzu piešķirt man atvaļinājumu bez darba algas saglabāšanas no {{ $vacation->start_date->format('d.m.Y.') }}
        līdz {{ $vacation->end_date->format('d.m.Y.') }} </p>
@else
    <p>Lūdzu piešķirt man ikgadējo apmaksāto atvaļinājumu no {{ $vacation->start_date->format('d.m.Y.') }} līdz
        {{ $vacation->end_date->format('d.m.Y.') }} Atvaļinājuma naudu, lūdzu, izmaksāt kopā ar darba algu.</p>
@endif
<p></p>
<p></p>
<div width="500" align="left">
    <p>___________________________</p>
    <p style="text-indent: 60px; font-size: smaller"><em>paraksts</em>
    </p>
</div>
