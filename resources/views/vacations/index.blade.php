<x-app-layout>

    <x-slot name="slot">

        <div class="flex justify-center mt-6 mb-24">
            <div class="rounded-lg sm:w-3/4 md:w-3/4 lg:w-4/5 w-full" style="min-width: 300px">
                <div class="flex justify-center mt-6">
                    <div class="w-full">
                        @if (session('status'))
                            <div class="flex justify-center rounded-lg my-8">
                                <div class="mx-auto bg-white bg-opacity-75 sm:w-3/4 lg:w-1/2 p-2 mt-6 text-center border border-green-200 shadow rounded-lg">
                                    {{ session('status') }}
                                </div>
                            </div>
                        @endif
                        <div class="text-center p-6">
                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                {{ __('Atvaļinājuma pieprasījums') }}
                            </h2>
                        </div>
                        <div class="p-6 bg-white border border-gray-200 rounded-none md:rounded-lg w-full mb-6">
                            <form action="{{ route('vacations.store') }}" method="post">
                                @csrf
                                <div class="text-center flex-wrap mb-4">
                                    <div class="text-lg mb-4">Pieejamās atvaļinājuma dienas:</div>
                                    <div class="block md:flex justify-center">
                                        @forelse($user->companies as $company)
                                            <div class="border-b p-2 my-1 w-1/2 mx-auto md:mx-1 md:w-auto {{ $loop->first ? 'border-t md:border-t-0' : '' }}">
                                                <div class="text-sm">
                                                    {{ $company->name }}
                                                </div>
                                                <div>
                                                    {{ $user->getAvailableVacationDays($company) }}
                                                </div>
                                            </div>
                                        @empty
                                            <div class="text-sm text-red-400">
                                                Nav noslēgts darba līgums
                                            </div>
                                        @endforelse
                                    </div>
                                </div>
                                @if($user->companies()->first() !== null)

                                    @livewire('vacation-request-create', ['user' => $user])

                                @endif

                            </form>
                        </div>

                        <div class="flex justify-center flex-wrap mt-6 mb-4 rounded-lg">
                            <div class="w-full block">

                                <div x-data="{  @if(request()->has('waiting')) tab:'#waitingApproval'
                                                @elseif(request()->has('approved')) tab:'#approvedVacations'
                                                @elseif(request()->has('ended')) tab:'#endedVacations'
                                                @elseif(request()->has('user_vacations')) tab:'#myVacations'
                                                @elseif(auth()->user()->hasRole('darbinieks')) tab: '#myVacations'
                                                @elseif(auth()->user()->hasRole('grāmatvedis')) tab:'#approvedVacations'
                                                @elseif(auth()->user()->hasPermissionTo('approve vacations') || auth()->user()->hasAnyRole('vadītājs|super-admin')) tab:'#waitingApproval'
                                                @endif }">

                                    @hasanyrole('vadītājs|grāmatvedis|super-admin')

                                        <div class="flex flex-row justify-center flex-wrap py-10">
                                                <a class="px-4 text-center mx-3 mb-4 lg:mb-0 border-b-2 border-gray-900 hover:border-blue-400"
                                                   href="#" x-on:click.prevent="tab='#calendar'">Kalendārs</a>

                                                <a class="px-4 text-center mx-3 mb-4 lg:mb-0 border-b-2 border-gray-900 hover:border-blue-400"
                                                   href="#" x-on:click.prevent="tab='#waitingApproval'">Gaida apstiprinājumu</a>

                                                <a class="px-4 text-center mx-3 mb-4 lg:mb-0 border-b-2 border-gray-900 hover:border-blue-400"
                                                   href="#" x-on:click.prevent="tab='#approvedVacations'">Apstiprinātie / aktīvie atvaļinājumi</a>

                                                <a class="px-4 text-center mx-3 mb-4 lg:mb-0 border-b-2 border-gray-900 hover:border-blue-400"
                                                   href="#" x-on:click.prevent="tab='#endedVacations'">Beigušies / neapstiprinātie atvaļinājumi</a>

                                                <a class="px-4 text-center mx-3 mb-4 lg:mb-0 border-b-2 border-gray-900 hover:border-blue-400"
                                                    href="#" x-on:click.prevent="tab='#myVacations'">Mani atvaļinājumi</a>
                                        </div>

                                    @endhasanyrole

                                    <div x-show="tab == '#calendar'" x-cloak>
                                        <div class="p-6 bg-white border border-gray-200 rounded-none md:rounded-lg w-full">
                                            <div id="calendar"></div>
                                            @push('scripts')
                                                <script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.6/index.global.min.js'></script>
                                                <script>
                                                    document.addEventListener('DOMContentLoaded', function () {
                                                        var calendarEl = document.getElementById('calendar');
                                                        var calendar = new FullCalendar.Calendar(calendarEl, {
                                                            initialView: 'dayGridMonth',
                                                            locale: 'lv',
                                                            height: 650,
                                                            firstDay: 1,
                                                            events: @json($events),
                                                        });
                                                        calendar.render();
                                                    });
                                                </script>
                                            @endpush
                                        </div>
                                    </div>

                                    <div x-show="tab == '#waitingApproval'" x-cloak>

                                        <div class="text-center p-6">
                                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                                {{ __('Gaida apstiprinājumu') }}
                                            </h2>
                                        </div>
                                        <div class="p-6 bg-white border border-gray-200 rounded-none md:rounded-lg w-full">
                                            @forelse($waitingApprovalVacations as $waitingApprovalVacation)
                                                <form action="/vacations/{{ $waitingApprovalVacation->id}}" method="get">
                                                    @csrf
                                                    <div class="border border-gray-200 shadow p-4 rounded-lg mb-2 md:grid grid-cols-5">
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block mb self-center">Darbinieks:</span>
                                                            <span>{{ $waitingApprovalVacation->user->name }}</span>
                                                        </div>
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block mb self-center">No:</span>
                                                            <span>{{ $waitingApprovalVacation->start_date->day }}. {{ $waitingApprovalVacation->start_date->locale('lv')->shortMonthName }}, {{ $waitingApprovalVacation->start_date->year }}</span>
                                                        </div>
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block self-center">Līdz:</span>
                                                            <span>{{ $waitingApprovalVacation->end_date->day }}. {{ $waitingApprovalVacation->end_date->locale('lv')->shortMonthName }}, {{ $waitingApprovalVacation->end_date->year }}</span>
                                                        </div>
                                                        <div class="flex items-center justify-center col-span-2">
                                                            <div class="text-center rounded-lg bg-red-100 p-2" style="height: fit-content">
                                                                {{ $waitingApprovalVacation->status() }}
                                                            </div>
                                                            <div class="ml-4">
                                                                <button type="submit" class="lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                                    Skatīt
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            @empty
                                                <div class="border border-gray-200 shadow p-4 rounded-lg mb-2 text-center">
                                                    <span class="block mb self-center">Nav neviena pieprasīta atvaļinājuma</span>
                                                </div>
                                            @endforelse
                                            <div>{{ $waitingApprovalVacations->links() }}</div>
                                        </div>
                                    </div>

                                    <div x-show="tab == '#approvedVacations'" x-cloak>
                                        <div class="text-center p-6">
                                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                                {{ __('Aptiprinātie / aktīvie atvaļinājumi') }}
                                            </h2>
                                        </div>
                                        <div class="p-6 bg-white border border-gray-200 rounded-none md:rounded-lg w-full">
                                            @forelse($approvedAndActiveVacations as $approvedAndActiveVacation)
                                                <form action="/vacations/{{ $approvedAndActiveVacation->id }}" method="get">
                                                    @csrf
                                                    <div class="border border-gray-200 shadow p-4 rounded-lg mb-2 md:grid grid-cols-5">
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block mb self-center">Darbinieks:</span>
                                                            <span>{{ $approvedAndActiveVacation->user->name }}</span>
                                                        </div>
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block mb self-center">No:</span>
                                                            <span>{{ $approvedAndActiveVacation->start_date->day }}. {{ $approvedAndActiveVacation->start_date->locale('lv')->shortMonthName }}, {{ $approvedAndActiveVacation->start_date->year }}</span>
                                                        </div>
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block self-center">Līdz:</span>
                                                            <span>{{ $approvedAndActiveVacation->end_date->day }}. {{ $approvedAndActiveVacation->end_date->locale('lv')->shortMonthName }}, {{ $approvedAndActiveVacation->end_date->year }}</span>
                                                        </div>
                                                        <div class="flex justify-center items-center col-span-2">
                                                            <div class="text-center rounded-lg bg-green-100 p-2" style="height: fit-content">
                                                                {{ $approvedAndActiveVacation->status() }}
                                                            </div>
                                                            <div class="ml-4">
                                                                <button type="submit" class="lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                                    Skatīt
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            @empty
                                                <div class="border border-gray-200 shadow p-4 rounded-lg mb-2 text-center">
                                                    <span class="block mb self-center">Nav neviena pieprasīta atvaļinājuma</span>
                                                </div>
                                            @endforelse
                                            <div>
                                                {{$approvedAndActiveVacations->links()}}
                                            </div>
                                        </div>
                                    </div>

                                    <div x-show="tab == '#endedVacations'" x-cloak>
                                        <div class="text-center p-6">
                                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                                {{ __('Beigušies / neapstiprinātie atvaļinājumi') }}
                                            </h2>
                                        </div>
                                        <div class="p-6 bg-white border border-gray-200 rounded-none md:rounded-lg w-full">
                                            @forelse($endedOrUnapprovedVacations as $endedOrUnapprovedVacation)
                                                <form action="/vacations/{{ $endedOrUnapprovedVacation->id }}" method="get">
                                                    @csrf
                                                    <div class="border border-gray-200 shadow p-4 rounded-lg mb-2 md:grid grid-cols-5">
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block mb self-center">Darbinieks:</span>
                                                            <span>{{ $endedOrUnapprovedVacation->user->name }}</span>
                                                        </div>
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block mb self-center">No:</span>
                                                            <span>{{ $endedOrUnapprovedVacation->start_date->day }}. {{ $endedOrUnapprovedVacation->start_date->locale('lv')->shortMonthName }}, {{ $endedOrUnapprovedVacation->start_date->year }}</span>
                                                        </div>
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block self-center">Līdz:</span>
                                                            <span>{{ $endedOrUnapprovedVacation->end_date->day }}. {{ $endedOrUnapprovedVacation->end_date->locale('lv')->shortMonthName }}, {{ $endedOrUnapprovedVacation->end_date->year }}</span>
                                                        </div>
                                                        <div class="flex items-center justify-center col-span-2">
                                                            @if($endedOrUnapprovedVacation->status() == "Beidzies pieprasījuma termiņš")
                                                                <div class="text-center rounded-lg bg-red-100 p-2"
                                                                     style="height: fit-content">
                                                                    {{ $endedOrUnapprovedVacation->status() }}
                                                                </div>
                                                            @elseif($endedOrUnapprovedVacation->status() == "Pieprasījums noraidīts")
                                                                <div class="text-center rounded-lg bg-red-500 text-white p-2"
                                                                     style="height: fit-content">
                                                                    {{ $endedOrUnapprovedVacation->status() }}
                                                                </div>
                                                            @else
                                                                <div class="text-center rounded-lg bg-purple-200 p-2"
                                                                     style="height: fit-content">
                                                                    {{ $endedOrUnapprovedVacation->status() }}
                                                                </div>
                                                            @endif
                                                            <div class="ml-4">
                                                                <button type="submit" class="lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                                    Skatīt
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            @empty
                                                <div class="border border-gray-200 shadow p-4 rounded-lg mb-2 text-center">
                                                    <span class="block mb self-center">Nav neviena pieprasīta atvaļinājuma</span>
                                                </div>
                                            @endforelse
                                                <div>
                                                    {{$endedOrUnapprovedVacations->links()}}
                                                </div>
                                        </div>
                                    </div>

                                    <div x-show="tab == '#myVacations'" x-cloak>
                                        <div class="text-center p-6">
                                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                                {{ __('Mani atvaļinājumi') }}
                                            </h2>
                                        </div>
                                        <div class="p-6 bg-white border border-gray-200 rounded-none md:rounded-lg w-full">
                                            @forelse($userVacations as $userVacation)
                                                <form action="/vacations/{{ $userVacation->id }}" method="get">
                                                    @csrf
                                                    <div class="border border-gray-200 shadow p-4 rounded-lg mb-2 md:grid grid-cols-5">
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block mb self-center">Darbinieks:</span>
                                                            <span>{{ $userVacation->user->name }}</span>
                                                        </div>
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block mb self-center">No:</span>
                                                            <span>{{ $userVacation->start_date->day }}. {{ $userVacation->start_date->locale('lv')->shortMonthName }}, {{ $userVacation->start_date->year }}</span>
                                                        </div>
                                                        <div class="text-center mb-2 col-span-1">
                                                            <span class="text-sm text-gray-800 block self-center">Līdz:</span>
                                                            <span>{{ $userVacation->end_date->day }}. {{ $userVacation->end_date->locale('lv')->shortMonthName }}, {{ $userVacation->end_date->year }}</span>
                                                        </div>
                                                        <div class="flex items-center justify-center col-span-2">
                                                            @if($userVacation->status() == "Beidzies pieprasījuma termiņš")
                                                                <div class="text-center rounded-lg bg-red-100 p-2"
                                                                     style="height: fit-content">
                                                                    {{ $userVacation->status() }}
                                                                </div>
                                                            @elseif($userVacation->status() == "Pieprasījums noraidīts")
                                                                <div class="text-center rounded-lg bg-red-500 text-white p-2"
                                                                     style="height: fit-content">
                                                                    {{ $userVacation->status() }}
                                                                </div>
                                                            @else
                                                                <div class="text-center rounded-lg bg-purple-200 p-2"
                                                                     style="height: fit-content">
                                                                    {{ $userVacation->status() }}
                                                                </div>
                                                            @endif
                                                            <div class="ml-4">
                                                                <button type="submit" class="lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                                    Skatīt
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            @empty
                                                <div class="border border-gray-200 shadow p-4 rounded-lg mb-2 text-center">
                                                    <span class="block mb self-center">Nav neviena pieprasīta atvaļinājuma</span>
                                                </div>
                                            @endforelse
                                            <div>
                                                {{$userVacations->links()}}
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </x-slot>

</x-app-layout>

