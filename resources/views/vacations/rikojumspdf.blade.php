<p align="center">
    <strong>{{ $company->name }}</strong>
</p>
<p align="center">
    Reģ.Nr. {{ $company->registration_number }}
</p>
<p align="center">
    {{ $company->address }}
</p>
<hr>
<p></p>
<p></p>
<p align="center">
    <strong>R Ī K O J U M S</strong>
</p>
<table border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td width="173" valign="top" align="left">
            <p>{{ $rikojuma_date ? \Illuminate\Support\Carbon::parse($rikojuma_date)->format('d.m.Y.') : '' }}</p>
        </td>
        <td valign="top" width="309">
            <p align="right">Nr. {{ $order_number }}</p>
        </td>
    </tr>
    </tbody>
</table>
<p></p>
<p></p>
<p></p>
@if($vacation->unpaid_leave == true)
    <p><em>Par atvaļinājuma bez darba algas saglabāšanas piešķiršanu</em></p>
@else
    <p><em>Par ikgadējā atvaļinājuma piešķiršanu</em></p>
@endif
<p><em>{{ $user->name_dativs }}</em></p>

<p></p>
<p></p>

<p style="text-indent: 40px"><strong>{{ $user->name_dativs }}, </strong>p.k.
    {{ !empty($user->personal_id) ? \Illuminate\Support\Facades\Crypt::decryptString($user->personal_id) : '' }},
    pamatojoties uz Darba likuma @if($vacation->unpaid_leave == true) 153.pantu, piešķirt  atvaļinājumu
    bez darba algas saglabāšanas @else 149.pantu, piešķirt
        {{ \Carbon\CarbonPeriod::create($vacation->start_date, $vacation->end_date)->count() }}
        kalendārās dienas ilgu ikgadējo atvaļinājumu @endif no {{ $vacation->start_date->format('d.m.Y.') }} līdz
    {{ $vacation->end_date->format('d.m.Y.') }}</p>
<p style="text-indent: 40px">Pamatojums: iesniegums.</p>
<p></p>
<p></p>
<table border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td width="173" valign="top" align="left">
            <p>{{ ucfirst($company->director_role_nominativs) }}</p>
        </td>
        <td width="310" valign="top">
            <p align="right">{{ $company->director()->name }}</p>
        </td>
    </tr>
    </tbody>
</table>
<p></p>
<table border="0" cellspacing="0" cellpadding="0">
    <tbody>
    <tr>
        <td width="173" valign="top" align="left">
            <p>Ar rīkojumu iepazinos</p>
        </td>
        <td width="310" valign="top">
            <p align="right">{{ $user->name }}</p>
        </td>
    </tr>
    </tbody>
</table>

