<x-app-layout>

    <x-slot name="slot">


        <div class="flex justify-center mt-6 mb-24 rounded-lg">


            <div class="rounded-lg w-full lg:w-3/4" style="min-width: 300px">

                <div class="flex justify-center mt-6">
                    <div class="w-full">
                        <div class=" text-center p-6">
                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                {{ $vacation->user->name }}
                            </h2>
                        </div>
                        <div class="p-6 bg-white border border-gray-200 rounded-none md:rounded-lg w-full">
                            <div class="mb-4">
                                <a href="/vacations">
                                    <button type="button" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                        Atpakaļ
                                    </button>
                                </a>
                            </div>
                            <div class="border border-gray-200 shadow p-4 rounded-lg mb-2 relative">
                                @if(($vacation->user->id == auth()->user()->id && $vacation->manager_approval == false && $vacation->not_approved == false) || auth()->user()->hasRole('super-admin'))
                                    <a href="/vacations/{{ $vacation->id }}/edit" class="absolute top-0 right-0 m-2 text-gray-400 text-opacity-50 hover:text-opacity-100 hover:text-gray-600"><i class="fas fa-pen"></i></a>
                                    <form action="/vacations/{{ $vacation->id }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" onclick="return confirm('Vai vēlies izdzēst pieprasīto atvaļinājumu?')" class="absolute top-0 left-0 m-2 text-gray-400 text-opacity-50 hover:text-opacity-100 hover:text-gray-600"><i class="fas fa-trash"></i></button>
                                    </form>
                                @endif
                                <div class=" md:grid grid-cols-5 mt-0 md:mt-4">
                                    <div class="text-center mb-2 col-span-1">
                                        <span class="text-sm text-gray-800 block mb self-center">Darbinieks:</span>
                                        <span>{{ $vacation->user->name }}</span>
                                    </div>
                                    <div class="text-center mb-2 col-span-1">
                                        <span class="text-sm text-gray-800 block mb self-center">No:</span>
                                        <span>{{ $vacation->start_date->day }}. {{ $vacation->start_date->locale('lv')->shortMonthName }}, {{ $vacation->start_date->year }}</span>
                                    </div>
                                    <div class="text-center mb-2 col-span-1">
                                        <span class="text-sm text-gray-800 block self-center">Līdz:</span>
                                        <span>{{ $vacation->end_date->day }}. {{ $vacation->end_date->locale('lv')->shortMonthName }}, {{ $vacation->end_date->year }}</span>
                                    </div>
                                    <div class="flex justify-center items-center col-span-2">
                                        @if($vacation->status() == "Apstiprināts")
                                            <div class="text-center rounded-lg bg-green-100 p-2"
                                                 style="height: fit-content">
                                                {{ $vacation->status() }}
                                            </div>
                                        @elseif($vacation->status() == "Darbinieks ir atvaļinājumā")
                                            <div
                                                class="text-center rounded-lg border-2 border-orange-200 bg-orange-100 p-2"
                                                style="height: fit-content">
                                                {{ $vacation->status() }}
                                            </div>
                                        @elseif($vacation->status() == "Gaida galējo apstiprinājumu")
                                            <div class="text-center rounded-lg bg-yellow-100 p-2"
                                                 style="height: fit-content">
                                                {{ $vacation->status() }}
                                            </div>
                                        @elseif($vacation->status() == "Pieprasījums noraidīts")
                                            <div class="text-center rounded-lg bg-red-500 text-white p-2"
                                                 style="height: fit-content">
                                                {{ $vacation->status() }}
                                            </div>
                                        @elseif($vacation->status() == "Atvaļinājums beidzies")
                                            <div class="text-center rounded-lg bg-purple-200 p-2"
                                                 style="height: fit-content">
                                                {{ $vacation->status() }}
                                            </div>
                                        @else
                                            <div class="text-center rounded-lg bg-red-100 p-2"
                                                 style="height: fit-content">
                                                {{ $vacation->status() }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="">
                                    @if(auth()->user()->hasRole('grāmatvedis') && in_array($vacation->status(), ['Apstiprināts', 'Atvaļinājums beidzies', 'Darbinieks ir atvaļinājumā']))
                                        @forelse($vacation->companies as $company)
                                            <div class="p-4 {{ $loop->last ? '' : 'border-b border-gray-200' }}">
                                                <form action="/vacations/{{ $vacation->id }}/export" target="_blank">
                                                    <input type="hidden" name="company_id" value="{{ $company->id }}">
                                                    <div class="mb-2">
                                                        {{ $company->name }}
                                                    </div>
                                                    <div class="ml-4 text-center md:text-left block md:flex items-end flex-wrap">
                                                        <div class="py-2 my-2 md:py-0 md:px-2 border-dashed md:border-b-0 md:border-r md:flex items-end">
                                                            <div class="mr-0 md:mr-2 mb-2 md:mb-0">
                                                                <label for="iesnieguma_date" class="text-sm text-gray-800 block">Iesnieguma datums:</label>
                                                                <input type="date" class="form-input" id="iesnieguma_date" name="iesnieguma_date" required value="{{ \Illuminate\Support\Carbon::now()->format('Y-m-d') }}">
                                                            </div>
                                                            <button type="submit" name="action" value="iesniegums"
                                                                    class="lg:my-0 md:my-0 mr-2 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                                Iesniegums PDF
                                                            </button>
                                                        </div>
                                                        <div class="px-2 my-2 block md:flex items-end">
                                                            <div class="mr-0 md:mr-2 mb-2 md:mb-0">
                                                                <label for="rikojuma_date" class="text-sm text-gray-800 block">Rīkojuma datums:</label>
                                                                <input type="date" class="form-input" id="rikojuma_date" name="rikojuma_date" required value="{{ \Illuminate\Support\Carbon::now()->format('Y-m-d') }}">
                                                            </div>
                                                            <div class="my-2 md:my-0 mx-2">
                                                                <label for="order_number" class="text-sm text-gray-800 block">Rīkojuma numurs:</label>
                                                                <input type="text" name="order_number" id="order_number"
                                                                       class="form-input" required>
                                                            </div>
                                                            <button type="submit" name="action" value="rikojums"
                                                                    class="lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                                Rīkojums PDF
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        @empty
                                        @endforelse
                                    @endif
                                </div>
                            </div>
                            @if($vacation->user->manager->id == auth()->user()->id && $vacation->status() == 'Gaida vadītāja apstiprinājumu')
                                <div class="p-4 flex justify-center">
                                    @if($vacation->status() == 'Gaida vadītāja apstiprinājumu')
                                        <form action="/vacations/{{ $vacation->id }}/update-status"
                                              class="md:flex items-center justify-center" method="post">
                                            @csrf
                                            @method('put')
                                            <div class="mr-0 md:mr-4">
                                                <input type="hidden" name="vacation_status" value="accept">
                                                <button type="submit" id="btn"
                                                        class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-green-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-green-200 hover:text-green-700 active:bg-green-700 focus:outline-none focus:border-green-800 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                    Apstiprināt
                                                </button>
                                            </div>
                                        </form>

                                        <form action="/vacations/{{ $vacation->id }}/update-status"
                                              class="md:flex items-center justify-center" method="post">
                                            @csrf
                                            @method('put')
                                            <div class="mr-0 md:mr-4">
                                                <input type="hidden" name="vacation_status" value="reject">
                                                <button type="submit" id="btn"
                                                        class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-red-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-200 hover:text-red-700 active:bg-red-700 focus:outline-none focus:border-red-800 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                    Noraidīt
                                                </button>
                                            </div>
                                        </form>

                                    @else
                                        <div class="md:flex items-center text-center">
                                            <div class="mx-auto">
                                                Vadītāja apstiprinājums jau iedots.
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endif
                            @if($vacation->user->companies()->first()->vacationManager->id == auth()->user()->id && in_array($vacation->status(), ["Pieprasījums noraidīts", "Gaida vadītāja apstiprinājumu", "Gaida galējo apstiprinājumu"]))
                                <div class="p-4 flex justify-center">
                                    <form action="/vacations/{{ $vacation->id }}/update-status"
                                          class="md:flex items-center justify-center" method="post">
                                        @csrf
                                        @method('put')
                                        <div class="mr-0 md:mr-4">
                                            <input type="hidden" name="vacation_status" value="accept">
                                            <button type="submit" id="btn"
                                                    class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-green-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-green-200 hover:text-green-700 active:bg-green-700 focus:outline-none focus:border-green-800 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                Apstiprināt
                                            </button>
                                        </div>
                                    </form>

                                    <form action="/vacations/{{ $vacation->id }}/update-status"
                                          class="md:flex items-center justify-center" method="post">
                                        @csrf
                                        @method('put')
                                        <div class="mr-0 md:mr-4">
                                            <input type="hidden" name="vacation_status" value="reject">
                                            <button type="submit" id="btn"
                                                    class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-red-400 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-red-200 hover:text-red-700 active:bg-red-700 focus:outline-none focus:border-red-800 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                Noraidīt
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            @elseif($vacation->not_approved == true)
                                <div class="text-center">
                                    <p class="text-sm text-red-400">Lūdzu griezies pie sava tiešā vadītāja lai saņemtu atvaļinājuma noraidīšanas iemeslu</p>
                                </div>
                            @endif
                            @if (session('comment_error'))
                                <div class="flex justify-center">
                                    <p class="text-sm text-red-600">{{ session('comment_error') }}</p>
                                </div>
                            @endif
                            <div class="py-2 block md:flex">
                                <div class="mb-2">
                                    <span class="text-sm text-gray-800">{{ $vacation->companies->count() > 1 ? 'Uzņēmumi:' : 'Uzņēmums:'}}</span>
                                </div>
                                <div class="ml-0 md:ml-2 block md:inline-block">
                                    @foreach($vacation->companies as $company)
                                        <span>{{ $company->name }}{{ $loop->last ? '' : ',' }}</span>
                                    @endforeach
                                </div>
                            </div>
                            <div class="py-2 block md:flex">
                                <div class="mb-2">
                                    <span class="text-sm text-gray-800">Veids:</span>
                                </div>
                                <div class="ml-0 md:ml-2 block md:inline-block">
                                    <p>{{ $vacation->unpaid_leave ? 'BEZALGAS' : 'APMAKSĀTS' }}</p>
                                </div>
                            </div>
                            <div class="py-2 block md:flex">
                                <div class="mb-2">
                                    <span class="text-sm text-gray-800">Izmantotās atvaļinājuma dienas:</span>
                                </div>
                                <div class="ml-0 md:ml-2 block md:inline-block">
                                    <p>{{ $vacation->vacation_days }}</p>
                                </div>
                            </div>
                            @foreach($vacation->companies as $company)
                                @php
                                    $vacationDays = $vacation->user->getAvailableVacationDays($company)
                                @endphp
                                @if($vacation->vacation_days > $vacationDays && in_array($vacation->status(), ['Gaida vadītāja apstiprinājumu', 'Gaida galējo apstiprinājumu'])  )
                                    <p class="text-sm text-red-400">{{ $company->name }}
                                        {{ $vacationDays == 1 ? 'ir pieejama' : 'ir pieejamas'}}
                                        {{ $vacationDays }} atvaļinājuma {{ $vacationDays == 1 ? 'diena' : 'dienas' }}.
                                        Tiks pārtērētas {{ $vacation->vacation_days - $vacationDays }} atvaļinājuma dienas</p>
                                @endif
                            @endforeach
                            <div class="py-2">
                                <div class="mb-2">
                                    <span class="text-sm text-gray-800">Jauns komentārs</span>
                                </div>
                                <div>
                                    <form action="/vacations/{{ $vacation->id }}/store-comment" method="post">
                                        @csrf
                                        <label for="comment"></label>
                                        <textarea name="comment" id="comment" cols="30" rows="2" class="form-input" placeholder="Komentārs..."></textarea>
                                        <div>
                                            <button type="submit" id="btn"
                                                    class="lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                Pievienot
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="py-4">
                                <div class="mb-2">
                                    <span class="text-sm text-gray-800">Komentāri</span>
                                </div>
                                <div class="border border-gray-200 rounded-lg">
                                    @forelse($vacation->comments->sortByDesc('created_at') as $comment)
                                        <div
                                            class="block md:grid grid-cols-3 p-2 {{ $loop->last ? '' : 'border-b border-gray-200' }}">
                                            <div class="md:col-span-2 grid grid-cols-2">
                                                <div class="md:col-span-1 text-center">
                                                    <span class="block text-sm text-gray-800 text-center">No:</span>
                                                    <p>{{ $comment->user->name }}</p>
                                                </div>
                                                <div class="md:col-span-1 text-center">
                                                        <span
                                                            class="block text-sm text-gray-800 text-center">Laiks:</span>
                                                    <p>{{ $comment->created_at->locale('lv')->diffForHumans() }}</p>
                                                </div>
                                            </div>

                                            <div class="md:col-span-1 text-center mt-2 md:mt-0">
                                                    <span
                                                        class="block text-sm text-gray-800 text-center">Komentārs:</span>
                                                <p>{{ $comment->body }}</p>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="p-2">Nav komentāru</div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </x-slot>

</x-app-layout>
