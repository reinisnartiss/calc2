@component('mail::message')
{{-- Greeting --}}
@if (! empty($greeting))
# {{ $greeting }}
@else
@if ($level === 'error')
# @lang('Whoops!')
@else
# @lang('Hey!')
@endif
@endif

<p style="text-align: justify">Jaunajā Parrot Group kalkulatorā ir atjaunota darba laika ievades sistēma, kas ļauj ievadīt datus uzreiz par
    vairākām dienām. Katrs darbinieks var apskatīt sev pieejamās atvaļinājuma dienas un veikt atvaļinājuma pieprasījumu.</p>

<p style="text-align: justify">Tu saņēmi šo epastu, jo vēlies iestatīt jaunu, vai esi aizmirsis esošo paroli.</p>
<p style="text-align: justify">Lai ievadītu jauno paroli, spied zemāk.</p>


@isset($actionText)
<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $actionUrl, 'color' => $color])
{{ 'Iestatīt paroli' }}
@endcomponent
@endisset

@endcomponent
