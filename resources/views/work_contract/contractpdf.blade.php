<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
    <div>
        <p align="center"><strong>DARBA LĪGUMS NR. {{ $workContract->contract_number }}</strong></p>
        <p align="left">Rīgā, {{ $workContract->date->format('d.m.Y') }}.</p>
        <p style="text-align: justify"><strong>SIA "{{ \Illuminate\Support\Str::after($workContract->company->name, 'SIA ') }}"</strong>, reģ. Nr. {{ $workContract->company->registration_number }}, juridiskā adrese
            {{ $workContract->company->address }} (tālāk – <strong>„Darba devējs”</strong>), kuru pārstāv
            {{ $workContract->company->director_role_nominativs }} {{ $workContract->company->director()->name }}, uz statūtu pamata,
            no vienas puses, un</p>
        <p style="text-align: justify"><strong>{{ $workContract->user->name }}</strong>, personas kods
            {{  !empty($workContract->user->personal_id) ? \Illuminate\Support\Facades\Crypt::decryptString($workContract->user->personal_id) : '' }},
            dzīvesvieta {{ $workContract->user->address_street }}, {{ $workContract->user->address_city }}, {{ $workContract->user->address_postal }}
            (tālāk – <strong>„Darbinieks”</strong>), no otras puses,
            turpmāk tekstā kopā sauktas – <strong>„Puses” </strong>vai katrs atsevišķi
            –<strong> „Puse”</strong>,</p>
        <p style="text-align: justify">bez maldības, viltus un spaidiem noslēdz šādu Līgumu, turpmāk tekstā – „ <strong>Līgums</strong>”:</p>

        <p align="center"></p>

        <p align="center">
            <strong>1.</strong>
            <strong>LĪGUMA PRIEKŠMETS</strong>
        </p>
        <p style="text-align: justify">1.1. Darba devējs pieņem darbā Darbinieku, un Darbinieks stājas darbā pie
            Darba devēja <em>{{ $workContract->position_genitivs }} </em>amatā, kas atbilst ar Ministru
            kabineta 23.05.2017. noteikumiem Nr. 264 apstiprinātā Profesiju
            klasifikatora kodam Nr. <em>{{ $workContract->position_id }}</em>.</p>
        <p style="text-align: justify">1.2. Darbinieks apņemas veikt noteikto darbu, ievērojot Darba devēja
            noteikto darba kārtību, instrukcijas, rīkojumus un citus Darba devēja
            noteiktos procesus, savukārt Darba devējs apņemas maksāt nolīgto darba
            samaksu un nodrošināt taisnīgus, drošus un veselībai nekaitīgus darba
            apstākļus.</p>
        <p align="center"></p>

        <p align="center"><strong>2. DARBA TIESISKO ATTIECĪBU TERMIŅŠ UN PĀRBAUDES LAIKS</strong></p>
        <p style="text-align: justify">2.1. Darba tiesiskās attiecības tiek uzsāktas ar <em>{{ $workContract->contract_start->year }}. gada {{ $workContract->contract_start->day }}. {{ $workContract->contract_start_month }}</em>.</p>
        @if($workContract->contract_period == 1)
            <p style="text-align: justify">2.2. Darba tiesiskās attiecības tiek nodibinātas uz <em> nenoteiktu laiku</em>.</p>
            <p style="text-align: justify">2.3. Puses vienojas, ka Darbiniekam <em>tiek noteikts 3 (trīs) mēnešu pārbaudes laiks</em>.</p>
        @elseif($workContract->contract_period == 2)
            <p style="text-align: justify">2.2. Darba tiesiskās attiecības tiek nodibinātas uz <em> noteiktu laiku līdz {{ $workContract->contract_end_date->year }}. gada {{ $workContract->contract_end_date->day }}. {{ $workContract->contract_end_date_month }}</em>.</p>
        @endif
        <p align="center"></p>

        <p align="center"><strong>3. DARBA VIETA UN LAIKS</strong></p>
        <p style="text-align: justify">3.1. Darbinieka darba vieta ir Darba devēja telpas,
            @if($workContract->company_id == 1)
                Skanstes iela 50,
            @else
                Rēzeknes iela 5B,
            @endif
            taču Darbinieku var nodarbināt dažādās vietās atkarībā no
            nepieciešamības.</p>
        <p style="text-align: justify">3.2. Darbinieks ir informēts un piekrīt, ka darba pienākumu pildīšana var
            ietvert komandējumus gan Latvijas Republikā, gan ārpus tās.</p>
        <p style="text-align: justify">3.3. Darbiniekam tiek noteikts
            @if($workContract->work_hours_type == 1)
                <em>nepilnais darba laiks
                    @elseif($workContract->work_hours_type == 2)
                        <em>normālais darba laiks
                            @else
                                <em>summētais darba laiks
                                    @endif
                    – {{ $workContract->weekly_hours }} ({{ NumberFormatter::create("lv", NumberFormatter::SPELLOUT)->format($workContract->weekly_hours) }}) stundas nedēļā.</em></p>
        <p style="text-align: justify">3.4. Virsstundu darbs tiek rakstveidā saskaņots ar Darbinieku. Par
            virsstundu darbu nav uzskatāms darbs, kas nav paveikts noteiktajā laikā
            (termiņā) Darbinieka vainas vai nepienācīgas darba izpildes dēļ, kā arī
            darbs, kas Darbiniekam saskaņā ar darba pienākumiem jāveic normālajā darba
            laikā.</p>
        @if($workContract->work_hours == 1)
        <p style="text-align: justify">3.5. Darbinieks veic darbu laika periodā no plkst. 9.00 līdz plkst. 18.00.</p>
        @else
            <p style="text-align: justify">3.5. Darbinieks veic darbu laika periodā no plkst. 9.00 līdz plkst. 18.00 ar vienas stundas pusdienu pārtraukumu laika periodā no plkst. 12.00 līdz plkst. 15.00.</p>
        @endif
        <p style="text-align: justify">3.6. Darba devējs ir tiesīgs noteikt citu darba un atpūtas laika režīmu, ja
            šāda vajadzība pamatota ar saimniecisku, organizatorisku, tehnoloģisku vai
            līdzīga rakstura pasākumu veikšanu Darba devēja uzņēmumā.</p>
        <p style="text-align: justify">3.7. Darbiniekam ir tiesības izmantot atvaļinājumus Darba likumā noteiktajā
            kārtībā.</p>

{{--        <br pagebreak="true"/>--}}
        <p align="center"><strong>4. DARBINIEKA VISPĀRĪGIE PIENĀKUMI UN TIESĪBAS</strong></p>

        <p style="text-align: justify">4.1. Darbinieka pienākumi tiek noteikti atbilstoši amata aprakstam (<strong><em>1. pielikums</em></strong>).</p>
        <p style="text-align: justify">4.2. Darbiniekam ir jādarbojas ar lielāko rūpību, lai pēc iespējas novērstu
            un mazinātu apstākļus, kas nelabvēlīgi ietekmē Darba devēja darbību, kā arī
            lai pēc iespējas novērstu un mazinātu radušos vai draudošus zaudējumus
            Darba devējam. Darbinieks apņemas sniegt Darba devējam patiesu, pilnīgu un
            savlaicīgu informāciju par jebkādiem apstākļiem vai riskiem, kas negatīvi
            ietekmē vai var negatīvi ietekmēt Darbinieka pienākumu pienācīgu izpildi.</p>
        <p style="text-align: justify">4.3. Darbiniekam jāievēro spēkā esošie normatīvie akti (tajā skaitā darba
            aizsardzības un ugunsdrošības noteikumi), Darba devēja apstiprinātie darba
            kārtības noteikumi un citi Darba devēja rīkojumi un lēmumi.</p>
        <p style="text-align: justify">4.4. Darbiniekam aizliegts veikt darbības, kas var kaitēt Darba devēja
            reputācijai un interesēm.</p>
        <p style="text-align: justify">4.5. Īslaicīgā prombūtnē esoša cita darbinieka pienākumu izpildīšana netiks
            uzskatīta par šim līgumam neatbilstošu darbu, ja vien tā raksturs nav
            acīmredzami neatbilstošs Darbinieka amatam un kvalifikācijai.</p>
        <p style="text-align: justify">4.6. Ja Darbinieks nevar ierasties darbā slimības vai jebkādu citu iemeslu
            dēļ, Darbiniekam nekavējoties par to jāpaziņo Darba devējam. Kompensācijas
            slimības laikā tiek maksātas tādā apmērā un kārtībā, kā nosaka Latvijas
            Republikā spēkā esošo normatīvo aktu obligātās prasības.</p>
        <p style="text-align: justify">4.7. Darbinieks atbild Darba devējam par zaudējumiem, kas Darba devējam
            radušies Darbinieka saistību nepienācīgas izpildes dēļ vai Darbinieka
            prettiesiskas rīcības dēļ. Darbinieks piekrīt, ka Darba devējam ir tiesības
            no Darbiniekam izmaksājamām summām likumos noteiktajā kārtībā ieturēt
            atlīdzību par minētajiem zaudējumiem. Darbinieks nav atbildīgs par
            zaudējumiem, kas radušies nepārvaramas varas rezultātā.</p>
        <p style="text-align: justify">4.8. Līgumā vai Darba devēja rīkojumos paredzētais Darbinieka pienākumu
            apraksts neatbrīvo Darbinieku no pienākuma pēc Darba devēja īpaša rīkojuma
            izpildīt blakus un papildus uzdevumus, kas nav acīmredzami un krasi
            neraksturīgi Līgumā noteiktajam Darbinieka amatam un pienākumiem.</p>
        <p style="text-align: justify">4.9. Darbinieks apņemas pienācīgi veikt savus darba pienākumus Darba devēja
            interesēs, Darba devēja noteiktajā darba vietā un darba laikā, atbilstoši
            konkrētā darba izpildes kārtībai un veidam saskaņā ar šo Līgumu, Latvijas
            Republikas normatīvajiem aktiem, kā arī Darbinieka amata aprakstu un citiem
            Darbiniekam saistošiem Darba devēja izdotiem iekšējiem tiesību aktiem, t.i.
            noteikumiem, instrukcijām, procedūrām, norādījumiem, lēmumiem, rīkojumiem
            un citiem Darba devēja izdotiem iekšējiem dokumentiem, turpmāk tekstā –
            Iekšējie tiesību akti. Darba devēja Iekšējie tiesību akti un to grozījumi
            tiek izvietoti, uzglabāti un uzturēti Darba devēja lietvedībā un ir
            pieejami Darbiniekam Darba devēja grāmatvedībā.</p>
        <p style="text-align: justify">4.10. Darbiniekam ir pienākums patstāvīgi iepazīties un visā Līguma
            darbības laikā ievērot Darba devēja Iekšējos tiesību aktus, pildot darba
            pienākumus, tajā skaitā attiecībās ar klientiem un citiem sadarbības
            partneriem. Darbiniekam minētie Darba devēja Iekšējie tiesību akti ir
            saistoši un Darbinieks, parakstot šo Līgumu, apliecina, ka ir ar tiem
            iepazinies. Par Iekšējo tiesību aktu veiktajām izmaiņām, tajā skaitā
            jaunajiem izdotiem Iekšējiem tiesību aktiem iepazīstina Darba devējs.
            Darbiniekam šādi Iekšējie tiesību akti kļūst saistoši pēc tam, kad Darba
            devējs ir informējis un iepazīstinājis Darbinieku.</p>
        <p style="text-align: justify">4.11. Darbinieks apņemas patstāvīgi paaugstināt savas profesionālās
            iemaņas, lai kvalitatīvi varētu veikt savus darba pienākumus.</p>
        <p style="text-align: justify">4.12. Darbinieks apņemas nekavējoties rakstveidā informēt Darba devēju,
            norādot detalizēti attiecīgos iemeslus un pamatojumu, kā arī neveikt
            darbības, kas saistītas ar attiecīgo darba uzdevumu, ja Darbiniekam nav
            pietiekamu profesionālo iemaņu konkrētu darba uzdevumu veikšanai nolīgtā
            darba ietvaros.</p>
        <p style="text-align: justify">4.13. Darbinieks apņemas nekavējoties informēt Darba devēju par Darbiniekam
            zināmiem trešo personu nodomiem vai darbībām, kuras varētu materiāli vai
            kādā citā veidā kaitēt Darba devēja vai tā klientu interesēm.</p>
        <p style="text-align: justify">4.14. Darbinieks apņemas nekavējoties, bet ne vēlāk kā nākamajā darba dienā
            informēt Darba devēju par pārejas darba nespēju vai citiem iemesliem, ja
            Darbinieks nevar ierasties darbā Līgumā noteikto pienākumu izpildei.
            Uzrādīt nepieciešamos, Latvijas Republikas normatīvajos aktos noteiktos,
            pierādījumus (darba nespējas lapu), par to kādēļ Darbinieks nav spējis
            veikt savus darba pienākumus. Pēc Darba devēja pieprasījuma, pārejas darba
            nespējas gadījumā, nekavējoties paziņot Darba devējam ārsta izrakstītās
            darba nespējas lapas reģistrācijas numuru.</p>
        <p style="text-align: justify">4.15. Darbinieks apņemas izmantot normatīvajos aktos noteiktajā kārtībā
            aprēķināto ikgadējo atvaļinājumu, atbilstoši Darba devēja apstiprinātajam
            ikgadējam darbinieku atvaļinājumu grafikam vai saskaņā ar Darbinieka un
            Darba devēja vienošanos.</p>
        <p style="text-align: justify">4.16. Darbinieks apņemas piedalīties Darba devēja organizētajos
            profesionālās apmācības semināros, konferencēs un programmās.</p>
        <p style="text-align: justify">4.17. Darbiniekam ir tiesības:</p>
        <p style="text-align: justify">4.17.1. Iesniegt sūdzību par viņa aizskartajām interesēm Darba devējam vai
            attiecīgi pilnvarotai personai.</p>
        <p style="text-align: justify">4.17.2. Saņemt atalgojumu par atbilstošā kvalitātē paveiktu darbu, saskaņā
            ar šī Līguma nosacījumiem.</p>
        <p style="text-align: justify">4.17.3. Saņemt sociālās garantijas apmēros, kā nosaka šis Līgums un spēkā
            esošie Latvijas Republikas normatīvie akti.</p>

        <p></p>

        <p align="center"><strong>5. DARBA DEVĒJA VISPĀRĪGIE PIENĀKUMI UN TIESĪBAS</strong></p>

        <p style="text-align: justify">5.1. Darba devējam ir šādi pienākumi:</p>
        <p style="text-align: justify">5.1.1. Par kvalitatīvi un laikā izpildītu darbu Darba devējs apņemas maksāt
            Darbiniekam darba algu šajā Līgumā noteiktajā apmērā un kārtībā un saskaņā
            ar darba kārtības noteikumiem.</p>
        <p style="text-align: justify">5.1.2. Pirms darba uzsākšanas, iepazīstināt Darbinieku ar veicamo darbu un
            tā apstākļiem.</p>
        <p style="text-align: justify">5.1.3. Pirms Darbinieks uzsāk pildīt darba pienākumus, iepazīstināt
            Darbinieku, ar iekšējās kārtības noteikumiem, darba un ugunsdrošības
            noteikumiem un citām Darba devēja izstrādātām instrukcijām, kas
            nepieciešamas tiešo pienākumu veikšanai.</p>
        <p style="text-align: justify">5.1.4. Nodrošināt darba veikšanai nepieciešamos apstākļus un tehniskos
            līdzekļus.</p>
        <p style="text-align: justify">5.1.5. Darba devējs piešķir Darbiniekam ikgadējo atvaļinājumu, atbilstoši
            Darba likumam un darba kārtības noteikumiem.</p>
        <p style="text-align: justify">5.2. Darba devēja tiesības:</p>
        <p style="text-align: justify">5.2.1. Darba devēja vai viņa pilnvarotās personas tiesības ir jebkurā brīdi
            atsaukt Darbinieku no ieņemamā amata, ja Darbinieks nepilda savus
            pienākumus vai saistības, vai pārkāpj šajā Līgumā noteiktās pilnvaras.</p>
        <p style="text-align: justify">5.2.2. Darba devējam ir tiesības norīkot Darbinieku Līgumā neparedzēta
            darba veikšanai, lai novērstu nepārvaramas varas, nejauša notikuma vai citu
            ārkārtēju apstākļu izraisītas sekas, kuras nelabvēlīgi ietekmē vai var
            ietekmēt parasto darba gaitu uzņēmumā.</p>
        <p style="text-align: justify">5.3. Darba devējam ir tiesības patstāvīgi izdot jaunus vai grozīt esošos
            Iekšējos tiesību aktus.</p>
        <p style="text-align: justify">5.4. Pārbaudīt Darbiniekam uzdoto pienākumu izpildi.</p>
        <p style="text-align: justify">5.5. Noteikt piemaksas saskaņā ar Latvijas Republikas tiesību aktiem un
            Darba devēja iekšējiem tiesību aktiem.</p>
        <p style="text-align: justify">5.6. Nodarbināt Darbinieku papildus vai virsstundu darbā saskaņā ar
            Latvijas Republikas tiesību aktiem, vienojoties ar Darbinieku par minētā
            darba samaksu katrā konkrētā gadījumā atsevišķi.</p>
        <p style="text-align: justify">5.7. Pārcelt iepriekšējā gadā Darbinieka neizlietoto ikgadējo atvaļinājumu,
            ja Darbinieks no Darba devēja neatkarīgu iemeslu dēļ nav izmantojis to vai
            tā daļu, izņemot Darba likumā noteiktajos gadījumos, kad tas noteikts
            citādi.</p>

        <p></p>

        <p align="center">
            <strong>6. DARBA SAMAKSA</strong>
        </p>
        @if($workContract->salary_kind == 1)
            <p style="text-align: justify">6.1. Darba devējs ik mēnesi maksās Darbiniekam darba algu <em>{{ number_format($workContract->salary, 2, ',', '') }} EUR ({{ $workContract->salary_written }}) apmērā</em>. Minētā darba alga ir <em>@if($workContract->salary_type == 1) bruto @else neto @endif</em> <em>alga</em>.</p>
        @elseif($workContract->salary_kind == 2)
            <p style="text-align: justify">6.1. Darbinieka darba alga tiek noteikta pēc stundu likmes  <em>{{ number_format($workContract->salary, 2, ',', '') }} EUR/stundā ({{ $workContract->salary_written }})</em>. Minētā darba alga ir <em>@if($workContract->salary_type == 1) bruto @else neto @endif</em> <em>alga</em>.</p>
        @endif
        <p style="text-align: justify">6.2. Darba samaksa tiks samaksāta vienu reizi mēnesī ne vēlāk kā līdz
            mēneša 15. datumam par iepriekšējo nostrādāto mēnesi.</p>
        <p style="text-align: justify">6.3. Darba samaksa tiek izmaksāta ar pārskaitījumu uz Darbinieka bankas
            kontu Nr. <em>{{ $workContract->user->bank_account }}</em>, un Darbinieks piekrīt šādai izmaksu
            kārtībai.</p>
        <p style="text-align: justify">6.4. Darba devējs kompensē Darbiniekam tikai tos izdevumus, kas veikti
            Darbinieka darba pienākumu ietvaros un iepriekš rakstveidā saskaņoti ar
            Darba devēju.</p>

        <p></p>

        <p align="center">
            <strong>7. ATVAĻINĀJUMA ILGUMS</strong>
        </p>
        <p style="text-align: justify">7.1. Darbiniekam tiek piešķirts ikgadējais apmaksātais atvaļinājums 4
            (četras) kalendāra nedēļas.</p>

        <p></p>

        <p align="center">
            <strong>8. PUŠU ATBILDĪBA</strong>
        </p>

        <p></p>

        <p style="text-align: justify">8.1. Par Darba devēja Iekšējo tiesību aktu pārkāpšanu Darba devējs
            Darbiniekam var izteikt rakstveida piezīmi vai rājienu, minot tos
            apstākļus, kas norāda uz pārkāpuma izdarīšanu, kā arī uzteikt Līgumu.</p>
        <p style="text-align: justify">8.2. Darbinieks ir personiski atbildīgs par savu šajā Līgumā noteikto
            pienākumu savlaicīgu un kvalitatīvu izpildi.</p>
        <p style="text-align: justify">8.3. Darbinieks saskaņā ar šo Līgumu ir materiāli un citādā veidā atbildīgs
            par viņam nodotām Darba devēja materiālajām, naudas un citām vērtībām,
            nepatiesas informācijas sniegšanu Darba devējam, kuras rezultātā Darba
            devējam radušies zaudējumi, kā arī par citu Līgumā uzņemto saistību
            neievērošanu.</p>
        <p style="text-align: justify">8.4. Darbinieks ir materiāli un citādā veidā atbildīgs par šā Līguma
            noteikumu, Iekšējo tiesību aktu, darba aizsardzības normu, darba drošības
            un ugunsdrošības noteikumu, sanitāro normu, kā arī citu Latvijas Republikas
            normatīvo aktu ievērošanu.</p>
        <p style="text-align: justify">8.5. Darbinieks apņemas atlīdzināt Darba devējam materiālos un citus
            zaudējumus, kas radušies šā Līguma neievērošanas dēļ vai arī citas
            Darbinieka darbības, vai bezdarbības dēļ, sedzot Darba devējam zaudējumus
            Latvijas Republikas tiesību aktos paredzētajā kartībā.</p>
        <p style="text-align: justify">8.6. Darbinieks ir materiāli un citādā veidā atbildīgs par pareizu un
            saudzīgu Darba devēja īpašuma izmantošanu.</p>
        <p style="text-align: justify">8.7. Darba devējs un Darbinieks par nodarītajiem zaudējumiem vai kaitējumu
            ir atbildīgi saskaņā ar spēkā esošo LR likumdošanu.</p>
        <p style="text-align: justify">8.8. Darbinieks atlīdzina ar savu darbību vai bezdarbību radītos
            zaudējumus, tajā skaitā, zaudējumus, kas rodas sakarā ar nepatiesas vai
            nepilnīgas informācijas sniegšanas Darba devējam, Darbiniekam noteiktā
            pilnvarojuma pārsniegšanu, Līgumā un Darba devēja Iekšējos tiesību aktos
            paredzēto noteikumu neievērošanu, komercnoslēpuma izpaušanu.</p>
        <p style="text-align: justify">8.9. Ja Darbinieks tīši vai netīši darījis zināmas atklātībai vai izpaudis
            trešajām personām, kurām nav tiesību saņemt attiecīgo informāciju, ziņas
            par Darba devēja klientiem, sadarbības partneriem vai darījumiem, kurus
            Darba devējs ir veicis, ja viņam šīs ziņas uzticētas vai kļuvušas zināmas
            kā Darba devēja darbiniekam, tad Darbinieks Latvijas Republikas tiesību
            aktos noteiktajos gadījumos saucams pie kriminālatbildības, kā arī Darba
            devējam ir tiesības prasīt nodarīto zaudējumu atlīdzināšanu.</p>
        <p style="text-align: justify">8.10. Darba devējs atlīdzina Darbiniekam zaudējumus, kas radušies
            Darbiniekam Darba devēja vainas dēļ.</p>

        <p></p>

        <p align="center">
            <strong>9. KOMERCNOSLĒPUMA NEIZPAUŠANAS PIENĀKUMS</strong>
        </p>
        <p style="text-align: justify">9.1. Darbinieks apņemas neizpaust Darba devēja, kā arī tā klientu
            komercnoslēpumus un citus noslēpumus, t.sk. zinātību (know-how), izņemot
            gadījumus, kad informācija tiek izpausta ar Darba devēja noteiktu
            piekrišanu vai Darba devēja uzdevumā. Par komercnoslēpumu jebkurā gadījumā
            ir uzskatāma informācija par darba samaksu, klientiem un to skaitu, dizaina
            vai cita veida izstrādes materiāliem, t.sk. foto, video vai audio darbiem,
            Darba devēja darījumiem, ienākumiem un izdevumiem, organizāciju, darba
            apstākļiem, izmantojamām tehnoloģijām, darba paņēmieniem, nākotnes plāniem
            un prognozēm, riska faktoriem un grūtībām, tehnisko jaunradi un izpēti, kā
            arī cita informācija, kas atbilstoši normatīvajiem aktiem ir uzskatāma par
            neizpaužamu. Izņēmums šim noteikumam ir Latvijas likumos tieši noteikti
            gadījumi, kad personai ir nenovēršams pienākums sniegt minēto informāciju,
            taču arī šādā gadījumā informācija sniedzama tikai attiecīgajam mērķim
            nepieciešamajā apjomā un noteiktajā kārtībā. Šajā punktā noteiktais
            pienākums pilnībā saista Darbinieku 5 (piecus) gadus pēc darba attiecību
            pārtraukšanas vai izbeigšanās.</p>
        <p style="text-align: justify">9.2. Darbiniekam ir pienākums pastāvīgi rūpēties un spert visus saprātīgos
            soļus konfidenciālas informācijas aizsardzībai, t.sk. rūpēties, lai
            konfidenciālo informāciju neizpaustu viņam pakļautie darbinieki.</p>
        <p style="text-align: justify">9.3. Darbinieks apņemas nekādā veidā neizmantot konfidenciālo informāciju
            savās personīgajās interesēs vai trešās personas interesēs, īpaši attiecībā
            uz darba devēja konkurentiem.</p>

        <p></p>

        <p align="center">
            <strong>10. BLAKUS DARBS UN KONKURENCES IEROBEŽOJUMS</strong>
        </p>
        <p style="text-align: justify">10.1. Darba tiesisko attiecību spēkā esamības laikā Darbiniekam bez Darba
            devēja noteiktas rakstiskas atļaujas<em> </em>nav<em> </em> tiesības slēgt
            darba līgumus vai uzņēmuma līgumus ar citiem darba devējiem, kuri ražo un
            sniedz tāda paša vai līdzvērtīga sortimenta preces un pakalpojumus kā Darba
            devējs.</p>
        <p style="text-align: justify">10.2. Darbinieks apliecina, ka šī Līguma noslēgšanas brīdī tas nav
            noslēdzis darba līgumus vai uzņēmuma līgumus ar citiem darba devējiem, kuri
            ražo un sniedz tāda paša vai līdzvērtīga sortimenta preces un pakalpojumus
            kā Darba devējs, izņemot tādu, par kuru rakstiski paziņojis Darba devējam
            pirms šī līguma noslēgšanas.</p>

        <p></p>

        <p align="center">
            <strong>11. PERSONAS DATU APSTRĀDE UN INTELEKTUĀLAIS ĪPAŠUMS</strong>
        </p>
        <p style="text-align: justify">11.1. Darbinieks piekrīt, ka Darba devējam ir tiesības savas darbības
            nodrošināšanai veikt Darbinieka personas datu apstrādi normatīvajos aktos
            noteiktajā kārtībā, tajā skaitā darbinieka foto, video vai cita veida
            materiālus, kuros redzams darbinieks. Darba devējs personas datu apstrādi
            veic saskaņā ar Darba devēja izstrādātajiem Personu datu apstrādes
            noteikumiem, kas ir pieejami Elektroniskā vietnē.</p>
        <p style="text-align: justify">11.2. Īpašumtiesības uz izstrādātajiem produktiem, izgudrojumiem, citiem
            rūpnieciskā īpašuma objektiem, preču zīmēm, atklājumiem, citu intelektuālo
            īpašumu, ko Darbinieks radījis, pildot darba pienākumus, pieder Darba
            devējam.</p>
        <p style="text-align: justify">11.3. Darbinieka darba pienākumu izpildes laikā radītais un Darba devēja
            apmaksātais Darbinieka darba produkts ir Darba devēja īpašums. Darba devējs
            iegūst autora mantiskās tiesības attiecībā uz darbinieka darba laikā radīto
            darbu, dizaina paraugu vai foto, video vai audio darbu, īpašuma tiesības uz
            Darbinieka darba laikā radīto izgudrojumu, preču zīmi vai dizainparaugu,
            tāpat Darbinieka kā autora un izgudrotāja mantisko tiesību regulējums un no
            tā izrietošās Darba devēja tiesības paliek spēkā arī pēc darba tiesisko
            attiecību beigām.</p>

        <p align="center"></p>

        <p align="center">
            <strong>12. DARBA TIESISKO ATTIECĪBU IZBEIGŠANA</strong>
        </p>
        <p style="text-align: justify">12.1. Darba tiesiskās attiecības izbeidzamas tādos gadījumos un ievērojot
            tādu kārtību un termiņus, kas noteikti normatīvajos aktos.</p>
        <p style="text-align: justify">12.2. Izbeidzoties darba tiesiskajām attiecībām, neatkarīgi no izbeigšanās
            iemesla Darbinieks nekavējoties atdod Darba devējam jebkuru no Darba devēja
            saņemto mantu (izņemot mantu, ko Darba devējs noteikti nodevis Darbinieka
            īpašumā kā dāvanu, darba samaksu vai prēmiju) un jebkurus informācijas
            nesējus, kas satur informāciju par Darba devēju un tā darbību.</p>

        <p></p>

        <p align="center">
            <strong>13. CITI NOTEIKUMI</strong>
        </p>
        <p style="text-align: justify">13.1. Jebkādiem paziņojumiem un citai korespondencei, kas adresēta otrai
            Pusei šī Līguma sakarā, ir jābūt:</p>
        <p style="text-align: justify">13.1.1. nodotai personīgi, un tā uzskatāma par saņemtu, kad adresāts
            parakstījies par korespondences saņemšanu, vai</p>
        <p style="text-align: justify">13.1.2. nosūtītai ar ierakstītu vēstuli, un tā uzskatāma par saņemtu 7.
            (septītajā) dienā pēc tam, kad tā nosūtīta adresātam uz šī Līguma ievaddaļā
            norādīto Puses adresi vai citu adresi, ko viena Puse šajā punktā noteiktā
            kārtībā ir paziņojusi otrai Pusei, vai ātrāk, ja ir saņemts attiecīgs pasta
            iestādes paziņojums par korespondences izsniegšanu adresātam,</p>
        <p style="text-align: justify">13.2. Līgums stājas spēkā tā parakstīšanas brīdī. Jebkuri Līguma noteikumu
            grozījumi stājas spēkā tikai pēc tam, kad to ir parakstījušas abas Puses,
            un tie kļūst par Līguma neatņemamu sastāvdaļu.</p>
        <p style="text-align: justify">13.3. Šis Līgums ir sastādīts un darbojas saskaņā ar Latvijas Republikā
            spēkā esošiem tiesību aktiem. Jebkurš strīds, domstarpība vai prasība, kas
            izriet no Līguma, kas skar to vai tā pārkāpšanu, izbeigšanu, spēkā esamību,
            vispirms tiek risināts sarunu ceļā, bet, ja vienošanās nav iespējama,
            strīds tiek izšķirts Latvijas Republikas tiesās.</p>
        <p style="text-align: justify">13.4. Līgums ir sastādīts 2 (divos) eksemplāros, no kuriem viens tiek
            nodots Darba devējam un viens Darbiniekam. Abiem eksemplāriem ir vienāds
            juridiskais spēks.</p>

{{--        <br pagebreak="true"/>--}}

        <p align="center">
            <strong>14. PUŠU PARAKSTI</strong>
        </p>
        <table border="0" cellspacing="0" cellpadding="0" width="567" align="center">
            <tbody align="center">
            <tr align="center">
                <td width="220" align="center">
                    <p align="center">
                        <strong>Darba devējs:</strong>
                    </p>
                    <p align="center">
                        ___________________
                    </p>
                    <p align="center">
                        <em>{{ $workContract->company->director()->name }}</em>
                    </p>
                </td>
                <td width="220" align="center">
                    <p align="center">
                        <strong>Darbinieks:</strong>
                    </p>
                    <p align="center">
                        ___________________
                    </p>
                    <p align="center">
                        <em>{{ $workContract->user->name }}</em>
                    </p>
                </td>
            </tr>
            </tbody>
        </table>

    </div>
</body>
</html>
