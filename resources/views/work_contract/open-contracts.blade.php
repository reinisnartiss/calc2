@if($openContracts->first())
    <div class="text-center p-6">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Sagatavotie darba līgumi') }}
        </h2>
    </div>
    <div class="p-2 md:p-6 bg-white border border-gray-200 rounded-none md:rounded:lg w-full">
        @foreach($openContracts as $contract)
            <div
                class="block md:grid grid-cols-4 text-center overflow-hidden border border-gray-200 rounded-lg mb-2 shadow">
                <div class="overflow-hidden my-2 md:my-4 col-span-1">
                    <span class="block text-sm text-gray-800">Vārds Uzvārds</span>
                    {{ $contract->user->name }}
                </div>
                <div class="overflow-hidden my-2 md:my-4 col-span-1">
                    <span class="block text-sm text-gray-800">Darba līguma numurs</span>
                    {{ $contract->contract_number }}
                </div>
                <div class="overflow-hidden my-2 md:my-4 col-span-1">
                    <span class="block text-sm text-gray-800">Uzņēmums</span>
                    {{ $contract->company->name }}
                </div>
                <div class="overflow-hidden my-2 md:my-4 col-span-1">
                    <a href="/work-contract/{{ $contract->id }}">
                        <button type="button"
                                class="md:my-2 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                            Skatīt
                        </button>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@else
    <div class="p-6 bg-white border border-gray-200 rounded-lg w-full">
        <div class="block text-center overflow-hidden border border-gray-200 rounded-lg mb-2 shadow p-2">
            Nav sagatavots neviens darba līgums
        </div>
    </div>
@endif
