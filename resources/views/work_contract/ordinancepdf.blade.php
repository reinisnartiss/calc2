<p align="center"><strong>{{ $workContract->company->name }}</strong></p>
<p align="center">
    Reģ. Nr. {{ $workContract->company->registration_number }}
    <br>{{ $workContract->company->address }}
</p>

<hr>
<p></p>
<p align="center">
    <strong>R Ī K O J U M S</strong>
</p>
<table width="100%">
    <tbody>
    <tr>
        @isset($same_as_contract)
            <td valign="top" align="left">
                <p>{{ $workContract->date->format('d.m.Y.') }}</p>
            </td>
        @else
            <td valign="top" align="left">
                @isset($order_date)
                    <p>{{ \Illuminate\Support\Carbon::parse($order_date)->format('d.m.Y.') }}</p>
                @else
                    <p>{{ $workContract->order_date->format('d.m.Y.') }}</p>
                @endisset
            </td>
        @endisset
        <td valign="top" align="right">
            @isset($order_number)
                <p align="right">
                    Nr. {{ $order_number }}
                </p>
            @else
                <p align="right">
                    Nr. {{ $workContract->order_number }}
                </p>
            @endisset
        </td>
    </tr>
    </tbody>
</table>
<p>
    <em> </em>
</p>
<p><em>Par pieņemšanu darbā</em></p>
<p></p>
<p align="justify" style="text-indent: 40px; text-align: justify">
    @if( $workContract->salary_kind == 1 )
        Ar {{ $workContract->contract_start->format('d.m.Y.') }} pieņemt darbā <strong>{{ $workContract->user->name_akuzativs }}</strong>, personas kods
        {{ !empty($workContract->user->personal_id) ? \Illuminate\Support\Facades\Crypt::decryptString($workContract->user->personal_id) : '' }}, par <strong>{{ $workContract->position_akuzativs }}</strong> (amata nosaukums atbilstoši profesiju
        klasifikatoram: {{ $workContract->position_id }} {{ $workContract->position_nominativs }}), nosakot darba algas veidu -
        "mēneša", ar likmi {{ number_format($workContract->salary, 2, ',', '') }} EUR. Minētā darba alga ir {{ $workContract->salary_type == 1 ? 'bruto' : 'neto' }} alga.
    @else
        Ar {{ $workContract->contract_start->format('d.m.Y.') }} pieņemt darbā <strong>{{ $workContract->user->name_akuzativs }}</strong>, personas kods
        {{ !empty($workContract->user->personal_id) ? \Illuminate\Support\Facades\Crypt::decryptString($workContract->user->personal_id) : ''}}, par <strong>{{ $workContract->position_akuzativs }}</strong> (amata nosaukums atbilstoši profesiju
        klasifikatoram: {{ $workContract->position_id }} {{ $workContract->position_nominativs }}), nosakot darba algas veidu -
        "laika darba alga", ar likmi {{ number_format($workContract->salary, 2, ',', '') }} EUR/stundā. Minētā darba alga ir {{ $workContract->salary_type == 1 ? 'bruto' : 'neto' }} alga.
    @endif
</p>
<br>
<br>
<table width="100%">
    <tbody>
    <tr>
        <td valign="top" align="left">
            <p>
                {{ \Illuminate\Support\Str::ucfirst($workContract->company->director_role_nominativs) }}
            </p>
        </td>
        <td valign="top" align="right">
            <p>
                {{ $workContract->company->director()->name }}
            </p>
        </td>
    </tr>
    </tbody>
</table>
<br>
<br>
<table width="100%">
    <tbody>
    <tr>
        <td valign="top" align="left">
            <p>
                Ar rīkojumu iepazinos
            </p>
        </td>
        <td valign="top" align="right">
            <p>
                {{ $workContract->user->name }}
            </p>
        </td>
    </tr>
    </tbody>
</table>

