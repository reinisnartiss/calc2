<x-app-layout>

    <x-slot name="slot">

        <div class="flex justify-center flex-wrap mt-6 mb-24 rounded-lg">

            <div class="container p-0 md:p-6 w-full block">

                @if (session('status'))
                    <div
                        class="mx-auto bg-white bg-opacity-75 sm:w-3/4 lg:w-1/3 p-2 mt-6 text-center border border-green-200 shadow rounded-lg">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="flex justify-center mt-6">
                    <div class="w-full">
                        <div class="text-center p-6">
                            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                {{ $contract->user->name }}
                            </h2>
                        </div>
                        <div>
                            <div
                                class="items-center p-6 bg-white border border-gray-200 shadow rounded-none md:rounded-lg w-full relative">
                                <div class="mb-4">
                                    <a href="/users/{{ $contract->user->id }}">
                                        <button type="button" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                            Atpakaļ
                                        </button>
                                    </a>
                                </div>
                                @role('grāmatvedis')
                                    <a href="/work-contract/{{ $contract->id }}/edit"
                                       class="absolute top-3 right-3 m-2 text-gray-500 text-opacity-50 hover:text-opacity-100 hover:text-gray-700"><i
                                            class="fas fa-pen"></i> Rediģēt</a>
                                @endrole
                                <div class="block md:flex mb-2">
                                    <span class="text-sm text-gray-800 ">Darbinieka vārds, uzvārds: </span>
                                    <p class="ml-4">{{ $contract->user->name }}</p>
                                </div>
                                <div class="block md:flex mb-2">
                                    <span class="text-sm text-gray-800 ">Darba līguma numurs: </span>
                                    <p class="ml-4">{{ $contract->contract_number }}</p>
                                </div>
                                <div class="block md:flex mb-2">
                                    <span class="text-sm text-gray-800 ">Līguma sastādīšanas datums: </span>
                                    <p class="ml-4">{{ $contract->date ? $contract->date->format('d.m.Y') : '' }}</p>
                                </div>
                                <div class="block md:flex mb-2">
                                    <span class="text-sm text-gray-800 ">Uzņēmums: </span>
                                    <p class="ml-4">{{ $contract->company->name }}</p>
                                </div>
                                <div class="block md:flex mb-2">
                                    <span class="text-sm text-gray-800 ">Amats: </span>
                                    <p class="ml-4">{{ $contract->position_nominativs }}</p>
                                </div>
                                <div class="block md:flex mb-2">
                                    <span class="text-sm text-gray-800 ">Amata klasifikators: </span>
                                    <p class="ml-4">{{ $contract->position_id }}</p>
                                </div>
                                <div class="block md:flex mb-2">
                                    <span class="text-sm text-gray-800 ">Darba uzsākšanas datums: </span>
                                    <p class="ml-4">{{ $contract->contract_start ? $contract->contract_start->format('d.m.Y') : '' }}</p>
                                </div>
                                <div class="block md:flex mb-2">
                                    <span class="text-sm text-gray-800 ">Uz: </span>
                                    @if($contract->contract_period == 1)
                                        <p class="ml-4">Nenoteiktu laiku</p>
                                    @else
                                        <p class="ml-4">Noteiktu laiku
                                            līdz {{ $contract->contract_end_date ? $contract->contract_end_date->format('d.m.Y') : '' }}</p>
                                    @endif
                                </div>
                                <div class="block md:flex mb-2">
                                    <span class="text-sm text-gray-800 ">Darba laiks: </span>
                                    @if($contract->work_hours_type == 1)
                                        <p class="ml-4">Nepilnais darba laiks {{ $contract->weekly_hours }} stundas
                                            nedēļā</p>
                                    @elseif($contract->work_hours_type == 2)
                                        <p class="ml-4">Normālais darba laiks {{ $contract->weekly_hours }} stundas
                                            nedēļā</p>
                                    @else
                                        <p class="ml-4">Summētais darba laiks {{ $contract->weekly_hours }} stundas
                                            nedēļā</p>
                                    @endif
                                </div>
                                <div class="block md:flex mb-2">
                                    <span class="text-sm text-gray-800 ">Darba stundas: </span>
                                    @if($contract->work_hours == 1)
                                        <p class="ml-4">No 9:00 līdz 18:00</p>
                                    @else
                                        <p class="ml-4">No 9:00 līdz 18:00 ar pusdienu pārtraukumu</p>
                                    @endif
                                </div>
                                <div class="block md:flex mb-2">
                                    <span class="text-sm text-gray-800 ">Alga: </span>
                                    @if($contract->salary_kind == 1)
                                        <p class="ml-4">Fiksēta alga: {{ $contract->salary }}
                                            EUR, @if($contract->salary_type == 1) BRUTO @else NETO @endif</p>
                                    @else
                                        <p class="ml-4">Stundas likme: {{ $contract->salary }} EUR/stundā </p>
                                    @endif
                                </div>
                                <div class="mt-4">
                                    <a href="/work-contract/{{ $contract->id }}/contract" target="_blank">
                                        <button type="button"
                                                class="my-4 md:my-2 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                            Līgums PDF
                                        </button>
                                    </a>
                                    <a href="/work-contract/{{ $contract->id }}/contract-word" target="_blank">
                                        <button type="button"
                                                class="my-4 md:my-2 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                            Līgums WORD
                                        </button>
                                    </a>
                                    @if($contract->is_printed_contract == true)
                                        <span class="text-sm text-gray-800 ml-2">Līgums jau ir izveidots</span>
                                    @endif
                                </div>
                                <div class="mt-4">
                                    <form action="/work-contract/{{ $contract->id }}/order" target="_blank">
                                        <div class="mb-4">
                                            @isset($contract->order_date)
                                                <div class="flex">
                                                    <span class="text-sm text-gray-800 ">Rīkojuma datums: </span>
                                                    <p class="ml-4">{{ $contract->order_date->format('d.m.Y') }}</p>
                                                </div>
                                            @else
                                                <div class="block md:flex">
                                                    <div class="mb-2 md:mb-0">
                                                        <label for="order_date" class="text-sm text-gray-800 mr-2">Rīkojuma
                                                            datums: </label>
                                                        <input type="date" name="order_date" id="order_date"
                                                               class="form-input">
                                                        @error('order_date') <span
                                                            class="text-sm text-red-500">{{ $message }}</span> @enderror
                                                    </div>
                                                    <div class="flex items-center">
                                                        <span class="text-sm text-gray-800 ml-0 mr-2 md:mx-4 ml-0 md:ml-4">Tāds pats kā līgumā</span>
                                                        <input type="checkbox" name="same_as_contract"
                                                               id="same_as_contract" class="form-checkbox p-3">
                                                        @error('same_as_contract') <span
                                                            class="text-sm text-red-500">{{ $message }}</span> @enderror
                                                    </div>
                                                </div>
                                            @endisset
                                        </div>
                                        <div class="mb-4">
                                            @isset($contract->order_number)
                                                <div class="flex">
                                                    <span class="text-sm text-gray-800 ">Rīkojuma numurs: </span>
                                                    <p class="ml-4">{{ $contract->order_number }}</p>
                                                </div>
                                            @else
                                                <label for="order_number" class="text-sm text-gray-800 mr-2">Rīkojuma
                                                    numurs: </label>
                                                <input type="text" name="order_number" id="order_number"
                                                       class="form-input" required>
                                                @error('order_number') <span class="text-sm text-red-500">{{ $message }}</span> @enderror
                                            @endisset
                                        </div>
                                        <div class="flex justify-between">
                                            <div>
                                                <button type="submit" name="action" value="pdf"
                                                        class="my-4 md:my-2 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                    Rīkojums PDF
                                                </button>
                                                <button type="submit" name="action" value="word"
                                                        class="my-4 md:my-2 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                    Rīkojums WORD
                                                </button>
                                                @if($contract->is_printed_ordinance == true)
                                                    <span class="text-sm text-gray-800 ml-2 inline-block">Rīkojums jau ir izveidots</span>
                                                @endif
                                            </div>
                                        </div>
                                    </form>
                                    <div class="flex justify-end">
                                        @role('grāmatvedis')
                                            <div class="mr-4">
                                                @livewire('resignation-date-form', ['contract' => $contract])
                                            </div>
                                        @endrole
                                        <div class="">
                                            <form action="/work-contract/{{ $contract->id }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" onclick="return confirm('Vai tiešām vēlaties dzēst šo lietotāja darba līgumu?')" class="inline-flex items-center px-4 py-2 bg-white border-2 border-red-600 rounded-md font-semibold text-xs text-red-800 hover:text-white uppercase tracking-widest hover:bg-red-600 active:bg-red-700 focus:outline-none focus:border-red-700 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                                    Izdzēst
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </x-slot>

</x-app-layout>
