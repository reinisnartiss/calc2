<x-app-layout>

    <x-slot name="slot">

        <div class="flex justify-center mt-6 mb-24 rounded-lg">

            <div class="container md:p-6 md:w-auto">
                <div class="p-6 text-center">
                    <div>
                        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                            {{ __('Darba laika atskaite') }}
                        </h2>
                    </div>
                </div>

                <div class="p-6 bg-white">
                    <div class="mb-4">
                        <a href="/work-report">
                            <button type="button" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-white border-2 border-gray-800 rounded-md font-semibold text-xs text-black hover:text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                Atpakaļ
                            </button>
                        </a>
                    </div>
                    <form action="/work-report" method="post">
                        @csrf
                        @forelse ($dates as $key => $date)
                            <div class="work-date show-hours border rounded-lg p-4 items-center shadow md:flex block mb-4 relative md:static {{ $date['work_date']->isWeekend() || $date['work_date']->isHoliday() ? 'text-red-600 border-red-300 weekend' : 'work-day' }}">
                                <div class="mr-6 mb-2 md:mb-0 flex md:block justify-center">
                                    <p class="text-sm text-gray-800 mr-4">{{ $date['work_date']->locale('lv')->dayName }}</p>
                                    {{ $date['work_date']->toDateString() }}
                                </div>

                                <div>
                                    <input type="hidden" name="workReports[{{ $key }}][work_date]" id="date" value="{{ $date['work_date']->toDateString() }}">
                                    @error('workReports.' . $key . '.work_date')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                                </div>

                                <div class="mb-2 ml-6 md:mb-0 flex md:block justify-left items-center">
                                    <label for="did_work" class="mr-2">Vai strādāji?</label>
                                    <select onchange="toggleHours(event)" class="didWork form-select" name="workReports[{{ $key }}][did_work]">
                                        <option value="1">Jā</option>
                                        <option value="0">Nē</option>
                                    </select>
                                    @error('didWork')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                                </div>

                                <div class="hours ml-6 flex md:block justify-center items-center mb-2 md:mb-0">
                                    <label for="work_hours" class="mr-2">Cik stundas?</label>
                                    <input type="number" onchange="toggleComment(event)" name="workReports[{{ $key }}][work_hours]" class="work_hours form-input" value="{{ $date['work_date']->isWeekend() || $date['work_date']->isHoliday() ? '0' : '8' }}">
                                    @error('workReports.' . $key . '.work_hours')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                                    <div class="text-center hidden lg:block">
                                        @error('workReports.' . $key . '.reason_id')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                                        @error('workReports.' . $key . '.comment')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                                    </div>
                                </div>

                                <div class="reason mx-6 flex md:block justify-center items-center mb-2 md:mb-0">
                                    <label for="reason" class="mr-2">Iemesls</label>
                                    <select name="workReports[{{ $key }}][reason_id]" class="reason_id form-select">
                                            <option class="display:none"></option>
                                        @foreach ($reasons as $reason)
                                            <option value="{{ $reason->id }}">{{ $reason->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="comment ml-6 flex md:block justify-center items-center mb-2 md:mb-0">
                                    <label for="comment" class="mr-2">Iemesls virsstundām</label>
                                    <input type="text" name="workReports[{{ $key }}][comment]" class="comment_text form-input">
                                </div>

                                <div class="ml-6 text-lg text-red-400 text-opacity-50 hover:text-opacity-100 hover:text-red-600 outline-none absolute md:static top-0 right-0 p-4 md:p-0">
                                    <button type="button" onclick="removeDiv(event)"><i class="fas fa-times"></i></button>
                                </div>
                                <div class="text-center block lg:hidden">
                                    @error('workReports.' . $key . '.reason_id')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                                    @error('workReports.' . $key . '.comment')<p class="text-red-600 text-sm">{{ $message }}</p>@enderror
                                </div>
                            </div>
                        @empty
                            <div class="work-date show-hours border rounded-lg p-4 items-center shadow md:flex block mb-4 relative md:static">
                                Nav nevienas neaizpildītas dienas!
                            </div>
                        @endforelse
                        <button type="submit" id="btn" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                            Iesniegt
                        </button>

                    </form>
                </div>
            </div>
        </div>

        <script defer>
            function toggleHours(event) {
                var row = event.target.closest(".work-date");
                row.classList.toggle("show-hours");

                var numberInput = row.querySelector('input.work_hours');
                var reasonInput = row.querySelector('select.reason_id');
                numberInput.value = null;
                reasonInput.selectedIndex = 0;
                row.classList.remove("show-comment");
            }

            function toggleComment(event) {
                var hours = event.target;
                var row = event.target.closest(".work-date");

                if (parseInt(hours.value) > 8 || row.classList.contains('weekend')) {
                    row.classList.add("show-comment");
                } else {
                    row.classList.remove("show-comment");
                }
            }

            function removeDiv(event) {
                event.target.closest(".work-date").remove();
            }
        </script>
    </x-slot>

</x-app-layout>
