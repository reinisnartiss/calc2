<x-app-layout>

    <x-slot name="slot">

        <div class="mx-auto mb-16 rounded-none md:rounded-lg">

            <div class="container sm:p-0 p-0 md:p-3 w-full block mx-auto">

                @if (session('status'))
                    <div class="flex justify-center rounded-lg my-8">
                        <div class="mx-auto bg-white bg-opacity-75 sm:w-3/4 lg:w-1/2 p-2 mt-6 text-center border border-green-200 shadow rounded-lg">
                            {{ session('status') }}
                        </div>
                    </div>
                @endif

                <div class="block lg:grid lg:grid-cols-2 lg:gap-4">
                    <div class="col-span-1">
                        <div class="p-6 text-center">
                            <div>
                                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                                    {{ __('Jauna darba laika atskaite') }}
                                </h2>
                            </div>
                        </div>
                        <div class="p-2 md:p-6 text-center bg-white rounded-none md:rounded-lg border border-gray-200" style="min-height: 11rem">
                            <form action="/work-report/create" method="get">
                                <div class="mb-4">
                                    <p>Ievadīt darba laika atskaiti par periodu</p>
                                </div>
                                <div class="block md:flex lg:flex justify-center items-start">
                                    <div class="inline-block">
                                        <div class="mb-4 block md:flex">
                                            <div class="text-center lg:mr-2">
                                                <label for="startDate" class="mr-1">No:</label>
                                                <input class="p-1 border border-gray-200 rounded-lg form-input" required type="date" name="startDate" id="start-date" value="{{ \Carbon\Carbon::now()->firstOfMonth()->toDateString() }}">
                                                @error('startDate')<p class="text-sm text-red-600">{{ $message }}</p>@enderror
                                            </div>
                                            <div class="text-center md:ml-2 mt-2 md:mt-0">
                                                <label for="endDate" class="mr-1">Līdz:</label>
                                                <input class="p-1 border border-gray-200 rounded-lg form-input" required type="date" name="endDate" id="end-date" value="{{ \Carbon\Carbon::now()->toDateString() }}">
                                                @error('endDate')<p class="text-sm text-red-600">{{ $message }}</p>@enderror
                                            </div>
                                        </div>
                                        <div class="flex items-center justify-center">
                                            <input type="checkbox" name="weekends" id="weekends" class="form-checkbox mx-2">
                                            <label for="weekends">Iekļaut brīvdienas</label>
                                        </div>
                                    </div>
                                    <div class="block md:inline-block lg:inline:block ml-0 md:ml-6">
                                        <button type="submit" class="mt-4 lg:my-0 md:my-0 inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:shadow-outline-gray disabled:opacity-25 transition ease-in-out duration-150">
                                            Tālāk
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-span-1">

                        @livewire('project-hours-report')

                    </div>
                </div>
            </div>
            <div class="container sm:p-0 p-0 md:p-3 w-full block mx-auto">
                <div class="p-6 text-center">
                    <div>
                        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                            {{ __('Mana darba laika atskaite') }}
                        </h2>
                    </div>
                </div>

                @livewire('work-report-index')

            </div>
        </div>

    </x-slot>

</x-app-layout>
