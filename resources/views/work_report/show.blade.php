<x-app-layout>

    <x-slot name="slot">

        <div class="flex justify-center flex-wrap mt-6 mb-24 rounded-lg">
            
            <div class="container sm:p-0 p-6 w-full block">

                @if (session('status'))
                    <div class="flex justify-center rounded-lg my-8">
                        <div class="bg-white bg-opacity-75 sm:w-3/4 lg:w-1/2 p-4 text-center border border-green-400">
                            {{ session('status') }}
                        </div>
                    </div>
                @endif

                @livewire('work-report-update', ['workReport' => $workReport, 'reasons' => $reasons])

            </div>
            <div class="container sm:p-0 p-6 w-full block">
                
            </div>
        </div>

    </x-slot>

</x-app-layout>