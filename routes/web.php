<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check()) {
        return view('work_report.index');
    }
    return view('auth.login');
});

Route::group(['middleware' => ['auth', 'check_blocked']], function () {
    Route::get('/work-report', 'WorkReportController@index')->name('work-report.index');
    Route::get('/work-report/create', 'WorkReportController@create')->name('work-report.create');
    Route::post('/work-report', 'WorkReportController@store')->name('work-report.store');
    Route::get('/work-report/{workReport}', 'WorkReportController@show')->name('work-report.show');
    Route::put('/work-report/{workReport}', 'WorkReportController@update')->name('work-report.update');
    Route::delete('/work-report/{workReport}', 'WorkReportController@destroy')->name('work-report.destroy');
    // ----------------
    Route::get('/vacations', 'VacationController@index')->name('vacations.index');
    Route::post('/vacations', 'VacationController@store')->name('vacations.store');
    Route::get('/vacations/{vacation}', 'VacationController@show')->name('vacations.show');
    Route::get('/vacations/{vacation}/edit', 'VacationController@edit')->name('vacations.edit');
    Route::patch('/vacations/{vacation}/update-dates', 'VacationController@updateDates')->name('vacations.update_dates');
    Route::post('/vacations/{vacation}/store-comment', 'VacationController@updateComment')->name('vacations.update_comment');
    Route::delete('/vacations/{vacation}', 'VacationController@destroy')->name('vacations.destroy');
    // ----------------
    Route::post('/notes', 'NoteController@store')->name('note.store');
});

Route::group(['middleware' => ['auth', 'role:super-admin|vadītājs|grāmatvedis']], function () {
    Route::get('/invite', 'InviteController@index')->name('invite.index');
    Route::post('/invite', 'InviteController@store')->name('invite.store');
    Route::get('/invite/{invite}/resend', 'InviteController@resendInvite')->name('invite.resend');
    Route::delete('/invite/{invite}', 'InviteController@destroy')->name('invite.destroy');
    // ----------------
    Route::get('/users', 'UserController@index')->name('users.index');
    Route::get('/users/{user}', 'UserController@show')->name('users.show');
    Route::get('/users/{user}/edit-role', 'UserController@editRole')->name('users.edit_role');
    Route::get('/users/{user}/edit', 'UserController@edit')->name('users.edit');
    Route::patch('users/{user}/block', 'UserController@toggleBlock')->name('users.toggleBlock');
    Route::patch('users/{user}', 'UserController@update')->name('users.update');
    Route::delete('/users/{user}', 'UserController@destroy')->name('users.destroy');
});

Route::group(['middleware' => ['auth', 'permission:get reports']], function () {
    Route::get('/employee-reports', function () {
        return view('employee_report.index');
    })->name('employee-report.index');

    Route::get('/employee-reports/export', 'ExportController@export')->name('employee-report.export');
    Route::get('/users/{user}', 'UserController@show')->name('users.show');
    Route::get('/work-contract/{contract}', 'WorkContractController@show')->name('work-contract.show');
});

Route::group(['middleware' => ['auth', 'role:grāmatvedis']], function () {
    Route::post('/work-contract', 'WorkContractController@store')->name('work-contract.store');
    Route::get('/work-contract/{user}/create', 'WorkContractController@create')->name('work-contract.create');
    Route::get('/work-contract/{contract}/edit', 'WorkContractController@edit')->name('work-contract.edit');
    Route::put('/work-contract/{contract}', 'WorkContractController@update')->name('work-contract.update');
    Route::post('/work-contract/{contract}/resignation', 'ResignationController@store')->name('work-contract.resign');
    Route::delete('/work-contract/{contract}/unresign', 'ResignationController@destroy')->name('work-contract.unresign');
    Route::get('/work-contract/{contract}/contract', 'WorkContractController@exportContract')->name('work-contract.exportContract');
    Route::get('/work-contract/{contract}/contract-word', 'WorkContractController@exportContractWord')->name('work-contract.exportContractWord');
    Route::get('/work-contract/{contract}/order', 'WorkContractController@exportOrdinance')->name('work-contract.exportOrder');
    Route::delete('/work-contract/{contract}', 'WorkContractController@destroy')->name('work-contract.destroy');
    // ----------------
    Route::get('/vacations/{vacation}/export', 'VacationController@export')->name('vacations.export');
});

Route::group(['middleware' => ['auth', 'role_or_permission:grāmatvedis|super-admin|accept requests']], function () {
    Route::put('vacations/{vacation}/update-status', 'VacationController@updateStatus')->name('vacations.updateStatus');
});

Route::post('/users/store/', 'UserController@store')->name('users.store');
Route::get('/users/create/{token}', 'UserController@create')->name('users.create');
